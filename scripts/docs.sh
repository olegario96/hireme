#!/bin/bash

./scripts/validate-docs.sh > ./swagger/swagger.json;

echo "[HIREME] - Documentation will be available on port 8080";

docker pull swaggerapi/swagger-ui \
    && docker run --rm -p 8080:8080 \
    -e SWAGGER_JSON=/swagger/swagger.json \
    -v `pwd`/swagger:/swagger swaggerapi/swagger-ui;
