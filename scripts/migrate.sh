#!/bin/bash

operation=$1;
environment=$2;

export NODE_ENV=dev
if [ "$2" = "ci" ]; then
  export NODE_ENV=ci
fi

if [ "$1" = "run" ]; then
  npx ts-node ./node_modules/.bin/typeorm migration:run -d src/database/connection.ts;
  exit 0;
fi

npx ts-node ./node_modules/.bin/typeorm migration:revert -d src/database/connection.ts;
exit 0;
