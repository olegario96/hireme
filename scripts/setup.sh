#!/bin/bash

# Config to run E2E tests
cp .env.ci .env
npm run db:migrate run ci
