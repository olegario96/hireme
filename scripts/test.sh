#!/bin/bash

test_type=$1;
environment=$2;

export NODE_ENV=dev
if [ "$1" = "unit" ]; then
  npx jest --coverage --silent --detectOpenHandles --config=tests/jest.config.unit.js;
  exit 0;
fi

if [ "$2" = "ci" ]; then
  export NODE_ENV=ci
fi

if [ "$1" = "integration" ]; then
  npx jest --coverage --silent --detectOpenHandles --config=tests/jest.config.integration.js;
  exit 0;
fi

npx jest --coverage --silent --detectOpenHandles --config=tests/jest.config.e2e.js;
exit 0;
