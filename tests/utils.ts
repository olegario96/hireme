/* eslint-disable @typescript-eslint/no-var-requires, @typescript-eslint/no-require-imports */
import { Application } from 'express';
import PromiseRouter from 'express-promise-router';
import { container } from 'tsyringe';

import { connection } from '../src/database/connection';
import { PostgresDatabase } from '../src/database/postgres-database';

const boom = require('express-boom');
const express = require('express');

export const endConnections = async (): Promise<void> => {
  await container.resolve(PostgresDatabase).endConnections();
};

export const buildAppForTests = async (context: string): Promise<Application> => {
  await connection.initialize();
  const loadRoutes = require(`${context}/routes`);
  const app = express();
  app.use(express.json());
  app.use(boom());
  const router = PromiseRouter();
  loadRoutes(router);
  app.use(router);
  return app;
};
