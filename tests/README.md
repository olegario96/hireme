# Tests
The tests for this project follow the [Martin Fowler's Pyramid Test](https://martinfowler.com/articles/practical-test-pyramid.html).
Engineers should focus mainly on unit tests and always
aim for 100% code coverage for any test layer.

In `package.json` you will find a unique command to execute tests. Depending on the
test type, you can execute one of the following instructions:
* `npm run test unit`
* `npm run test integration`
* `npm run test e2e`

By default, the above command signatures will consider you are trying to run
tests outside of the docker container. Now, if you have SSHed into
the docker instance, you have to use `ci` as the target environment:
```sh
npm run test integration ci
```

We refer to `dev` environment as the host machine for the docker container.

Usually, integration and E2E tests interact with the database and
third party APIs, so your docker Postgres instance must be
up and running.

As you can see in this folder, there's a `jest.config` for each test layer.

## Unit tests
Unit tests are encouraged for all files and they are required for any
`DomainLogic` class or objects that doesn't require external APIs or
I/O operations.

For this kind of test, engineers should mock every single class, except
for the one that is being actually tested. This way, we can isolate our
code for testing purposes and ensure the new component is doing
what we want.

Take a look at the following example:

<details>
  <summary>Unit test example</summary>

  ```ts
    jest.mock('fs');

    import { readFileSync } from 'fs';

    describe('Config', () => {
        let mockReadFileSync: jest.Mock;
        beforeAll(() => {
          mockReadFileSync = readFileSync as jest.Mock;
        });

        describe('when .env is not provided', () => {
          beforeAll(() => {
            mockExistsSync.mockReturnValueOnce(false);
          });

          it('should throw an error', () => {
            expect(() => container.resolve(Config)).toThrowError('.env file does not exist');
          });
        });
    });
  ```
</details>

On the above code snippet, you can notice we are actually mocking
native functions from NodeJS (i.e. `existsSync`). This way we can
test all the forking paths on our code.

## Integration tests
Integration tests are required for Controller classes or any abstraction
that interact with other classes and requires an I/O operation.
Integration tests are also a requirement for `DAO` classes

We do not encourage use mock for integration tests, unless you need
to test a `try/catch` statement and we have to force an exception.

## E2E tests
End to end tests should be created for any new route. The main goal
is to emulate an external HTTP client interacting with the API. No
mock should be used at this test layer.

The same way we do for integration tests, we don't want to use any
type of mocks for E2E tests, since that would break the whole purpose of
having an E2E test. Once again, mocks are only allowed if we
want to understand how our system would behave when an exception
is thrown by an external component.
