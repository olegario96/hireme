/** @type {import('jest').Config} */
module.exports = {
  clearMocks: true,
  coverageDirectory: 'coverage',
  coverageProvider: 'babel',
  collectCoverageFrom: [
    'src/**/routes.ts',
  ],
  coveragePathIgnorePatterns: [
    'dist/',
    'seed/',
    // abstract class
    'src/common/domain-logic.ts',
  ],
  coverageThreshold: {
    global: {
      'lines': 100,
    }
  },
  rootDir: '../',
  testEnvironment: 'node',
  testMatch: [
    '**.e2e.spec.ts',
  ]
};
