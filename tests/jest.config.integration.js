/** @type {import('jest').Config} */
module.exports = {
  clearMocks: true,
  coverageDirectory: 'coverage',
  coverageProvider: 'babel',
  collectCoverageFrom: [
    'src/**/*-dao.ts',
    'src/**/*-controller.ts',
    'src/database/postgres-database.ts',
    'src/database/connection.ts',
  ],
  coveragePathIgnorePatterns: [
    'dist/',
    'seed/',
    // abstract class
    'src/common/domain-logic.ts',
  ],
  coverageThreshold: {
    global: {
      'lines': 100,
    }
  },
  rootDir: '../',
  testEnvironment: 'node',
  testMatch: [
    '**.integration.spec.ts',
  ]
};
