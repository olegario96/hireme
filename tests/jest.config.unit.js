/** @type {import('jest').Config} */
module.exports = {
  clearMocks: true,
  coverageDirectory: 'coverage',
  coverageProvider: 'babel',
  collectCoverageFrom: [
    'src/**/*.ts',
    '!src/**/*.integration.spec.ts',
    '!src/**/*.e2e.spec.ts'
  ],
  coveragePathIgnorePatterns: [
    'dist/',
    'node_modules/',
    'seed/',
    // Module rename
    'src/middlewares/index.ts',
    // abstract classes
    'src/common/domain-logic.ts',
    'src/common/mapper.ts',
    // Covered by E2E and integration
    'src/index.ts',
    'src/server/index.ts',
    'postgres-database.ts',
    'connection.ts',
    '.*-dao.ts',
    '.*-controller.ts',
    'routes.ts',
    'types.ts',
    'entity.ts',
  ],
  coverageThreshold: {
    global: {
      'lines': 100,
    }
  },
  rootDir: '../',
  testEnvironment: 'node',
  testMatch: [
    '**.unit.spec.ts',
  ]
};
