import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { TABLE_SKILLS } from '../src/skills/entity';
import { metaFieldColumns, metaFieldIndices } from './common/index';

export class skills1673542664330 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_SKILLS,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            isNullable: false,
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    const indices = metaFieldIndices(TABLE_SKILLS);
    await queryRunner.createIndices(TABLE_SKILLS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_SKILLS);
  }
}
