import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

import { TABLE_COMPANIES } from '../src/companies/entity';
import { TABLE_JOB_OPPORTUNITIES } from '../src/job-opportunities/types';
import { metaFieldColumns, metaFieldIndices } from './common/index';

export class jobOpportunities1673543110034 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_JOB_OPPORTUNITIES,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            isNullable: false,
          },
          {
            name: 'company_id',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'description',
            type: 'text',
            isNullable: false,
          },
          {
            name: 'is_remote',
            type: 'boolean',
            isNullable: false,
            default: true,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    await queryRunner.createForeignKey(
      TABLE_JOB_OPPORTUNITIES,
      new TableForeignKey({
        columnNames: ['company_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_COMPANIES,
        onDelete: 'CASCADE',
        name: 'fk_company',
      })
    );

    const indices = metaFieldIndices(TABLE_JOB_OPPORTUNITIES);
    await queryRunner.createIndices(TABLE_JOB_OPPORTUNITIES, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_JOB_OPPORTUNITIES);
  }
}
