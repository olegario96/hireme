import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

import { TABLE_TWO_FA_TOKENS } from '../src/security/two-fa/entity';
import { TABLE_USERS } from '../src/users/entity';
import { metaFieldColumns, metaFieldIndices } from './common/index';

export class twoFaTokens1673644380900 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_TWO_FA_TOKENS,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            isNullable: false,
          },
          {
            name: 'user_id',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'token',
            type: 'varchar(255)',
            isNullable: false,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    await queryRunner.createForeignKey(
      TABLE_TWO_FA_TOKENS,
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_USERS,
        onDelete: 'CASCADE',
        name: 'fk_user',
      })
    );

    const indices = metaFieldIndices(TABLE_TWO_FA_TOKENS);
    await queryRunner.createIndices(TABLE_TWO_FA_TOKENS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_TWO_FA_TOKENS);
  }
}
