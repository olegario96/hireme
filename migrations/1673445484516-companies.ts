import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { TABLE_COMPANIES } from '../src/companies/entity';
import { metaFieldColumns, metaFieldIndices } from './common';

export class companies1673445484516 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_COMPANIES,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            isNullable: false,
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'logo_url',
            type: 'varchar(255)',
            default: null,
            isNullable: true,
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true,
            default: null,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    const indices = metaFieldIndices(TABLE_COMPANIES);
    await queryRunner.createIndices(TABLE_COMPANIES, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_COMPANIES);
  }
}
