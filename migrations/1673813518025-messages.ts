import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

import { TABLE_CONVERSATIONS } from '../src/conversations/types';
import { TABLE_MESSAGES } from '../src/messages/types';
import { TABLE_USERS } from '../src/users/entity';
import { metaFieldColumns, metaFieldIndices } from './common/index';

export class messages1673813518025 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_MESSAGES,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            isNullable: false,
          },
          {
            name: 'conversation_id',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'response_to',
            type: 'int',
            default: null,
            isNullable: true,
          },
          {
            name: 'user_id_sender',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'user_id_receiver',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'content',
            type: 'text',
            isNullable: false,
          },
          {
            name: 'sent_at',
            type: 'timestamp',
            isNullable: true,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    await queryRunner.createForeignKey(
      TABLE_MESSAGES,
      new TableForeignKey({
        columnNames: ['conversation_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_CONVERSATIONS,
        onDelete: 'CASCADE',
        name: 'fk_conversation',
      })
    );

    await queryRunner.createForeignKey(
      TABLE_MESSAGES,
      new TableForeignKey({
        columnNames: ['response_to'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_MESSAGES,
        onDelete: 'CASCADE',
        name: 'fk_message',
      })
    );

    await queryRunner.createForeignKey(
      TABLE_MESSAGES,
      new TableForeignKey({
        columnNames: ['user_id_sender'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_USERS,
        onDelete: 'CASCADE',
        name: 'fk_user_sender',
      })
    );

    await queryRunner.createForeignKey(
      TABLE_MESSAGES,
      new TableForeignKey({
        columnNames: ['user_id_receiver'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_USERS,
        onDelete: 'CASCADE',
        name: 'fk_user_receiver',
      })
    );

    const indices = metaFieldIndices(TABLE_MESSAGES);
    await queryRunner.createIndices(TABLE_MESSAGES, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_MESSAGES);
  }
}
