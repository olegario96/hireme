import { MigrationInterface, QueryRunner } from 'typeorm';

export class populateSkills1673542811579 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      INSERT INTO
        skills
          (name)
        VALUES
          ('Typescript'),
          ('Postgres'),
          ('ReactJS'),
          ('Javascript'),
          ('MySQL'),
          ('PHP'),
          ('NodeJS'),
          ('VueJS'),
          ('CSS'),
          ('HTML'),
          ('Linux'),
          ('AWS'),
          ('Ruby'),
          ('Rails'),
          ('Git'),
          ('Java'),
          ('C++'),
          ('C#'),
          ('Docker'),
          ('Kubernetes'),
          ('Docker Compose'),
          ('Google Cloud'),
          ('Azure'),
          ('C'),
          ('Hibernate'),
          ('SQLite'),
          ('SQL Server'),
          ('Backbone'),
          ('Python'),
          ('Ubuntu')
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      DELETE FROM skills
    `);
  }
}
