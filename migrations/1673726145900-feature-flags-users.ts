import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

import { TABLE_FEATURE_FLAGS, TABLE_FEATURE_FLAGS_USERS } from '../src/feature-flags/types';
import { TABLE_USERS } from '../src/users/entity';
import { metaFieldColumns, metaFieldIndices } from './common';

export class featureFlagsUsers1673726145900 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_FEATURE_FLAGS_USERS,
        columns: [
          {
            name: 'feature_flag_id',
            type: 'int',
            isPrimary: true,
            isNullable: false,
          },
          {
            name: 'user_id',
            type: 'int',
            isPrimary: true,
            isNullable: false,
          },
          {
            name: 'is_turned_on',
            type: 'boolean',
            isNullable: false,
            default: false,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    await queryRunner.createForeignKey(
      TABLE_FEATURE_FLAGS_USERS,
      new TableForeignKey({
        columnNames: ['feature_flag_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_FEATURE_FLAGS,
        onDelete: 'CASCADE',
        name: 'fk_feature_flag',
      })
    );

    await queryRunner.createForeignKey(
      TABLE_FEATURE_FLAGS_USERS,
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_USERS,
        onDelete: 'CASCADE',
        name: 'fk_user',
      })
    );

    const indices = metaFieldIndices(TABLE_FEATURE_FLAGS_USERS);
    await queryRunner.createIndices(TABLE_FEATURE_FLAGS_USERS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_FEATURE_FLAGS_USERS);
  }
}
