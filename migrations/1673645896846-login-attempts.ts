import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { TABLE_LOGIN_ATTEMPTS } from '../src/server/entity';
import { metaFieldColumns, metaFieldIndices } from './common';

export class loginAttempts1673645896846 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_LOGIN_ATTEMPTS,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            isNullable: false,
          },
          {
            name: 'ip',
            type: 'VARCHAR(39)',
            isNullable: false,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    const indices = metaFieldIndices(TABLE_LOGIN_ATTEMPTS);
    await queryRunner.createIndices(TABLE_LOGIN_ATTEMPTS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_LOGIN_ATTEMPTS);
  }
}
