import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { TABLE_FEATURE_FLAGS } from '../src/feature-flags/types';
import { metaFieldColumns, metaFieldIndices } from './common';

export class featureFlags1673725840168 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_FEATURE_FLAGS,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            isNullable: false,
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isUnique: true,
            isNullable: false,
          },
          {
            name: 'description',
            type: 'varchar(255)',
            default: null,
            isNullable: true,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    const indices = metaFieldIndices(TABLE_FEATURE_FLAGS);
    await queryRunner.createIndices(TABLE_FEATURE_FLAGS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_FEATURE_FLAGS);
  }
}
