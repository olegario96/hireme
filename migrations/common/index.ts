import { TableIndex } from 'typeorm';
export const metaFieldColumns = [
  {
    name: 'created_at',
    type: 'timestamp',
    default: 'now()',
    isNullable: false,
  },
  {
    name: 'updated_at',
    type: 'timestamp',
    default: 'now()',
    isNullable: false,
  },
  {
    name: 'deleted_at',
    type: 'timestamp',
    default: null,
    isNullable: true,
  },
];

export const metaFieldIndices = (tableName: string): TableIndex[] => ([
  new TableIndex({
    name: `key_created_at_${tableName}`,
    columnNames: ['created_at'],
    isUnique: false,
  }),
  new TableIndex({
    name: `key_deleted_at_${tableName}`,
    columnNames: ['deleted_at'],
    isUnique: false,
  }),
]);
