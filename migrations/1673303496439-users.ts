import { MigrationInterface, QueryRunner, Table } from 'typeorm';

import { TABLE_USERS } from '../src/users/entity';
import { metaFieldColumns, metaFieldIndices } from './common';
export class users1673303496439 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_USERS,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            isNullable: false,
          },
          {
            name: 'first_name',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'last_name',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'email',
            type: 'varchar(255)',
            isNullable: false,
            isUnique: true,
          },
          {
            name: 'password',
            type: 'varchar(255)',
            isNullable: true,
          },
          {
            name: 'salt',
            type: 'varchar(255)',
            isNullable: true,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists
    );

    const indices = metaFieldIndices(TABLE_USERS);
    await queryRunner.createIndices(TABLE_USERS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_USERS);
  }
}
