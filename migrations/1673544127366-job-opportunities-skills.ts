import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

import { TABLE_JOB_OPPORTUNITIES, TABLE_JOB_OPPORTUNITIES_SKILLS } from '../src/job-opportunities/types';
import { TABLE_SKILLS } from '../src/skills/entity';
import { metaFieldColumns, metaFieldIndices } from './common/index';

export class jobOpportunitiesSkills1673544127366 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_JOB_OPPORTUNITIES_SKILLS,
        columns: [
          {
            name: 'job_opportunity_id',
            type: 'int',
            isPrimary: true,
            isNullable: false,
          },
          {
            name: 'skill_id',
            type: 'int',
            isPrimary: true,
            isNullable: false,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    await queryRunner.createForeignKey(
      TABLE_JOB_OPPORTUNITIES_SKILLS,
      new TableForeignKey({
        columnNames: ['job_opportunity_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_JOB_OPPORTUNITIES,
        onDelete: 'CASCADE',
        name: 'fk_job_opportunity',
      })
    );

    await queryRunner.createForeignKey(
      TABLE_JOB_OPPORTUNITIES_SKILLS,
      new TableForeignKey({
        columnNames: ['skill_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_SKILLS,
        onDelete: 'CASCADE',
        name: 'fk_skill',
      })
    );

    const indices = metaFieldIndices(TABLE_JOB_OPPORTUNITIES_SKILLS);
    await queryRunner.createIndices(TABLE_JOB_OPPORTUNITIES_SKILLS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_JOB_OPPORTUNITIES_SKILLS);
  }
}
