import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

import { TABLE_CONVERSATIONS } from '../src/conversations/types';
import { TABLE_USERS } from '../src/users/entity';
import { metaFieldColumns, metaFieldIndices } from './common/index';

export class conversations1673813103123 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_CONVERSATIONS,
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isNullable: false,
            isGenerated: true,
          },
          {
            name: 'user_id_sender',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'user_id_receiver',
            type: 'int',
            isNullable: false,
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    await queryRunner.createForeignKey(
      TABLE_CONVERSATIONS,
      new TableForeignKey({
        columnNames: ['user_id_sender'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_USERS,
        onDelete: 'CASCADE',
        name: 'fk_user_sender',
      })
    );

    await queryRunner.createForeignKey(
      TABLE_CONVERSATIONS,
      new TableForeignKey({
        columnNames: ['user_id_receiver'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_USERS,
        onDelete: 'CASCADE',
        name: 'fk_user_receiver',
      })
    );

    await queryRunner.createIndex(
      TABLE_CONVERSATIONS,
      new TableIndex({
        columnNames: ['user_id_sender', 'user_id_receiver'],
        isUnique: true,
      })
    );

    const indices = metaFieldIndices(TABLE_CONVERSATIONS);
    await queryRunner.createIndices(TABLE_CONVERSATIONS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_CONVERSATIONS);
  }
}
