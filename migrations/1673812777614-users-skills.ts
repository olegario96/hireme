import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

import { TABLE_SKILLS } from '../src/skills/entity';
import { TABLE_USERS, TABLE_USERS_SKILLS } from '../src/users/entity';
import { metaFieldColumns, metaFieldIndices } from './common/index';

export class usersSkills1673812777614 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const ifNotExists = true;
    await queryRunner.createTable(
      new Table({
        name: TABLE_USERS_SKILLS,
        columns: [
          {
            name: 'user_id',
            type: 'int',
            isPrimary: true,
            isNullable: false,
          },
          {
            name: 'skill_id',
            type: 'int',
            isPrimary: true,
            isNullable: false,
          },
          {
            name: 'skill_proficiency',
            type: 'smallint',
            isNullable: false,
            default: 0,
          },
          ...metaFieldColumns,
        ],
      }),
      ifNotExists,
    );

    await queryRunner.createForeignKey(
      TABLE_USERS_SKILLS,
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_USERS,
        onDelete: 'CASCADE',
        name: 'fk_user',
      })
    );

    await queryRunner.createForeignKey(
      TABLE_USERS_SKILLS,
      new TableForeignKey({
        columnNames: ['skill_id'],
        referencedColumnNames: ['id'],
        referencedTableName: TABLE_SKILLS,
        onDelete: 'CASCADE',
        name: 'fk_skill',
      })
    );

    await queryRunner.createIndex(
      TABLE_USERS_SKILLS,
      new TableIndex({
        columnNames: ['user_id', 'skill_id'],
        isUnique: true,
      })
    );

    const indices = metaFieldIndices(TABLE_USERS_SKILLS);
    await queryRunner.createIndices(TABLE_USERS_SKILLS, indices);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_USERS_SKILLS);
  }
}
