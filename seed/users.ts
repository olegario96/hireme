import { container } from 'tsyringe';

import { PersistedUser } from '../src/users/entity';
import { UserController } from '../src/users/user-controller';
import { assertEnvironment } from './helper';

export const populateUsersTable = (override?: PersistedUser): Promise<PersistedUser | null> => {
  assertEnvironment();
  const defaultUser = {
    firstName: 'Darth',
    lastName: 'Sidious',
    email: 'darth@sidious.com',
    password: 'ExecuteOrder66',
    salt: '',
  };

  const user = !override ? defaultUser : { ...defaultUser, ...override };
  return container.resolve(UserController).createUser(user);
};
