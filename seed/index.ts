import 'reflect-metadata';

import { readFileSync } from 'fs';
import { container } from 'tsyringe';

import { UTF_8 } from '../src/common/constants';
import { TABLE_COMPANIES } from '../src/companies/entity';
import { TABLE_CONVERSATIONS } from '../src/conversations/types';
import { connection } from '../src/database/connection';
import { TABLE_FEATURE_FLAGS } from '../src/feature-flags/types';
import { TABLE_JOB_OPPORTUNITIES } from '../src/job-opportunities/types';
import { Logger } from '../src/logger';
import { TABLE_SKILLS } from '../src/skills/entity';
import { TABLE_USERS } from '../src/users/entity';
import { populateCompaniesTable } from './companies';
import { populateConversationsTable } from './conversations';
import { populateFeatureFlagsTable } from './feature-flags';
import { populateJobOpportunitiesTable } from './job-opportunities';
import { populateSkillsTable } from './skills';
import { populateUsersTable } from './users';

const logger = container.resolve(Logger);
const tableSeedProcMap = {
  [TABLE_COMPANIES]: populateCompaniesTable,
  [TABLE_CONVERSATIONS]: populateConversationsTable,
  [TABLE_FEATURE_FLAGS]: populateFeatureFlagsTable,
  [TABLE_JOB_OPPORTUNITIES]: populateJobOpportunitiesTable,
  [TABLE_SKILLS]: populateSkillsTable,
  [TABLE_USERS]: populateUsersTable,
};

const main = (): void => {
  const minimumArguments = 2;
  const tables = process.argv.splice(minimumArguments);
  if (!tables.length) {
    const fileContent = readFileSync(`${__dirname}/usage.txt`, { encoding: UTF_8 });
    logger.log(fileContent);
    return;
  }

  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  tables.forEach(async (table) => {
    logger.log(`Preparing to populate ${table} table`);
    /** @ts-ignore */
    const seedProcedure = tableSeedProcMap[table];
    if (!seedProcedure) {
      throw new Error(`seed procedure does not support ${table} table`);
    }

    await connection.initialize();
    await seedProcedure();
    logger.log(`${table} table has been populated!`);
  });
};

main();
