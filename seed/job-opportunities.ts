import { container } from 'tsyringe';

import { PersistedCompany } from '../src/companies/entity';
import { JobOpportunityDao } from '../src/job-opportunities/job-opportunity-dao';
import { PersistedJobOpportunity } from '../src/job-opportunities/types';
import { PersistedSkill } from '../src/skills/entity';
import { populateCompaniesTable } from './companies';
import { assertEnvironment } from './helper';

export const populateJobOpportunitiesTable = async (
  override?: PersistedJobOpportunity,
  company?: PersistedCompany,
  skills?: PersistedSkill[],
): Promise<PersistedJobOpportunity> => {
  assertEnvironment();
  const { id: companyId } = company ? company : (await populateCompaniesTable());
  const defaultJobOpportunity = { companyId, name: 'Senior Software Engineer', description: 'foo', isRemote: true, skills };
  const jobOpportunity = !override ? defaultJobOpportunity : { ...defaultJobOpportunity, ...override };
  return container.resolve(JobOpportunityDao).createJobOpportunity(jobOpportunity);
};
