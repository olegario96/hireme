import { container } from 'tsyringe';

import { CompanyDao } from '../src/companies/company-dao';
import { PersistedCompany } from '../src/companies/entity';
import { assertEnvironment } from './helper';

export const populateCompaniesTable = (override?: PersistedCompany): Promise<PersistedCompany> => {
  assertEnvironment();
  const dao = container.resolve(CompanyDao);
  const defaultCompany = {
    name: 'hireme',
    logoUrl: 'http://www.officialpix.com/images/order66/order66-2.jpg',
    description: '',
  };

  const company = !override ? defaultCompany : { ...defaultCompany, ...override };
  return dao.createCompany(company);
};
