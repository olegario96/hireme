import { container } from 'tsyringe';

import { FeatureFlagDao } from '../src/feature-flags/feature-flag-dao';
import { PersistedFeatureFlag } from '../src/feature-flags/types';
import { assertEnvironment } from './helper';

export const populateFeatureFlagsTable = async (override?: PersistedFeatureFlag): Promise<PersistedFeatureFlag> => {
  assertEnvironment();
  const targetFeatureFlag = { name: 'create.companies', description: 'foo', ...override };
  return container.resolve(FeatureFlagDao).createFeatureFlag(targetFeatureFlag);
};
