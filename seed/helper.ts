import { container } from 'tsyringe';

import { Config } from '../src/config';
import { Environment } from '../src/config/types';
import { PostgresDatabase } from '../src/database/postgres-database';

const testEnvironments = [
  Environment.DEV,
  Environment.CI,
  Environment.STG,
];

export const resetTable = async(tableName: string): Promise<void> => {
  const query = `
    TRUNCATE TABLE
      ${tableName}
    RESTART IDENTITY CASCADE;
  `;

  await container.resolve(PostgresDatabase).executeQuery({ query });
};

export const assertEnvironment = (allowedEnvironments = testEnvironments): void => {
  if (!allowedEnvironments.length) {
    throw new Error('Invalid environments list provided');
  }

  container.resolve(Config).ensureEnvironmentVariableIsDefined(['NODE_ENV']);
  const currentEnvironment = process.env.NODE_ENV! as Environment;
  if (!allowedEnvironments.includes(currentEnvironment)) {
    throw new Error(`Seed procedure can not be executed in ${currentEnvironment}`);
  }
};
