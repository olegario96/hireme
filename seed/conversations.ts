import { container } from 'tsyringe';

import { ConversationDao } from '../src/conversations/conversation-dao';
import { PersistedConversation } from '../src/conversations/types';

export const populateConversationsTable = (override?: PersistedConversation): Promise<PersistedConversation> => {
  const defaultConversation = {
    userIdSender: 1,
    userIdReceiver: 2,
    isActive: 1,
  };

  const targetConversation = { ...defaultConversation, ...override };
  return container.resolve(ConversationDao).createConversation(targetConversation);
};
