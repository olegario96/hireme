import { container } from 'tsyringe';

import { PersistedSkill } from '../src/skills/entity';
import { SkillDao } from '../src/skills/skill-dao';
import { assertEnvironment } from './helper';

export const populateSkillsTable = (override?: PersistedSkill): Promise<PersistedSkill> => {
  assertEnvironment();
  const defaultSkill = { name: 'Typescript' };
  const skill = !override ? defaultSkill : { ...defaultSkill, ...override };
  return container.resolve(SkillDao).createSkill(skill);
};
