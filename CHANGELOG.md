# [0.5.0](https://gitlab.com/olegario96/hireme/compare/v0.4.3...v0.5.0) (2023-11-03)


### Features

* **git:** add pre-push validation file ([3972696](https://gitlab.com/olegario96/hireme/commit/3972696512cce7fd0156263982d6b61d4bb3e90b))

## [0.4.3](https://gitlab.com/olegario96/hireme/compare/v0.4.2...v0.4.3) (2023-11-02)


### Bug Fixes

* **release:** change order for plugins ([2ebcf4c](https://gitlab.com/olegario96/hireme/commit/2ebcf4c6e13ee381ccc0ae2b1e78c668fb1cd101))

## [0.4.2](https://gitlab.com/olegario96/hireme/compare/v0.4.1...v0.4.2) (2023-11-01)


### Bug Fixes

* **npm:** include repository URL ([a2fae2c](https://gitlab.com/olegario96/hireme/commit/a2fae2c9e831b7627b35fe6db991d2b1f80bdcbf))

## [0.4.1](https://gitlab.com/olegario96/hireme/compare/v0.4.0...v0.4.1) (2023-11-01)


### Bug Fixes

* **release:** prevent script from publishing to npm ([df73f1b](https://gitlab.com/olegario96/hireme/commit/df73f1b1997d6aae8c5bf384d08492c4bfe9751b))

# [0.4.0](https://gitlab.com/olegario96/hireme/compare/v0.3.2...v0.4.0) (2023-10-29)


### Bug Fixes

* **all:** Address linter issues ([363dcbe](https://gitlab.com/olegario96/hireme/commit/363dcbec9049df0426f5bd3523e5cb4ee54a318c))
* **all:** Remove migration ci command ([ca85162](https://gitlab.com/olegario96/hireme/commit/ca8516283ccaf5c055dafb365266c28c29e9c25c))
* **common:** Address missing column name ([0890ebe](https://gitlab.com/olegario96/hireme/commit/0890ebe177e110b34e1346b2a5d2fb97930b82bb))
* **config:** Read environment variables from dotenv file ([ecbe339](https://gitlab.com/olegario96/hireme/commit/ecbe33937fe264484c2eaa79f199bdc68d96da96))
* **database:** Address TS errors ([782098d](https://gitlab.com/olegario96/hireme/commit/782098dfcc0cc0af5c9326445007769410c51f65))
* **deps:** remove npm integration for semantic release ([21dd7ff](https://gitlab.com/olegario96/hireme/commit/21dd7ff36b86116e67d2597e32897407435f9127))
* **docker:** Address hadolint errors ([91ffe46](https://gitlab.com/olegario96/hireme/commit/91ffe4692b10d3882778cb712db1e48a2b86de90))
* **docker:** Address outdated nodejs version ([8026cb5](https://gitlab.com/olegario96/hireme/commit/8026cb5769e1ca7ae3089ef85927edd88f33b48a))
* **docker:** Address syntax error ([dca5581](https://gitlab.com/olegario96/hireme/commit/dca5581651263b8cf33295d1ee6d0faeb7f97dd7))
* **docker:** List volumes for composer ([be248cc](https://gitlab.com/olegario96/hireme/commit/be248cc4da09cfaac9bd08b1fef7327a6ed169e7))
* **docker:** Lock git version ([9aa7d67](https://gitlab.com/olegario96/hireme/commit/9aa7d671ae57e035ef3cf4a56b7b4c289c838874))
* **docker:** Remove legacy command to run migrations ([f1e2e23](https://gitlab.com/olegario96/hireme/commit/f1e2e23b246121f55648c3d6f75c7bae1c75e39d))
* **e2e:** Address outdated migration operation name ([3d3b907](https://gitlab.com/olegario96/hireme/commit/3d3b90770952b5ac63b77261ced0b77b1d3e63c6))
* **featureflag:** Address missing early return ([8607a4d](https://gitlab.com/olegario96/hireme/commit/8607a4d405bc83627f8faab045907d143cae1f13))
* **featureflag:** Istanbul ignore ([99b8fc1](https://gitlab.com/olegario96/hireme/commit/99b8fc1c185c7b402eae227551a9dc4b0f31f1d3))
* **gitlab:** address incorrect command name ([718741f](https://gitlab.com/olegario96/hireme/commit/718741f27fa1fcbbc5a1da340ae9e8af6b425afa))
* **gitlab:** Remove outdated command to execute tests ([972e05c](https://gitlab.com/olegario96/hireme/commit/972e05caba51654948d30e0b53dc8d3b8138293c))
* **gitlab:** Run dependabot right after setup ([3486b26](https://gitlab.com/olegario96/hireme/commit/3486b26329fd6bf9746453124f6d0048b78ac949))
* **husky:** Address incorrect command name ([9aadb25](https://gitlab.com/olegario96/hireme/commit/9aadb25809418365d42397551948d2968d9b955c))
* **husky:** use new standard for githooks ([92062ef](https://gitlab.com/olegario96/hireme/commit/92062ef2932db9db025bcf1a57fcd13364cc065c))
* **ip:** allow undefined values as input validation ([1714923](https://gitlab.com/olegario96/hireme/commit/1714923db6083bd016afbe8cb6f601479dfef07c))
* **lint:** Address outdated rule ([eff8159](https://gitlab.com/olegario96/hireme/commit/eff81591131239af588b2965705bd2cd907da83e))
* **lint:** Address outdated rule ([4857711](https://gitlab.com/olegario96/hireme/commit/48577118c8652432165ef3c22c21cf0e82f346e6))
* **lint:** Disable unsafe argument rule ([5acdddc](https://gitlab.com/olegario96/hireme/commit/5acdddc8e5b77436e8249c7aba9ddeb71a5cf5be))
* **login:** Address incorrect class import ([acf800f](https://gitlab.com/olegario96/hireme/commit/acf800fcd4f41053bffd64979ff730d0c2a1b818))
* **migrations:** Address bad copy and pasta ([0357ea8](https://gitlab.com/olegario96/hireme/commit/0357ea8917b45da114559efc1b11565907867f00))
* **migrations:** Address incorrect import ([3dfd2da](https://gitlab.com/olegario96/hireme/commit/3dfd2da23519aa097adb18e4fcb2505aa4210986))
* **migrations:** Address incorrect table name ([c2d1a8b](https://gitlab.com/olegario96/hireme/commit/c2d1a8b57260676c9536bcadcf3e59e66b3fd471))
* **migrations:** Address linter issues ([b04a3df](https://gitlab.com/olegario96/hireme/commit/b04a3dfe42cc987a62cc8be95a41885e7b1a04ea))
* **migrations:** Address linter issues ([323045a](https://gitlab.com/olegario96/hireme/commit/323045ae4812695f2e4e61cbd625de7b0dfe7ef4))
* **migrations:** Address missing autogenerated field ([0bc8b6e](https://gitlab.com/olegario96/hireme/commit/0bc8b6e883f08119b4ff238b16ab9438f73a45ef))
* **migrations:** Address missing id ([e117213](https://gitlab.com/olegario96/hireme/commit/e1172130a79e8a17194ce9799790a4f3c7bd9eaf))
* **migrations:** Adds missing unique index ([b7296d1](https://gitlab.com/olegario96/hireme/commit/b7296d1148e9d4fcb8ce3ad412d8f54300c4acb5))
* **migrations:** Description can't be empty for companies ([5fc544f](https://gitlab.com/olegario96/hireme/commit/5fc544f539455ec91915b949ba82de83ac3cab15))
* **migrations:** Remove nullable constraints ([876d078](https://gitlab.com/olegario96/hireme/commit/876d078f0007ad5a67dafd550cfd9e5495d8b6c2))
* **opportunity:** Remove skill mapper from DAO ([4aef6ca](https://gitlab.com/olegario96/hireme/commit/4aef6cadfb70d1eaa71c25e71ccab020993850b7))
* **[secure]:** Remove useless config ([30290c2](https://gitlab.com/olegario96/hireme/commit/30290c204d76469d7879b8d6ff4af1af1e4f4c64))
* **release:** disable success comment ([43075ef](https://gitlab.com/olegario96/hireme/commit/43075ef11bc76d19c968a5a7443f30de7a781d66))
* **release:** remove npm integration ([7d08a8e](https://gitlab.com/olegario96/hireme/commit/7d08a8eae65fae64888558b1de68d43261d2e5dd))
* **root:** Address incorrect script alias name ([4e415d6](https://gitlab.com/olegario96/hireme/commit/4e415d61627016d6a1e4406312257158e29acd57))
* **scripts:** Address incorrect container name ([e4c5949](https://gitlab.com/olegario96/hireme/commit/e4c59491f9adc7d5d49ddfd0977bb34f57acc481))
* **scripts:** Address incorrect DB user name ([4d31bdc](https://gitlab.com/olegario96/hireme/commit/4d31bdc04fbe6c7648a19662a5949f7b09bf45b0))
* **scripts:** Address linter issues ([0c2ec01](https://gitlab.com/olegario96/hireme/commit/0c2ec01da6f1176bd87f0665471b32f5db341f43))
* **scripts:** Address outdated migration execution command ([eb67faf](https://gitlab.com/olegario96/hireme/commit/eb67faf371c1194bbf5172b223af46cbc279804a))
* **scripts:** Use new command signature to run tests ([cce4ce8](https://gitlab.com/olegario96/hireme/commit/cce4ce81e0867f3e004e710e21ccf8dc65cc59c4))
* **seed:** Address broken imports ([8d1ce33](https://gitlab.com/olegario96/hireme/commit/8d1ce338f0fecee3a6b09db3b284ebb307591610))
* **seed:** Address broken procedure ([4dd3b96](https://gitlab.com/olegario96/hireme/commit/4dd3b96006a4b42d4ba4c47369a094ca4ac88280))
* **server:** Address incorrect type for meta fields ([9dd8213](https://gitlab.com/olegario96/hireme/commit/9dd821383825032977c36f7188d20aae6c38d6dc))
* **tests:** Address outdated command to run migrations ([029e515](https://gitlab.com/olegario96/hireme/commit/029e515f846f77e047a9ad2dc9569de0ea384dc4))
* **tests:** Address warning messages for integration files ([352577a](https://gitlab.com/olegario96/hireme/commit/352577abc72b582325f7c55cc6882b439b7a86c3))
* **tsconfig:** Error handled as Exception by default ([8e6c2cc](https://gitlab.com/olegario96/hireme/commit/8e6c2ccc17b6df85c38629e2c5072d39d4d99728))
* **users:** Address repeated email issue ([93b056e](https://gitlab.com/olegario96/hireme/commit/93b056e185519ba7a48985c1bc4accfe58bf2683))


### Features

* **2fa:** Create entity file ([53f9161](https://gitlab.com/olegario96/hireme/commit/53f91614393998a5751933686f2754d08391e137))
* **2fa:** DAO class implements repository standard ([cbec44f](https://gitlab.com/olegario96/hireme/commit/cbec44fc5ad8fdaecee0c9b7bcf3f2da4815b7e7))
* **commitlint:** Ignore version bump commit messages ([4aece72](https://gitlab.com/olegario96/hireme/commit/4aece72d32fbc08d76fc713e62b250313e258f92))
* **common:** Adds user entity to global definition ([5fdaf7b](https://gitlab.com/olegario96/hireme/commit/5fdaf7b82f60ade7e3115190fa69bf91f2994534))
* **common:** Meta fields implement typeORM date type ([41774ed](https://gitlab.com/olegario96/hireme/commit/41774eddcdba7e65805631bfc95e2ef1cac0583a))
* **common:** Shared entity implements metafields following typeORM convention ([4737a1e](https://gitlab.com/olegario96/hireme/commit/4737a1e30d58131a0d67861361a23aea58e18dbf))
* **common:** Shared method to parse model to entity ([c7ae57d](https://gitlab.com/olegario96/hireme/commit/c7ae57dd515414a6d649368e1d957a7c059aeee9))
* **company:** Create entity file ([dbd23dd](https://gitlab.com/olegario96/hireme/commit/dbd23dd6cbb40abe59ece5be61ac071ecf0cb5fc))
* **company:** DAO class implements typeORM standard ([7ec6f70](https://gitlab.com/olegario96/hireme/commit/7ec6f70507f4bc5151701ba916b9d2f359ac47c7))
* **company:** Entity definition extends Metafields class ([71f5a71](https://gitlab.com/olegario96/hireme/commit/71f5a7131f62eb6a9e950f886850ac20576d994a))
* **config:** Use dotenv package ([584a667](https://gitlab.com/olegario96/hireme/commit/584a667e4b86b92b7c39f72714a83830adb8eb7b))
* **connection:** Adds login attempt entity to the global list ([0c56477](https://gitlab.com/olegario96/hireme/commit/0c564773dbd5bcf4dbfd5ef7cc3ce6949f2c9b15))
* **database:** Add user entity definition to global list ([d0f0592](https://gitlab.com/olegario96/hireme/commit/d0f059278eae719f3a354ac98b7157a2740ecd15))
* **database:** Adds company to global entity list ([e493a24](https://gitlab.com/olegario96/hireme/commit/e493a24239699bee54b5297e60adbdc82484f5af))
* **database:** Create connection file ([d07097f](https://gitlab.com/olegario96/hireme/commit/d07097f8061960d0ea48327955d566216b8c34c4))
* **database:** Creates abstraction to integrate with typeORM ([1693151](https://gitlab.com/olegario96/hireme/commit/16931519fd54adb7bbae39480bc7c823b20b7f37))
* **database:** Login attempt listed as entity ([ac85004](https://gitlab.com/olegario96/hireme/commit/ac850044d136193f3b82819df055b8280666adce))
* **database:** Method to terminate connection ([7747867](https://gitlab.com/olegario96/hireme/commit/7747867693fb9968ee058f889b540d79985c5b89))
* **docker:** Add healthcheck to [secure] container ([077a95f](https://gitlab.com/olegario96/hireme/commit/077a95fb9029305f8036bf6d772cf8b7bbeb67ab))
* **docker:** Adds redis container to compose file ([53707f6](https://gitlab.com/olegario96/hireme/commit/53707f64b904b243b0d26e30f6b19657138119e0))
* **docker:** Create compose file ([5d74b91](https://gitlab.com/olegario96/hireme/commit/5d74b9161d1e8cc53a5f0cea6980622fa2e3868a))
* **docker:** File builds app ([5723696](https://gitlab.com/olegario96/hireme/commit/57236967ac8073bcb9905f23140b1fc6584b6538))
* **docker:** Run migration before start container API ([e621c40](https://gitlab.com/olegario96/hireme/commit/e621c405ae63ddcabb03cf5c24d4d731c985cbb6))
* **gitlab:** Assert lockfile won't be changed on CI pipeline ([63098fd](https://gitlab.com/olegario96/hireme/commit/63098fd880228fd00221ea0a7561ee226eb300e2))
* **login:** Attempts DAO uses only typeORM ([eb95a52](https://gitlab.com/olegario96/hireme/commit/eb95a52f3b05733c9dae71b487a78a8bc982a2a2))
* **login:** Login attempt uses typeORM connection ([3bde7e8](https://gitlab.com/olegario96/hireme/commit/3bde7e8561f4d6b7c916fef4dc3a5b895afc301f))
* **migrations:** Common definition for metafields ([c45ff74](https://gitlab.com/olegario96/hireme/commit/c45ff74f4dfa12272d21702fc845af595cc1d6a5))
* **migrations:** Companies table creation use typeORM lib ([8c41fd3](https://gitlab.com/olegario96/hireme/commit/8c41fd3fd9c9cd0c519116e8ac0942191de9e08d))
* **migrations:** Conversations table creation file uses typeORM ([3215174](https://gitlab.com/olegario96/hireme/commit/3215174a41aacbb0067a7f213fb4846efd4beed8))
* **migrations:** Feature flags table uses typeORM ([9f26cfe](https://gitlab.com/olegario96/hireme/commit/9f26cfef36bd9e67868df2d01706864d52eab50c))
* **migrations:** Feature flags users uses typeORM standard ([d2d153c](https://gitlab.com/olegario96/hireme/commit/d2d153ce2b543963c2576f9714ba46afc8a2b2d4))
* **migrations:** File to create 2FA token table uses typeORM ([01f0d69](https://gitlab.com/olegario96/hireme/commit/01f0d6962a241041f4cda20122b6261b62720fa9))
* **migrations:** File to create job opportunities skills table uses typeORM lib ([8f748fa](https://gitlab.com/olegario96/hireme/commit/8f748fa4dc76e5ca91ad6c3be2e7aa7f4064fab5))
* **migrations:** File to create users skills table uses typeORM ([2d345ec](https://gitlab.com/olegario96/hireme/commit/2d345eca5dad692dff3a7bc3a18568b0f52ee6d8))
* **migrations:** Login attempts are using typeORM standard ([9ee6dc9](https://gitlab.com/olegario96/hireme/commit/9ee6dc965095f452fe893918266b9c668de0c75f))
* **migrations:** Messages table creation file uses typeORM ([cc1f994](https://gitlab.com/olegario96/hireme/commit/cc1f994024d67dfdcbc5288257e1d49caf17383e))
* **migrations:** Migration file for skills table use typeORM lib ([b695367](https://gitlab.com/olegario96/hireme/commit/b695367eb05ec3393169eacd4c4cab5ae03dce9a))
* **migrations:** Migration file to create job opportunities use typeORM lib ([8bd022e](https://gitlab.com/olegario96/hireme/commit/8bd022eaaadd980c97bf91372d9fe0a65b0e5dfe))
* **migrations:** Populate skills table use typeORM lib ([ce51c4e](https://gitlab.com/olegario96/hireme/commit/ce51c4e06d195af9a725cce7975ed9fb70e144ac))
* **migrations:** TypeORM migration for users table ([99d6ccd](https://gitlab.com/olegario96/hireme/commit/99d6ccd689b2f8c1f01db48903aa97d29ef0a6e9))
* **migrations:** Use shared definition to create metafields ([a3516cd](https://gitlab.com/olegario96/hireme/commit/a3516cd11f064c7e800026359d30211187025cc7))
* **migrations:** Use text type for migrations ([5548376](https://gitlab.com/olegario96/hireme/commit/5548376bf2df6c526fa50425149970105e02c836))
* **release:** enable gitlab integration ([9dbe038](https://gitlab.com/olegario96/hireme/commit/9dbe038b6c53b87241bf264c226848a20feafa0a))
* **release:** improve config to manage release ([723f6e8](https://gitlab.com/olegario96/hireme/commit/723f6e83f2794b24a20b73101794fd904805a1ad))
* **root:** Add new dotenv files ([73b01d9](https://gitlab.com/olegario96/hireme/commit/73b01d9cdf051bb28c325bb65ec5cb050299ba8a))
* **root:** Add nodemon config file ([0cfb14c](https://gitlab.com/olegario96/hireme/commit/0cfb14c675e328d0ea614c33e47b353c2a62723d))
* **root:** Adds typeorm package ([66e506a](https://gitlab.com/olegario96/hireme/commit/66e506ad150b91013fb18069cf80dd0cbf5b1774))
* **scripts:** Bash file to run migrations ([2ca00d2](https://gitlab.com/olegario96/hireme/commit/2ca00d2ab2256ab3fafd805fbec69261e2e66b19))
* **scripts:** File to handle test execution logic ([db899ea](https://gitlab.com/olegario96/hireme/commit/db899ea951127bfd135593fbb236aedc4d5ca71e))
* **server:** Create entity file for login attempts ([3a4018b](https://gitlab.com/olegario96/hireme/commit/3a4018b4e2b731768888492be3b04de68304b6ed))
* **server:** Create model for login attempt ([35396a9](https://gitlab.com/olegario96/hireme/commit/35396a993b5ea2d45b364c69d898247792b4b3a8))
* **server:** Entity definition extends Metafields class ([b6c6782](https://gitlab.com/olegario96/hireme/commit/b6c67821c657ee8722fa86b742189aba28cec4b9))
* **server:** Initialize DB connection ([8f707b3](https://gitlab.com/olegario96/hireme/commit/8f707b305c4d87bc47856a52c455d7340e8543a4))
* **skill:** DAO class uses repository typeORM standard ([95f26cb](https://gitlab.com/olegario96/hireme/commit/95f26cb408ba3a930de278da7ba0422c18c5cf72))
* **skills:** Create entity file ([e63843e](https://gitlab.com/olegario96/hireme/commit/e63843ee9cf5d68b768d18cdb30e6910c26ecfa7))
* **skills:** Method to retrive proficiences ([a0ed0a7](https://gitlab.com/olegario96/hireme/commit/a0ed0a7a68f6dd5279db38a1b47db41f480da227))
* **tests:** Creates connection helper ([9bd3592](https://gitlab.com/olegario96/hireme/commit/9bd3592bca80d8c0ee60121700389ac79f063a0f))
* **types:** Annotation for login attempt ([3f161f1](https://gitlab.com/olegario96/hireme/commit/3f161f1235da3148343b05ebb9bfc349660c363c))
* **user:** Create entity file ([9879ed9](https://gitlab.com/olegario96/hireme/commit/9879ed9d272610583f6a628bd94e9e44310de4b8))
* **user:** DAO class implements typeORM standard ([857589d](https://gitlab.com/olegario96/hireme/commit/857589dd14d03a9bffaced986986f4d0f0c08421))
* **users:** Use typeORM abstraction to handle attributes ([8e9803b](https://gitlab.com/olegario96/hireme/commit/8e9803bdddda0833f7ff91063317a6803fd3dbea))

### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

#### [v0.3.2](https://gitlab.com/olegario96/hireme/compare/v0.3.1...v0.3.2)

- Revert "v0.4.0" [`#183`](https://gitlab.com/olegario96/hireme/merge_requests/183)
- fix: Minor improvements for .md files [`#181`](https://gitlab.com/olegario96/hireme/merge_requests/181)
- fix: Address no longer maintained packages [`#180`](https://gitlab.com/olegario96/hireme/merge_requests/180)
- feat: Upgrade NodeJS version [`#179`](https://gitlab.com/olegario96/hireme/merge_requests/179)
- feat: Linter for dockerfile [`#178`](https://gitlab.com/olegario96/hireme/merge_requests/178)
- feat: Ubuntu and Postgres update [`#177`](https://gitlab.com/olegario96/hireme/merge_requests/177)
- Dependabot config [`#176`](https://gitlab.com/olegario96/hireme/merge_requests/176)
- Tsyringe deps injection [`#175`](https://gitlab.com/olegario96/hireme/merge_requests/175)
- Bump Postgres version [`#174`](https://gitlab.com/olegario96/hireme/merge_requests/174)
- Gitlab SAST config [`#173`](https://gitlab.com/olegario96/hireme/merge_requests/173)
- fix(all): Address IDE error [`#172`](https://gitlab.com/olegario96/hireme/merge_requests/172)
- Endpoint to create messages [`#171`](https://gitlab.com/olegario96/hireme/merge_requests/171)
- Conversations route [`#170`](https://gitlab.com/olegario96/hireme/merge_requests/170)
- docs(root): Creates collapsible section for unit test [`#169`](https://gitlab.com/olegario96/hireme/merge_requests/169)
- chore(all): Use new standard to import middlewares [`#168`](https://gitlab.com/olegario96/hireme/merge_requests/168)
- Improves code coverage for E2E tests [`#167`](https://gitlab.com/olegario96/hireme/merge_requests/167)
- Rename middlewares [`#166`](https://gitlab.com/olegario96/hireme/merge_requests/166)
- Documentation for 2FA endpoints [`#165`](https://gitlab.com/olegario96/hireme/merge_requests/165)
- JWT Middleware returns internal server error on exception [`#164`](https://gitlab.com/olegario96/hireme/merge_requests/164)
- fix(deps): Address no longer maintained packages [`f797053`](https://gitlab.com/olegario96/hireme/commit/f797053c463c0e8c628c438a16a688402c689f70)
- feat(all): Tsyringe automatically injects dependencies [`43edb7f`](https://gitlab.com/olegario96/hireme/commit/43edb7f2fd8bddd63d1233ca899a2b2c55c6c6dc)
- docs(security): Swagger notation for 2FA endpoints [`0e32a61`](https://gitlab.com/olegario96/hireme/commit/0e32a61ca18e7b7b486c60fed581de99ceb0fcfb)

#### [v0.3.1](https://gitlab.com/olegario96/hireme/compare/v0.3.0...v0.3.1)

> 21 January 2022

- Swagger documentation for job opportunities endpoints [`#163`](https://gitlab.com/olegario96/hireme/merge_requests/163)
- Swagger documentation for FF endpoints [`#162`](https://gitlab.com/olegario96/hireme/merge_requests/162)
- Inline documentation with swagger [`#161`](https://gitlab.com/olegario96/hireme/merge_requests/161)
- chore(root): Adds swagger-inline as dev dependency [`651e47f`](https://gitlab.com/olegario96/hireme/commit/651e47fdc04806ecd1ef07a7932e52becc1d71a5)
- chore(swagger): Delete outdated documentation [`d5ed6c3`](https://gitlab.com/olegario96/hireme/commit/d5ed6c360f6bf4955bad637bd06f148e5e9a6608)
- docs(job-opportunities): Swagger documentation for endpoints [`a1c7af0`](https://gitlab.com/olegario96/hireme/commit/a1c7af0dc264451c0d3425c3ae4b114fb3da625f)

#### [v0.3.0](https://gitlab.com/olegario96/hireme/compare/v0.2.5...v0.3.0)

> 17 January 2022

- Fix release script [`#160`](https://gitlab.com/olegario96/hireme/merge_requests/160)
- Revert "v0.3.0" [`5019d9e`](https://gitlab.com/olegario96/hireme/commit/5019d9e58d1f52377102dd100b91e7929bf0c58e)
- chore(scripts): Version bump incremented on minor chunks [`6ab5af8`](https://gitlab.com/olegario96/hireme/commit/6ab5af82ecc6f30a9ee954fdb2beb492848e2104)

#### [v0.2.5](https://gitlab.com/olegario96/hireme/compare/v0.2.4...v0.2.5)

> 16 January 2022

- Context for 2FA routes [`#159`](https://gitlab.com/olegario96/hireme/merge_requests/159)
- Server recursively load routes [`#158`](https://gitlab.com/olegario96/hireme/merge_requests/158)
- Conversation controller logic [`#157`](https://gitlab.com/olegario96/hireme/merge_requests/157)
- Domain logic for messages [`#156`](https://gitlab.com/olegario96/hireme/merge_requests/156)
- Conversation controller [`#155`](https://gitlab.com/olegario96/hireme/merge_requests/155)
- Message controller [`#154`](https://gitlab.com/olegario96/hireme/merge_requests/154)
- Conversations unique index [`#153`](https://gitlab.com/olegario96/hireme/merge_requests/153)
- Automated test for ConversationDAO [`#152`](https://gitlab.com/olegario96/hireme/merge_requests/152)
- DAO date helper method [`#151`](https://gitlab.com/olegario96/hireme/merge_requests/151)
- DAO classes for conversations and messages [`#150`](https://gitlab.com/olegario96/hireme/merge_requests/150)
- Remove useless constructor from DAO classes [`#149`](https://gitlab.com/olegario96/hireme/merge_requests/149)
- Migration messages table [`#148`](https://gitlab.com/olegario96/hireme/merge_requests/148)
- Migrations refactoring [`#147`](https://gitlab.com/olegario96/hireme/merge_requests/147)
- Migration conversation [`#146`](https://gitlab.com/olegario96/hireme/merge_requests/146)
- Adds indexes for metada [`#145`](https://gitlab.com/olegario96/hireme/merge_requests/145)
- Revert "v0.2.4" [`#144`](https://gitlab.com/olegario96/hireme/merge_requests/144)
- Revert "v0.2.5" [`a8d00b7`](https://gitlab.com/olegario96/hireme/commit/a8d00b7d9336629a216768273355a64032354c38)
- chore(migrations): Refactor all DB schema changes to properly handle indexes [`77ec425`](https://gitlab.com/olegario96/hireme/commit/77ec425f0d2a315c8d48b04e8a58a4c241c732b7)
- chore(all): DAO classes use internal helper method to compute date [`984f54c`](https://gitlab.com/olegario96/hireme/commit/984f54cf69447079649b99f72f539a0039a57886)

#### [v0.2.4](https://gitlab.com/olegario96/hireme/compare/v0.2.3...v0.2.4)

> 2 January 2022

- chore(ci): Automatically release new version [`#143`](https://gitlab.com/olegario96/hireme/merge_requests/143)
- Revert "v0.2.4" [`ee7e6de`](https://gitlab.com/olegario96/hireme/commit/ee7e6dec89e0e3428b203d11724864bd082fdb99)

#### [v0.2.3](https://gitlab.com/olegario96/hireme/compare/v0.2.1...v0.2.3)

> 1 January 2022

- Documentation for best practices related to automated tests [`#142`](https://gitlab.com/olegario96/hireme/merge_requests/142)
- Shared validation for internal token at schema level [`#140`](https://gitlab.com/olegario96/hireme/merge_requests/140)
- Feature Flags seed procedure [`#141`](https://gitlab.com/olegario96/hireme/merge_requests/141)
- fix(users): Address incorrect text attribute [`#139`](https://gitlab.com/olegario96/hireme/merge_requests/139)
- Endpoint to rollout FFs [`#138`](https://gitlab.com/olegario96/hireme/merge_requests/138)
- DB custom query config [`#137`](https://gitlab.com/olegario96/hireme/merge_requests/137)
- Pre Authorized Token middleware [`#136`](https://gitlab.com/olegario96/hireme/merge_requests/136)
- NodeJS version upgrade [`#135`](https://gitlab.com/olegario96/hireme/merge_requests/135)
- test(feature-flags): E2E automated test [`0ec1201`](https://gitlab.com/olegario96/hireme/commit/0ec1201e9a9b7c2b80be3486d72f9006ea8f41cc)
- chore(all): Use new query config type [`fdc1edc`](https://gitlab.com/olegario96/hireme/commit/fdc1edcf610ca702bd2e14fb4c2e48d2d2621993)
- test(middlewares): Automated test for new middleware [`b51a4f8`](https://gitlab.com/olegario96/hireme/commit/b51a4f86011ea157674b49b77d64777c6b494a66)

#### [v0.2.1](https://gitlab.com/olegario96/hireme/compare/v0.2.0...v0.2.1)

> 24 October 2021

- Users skills dao [`#134`](https://gitlab.com/olegario96/hireme/merge_requests/134)
- Users skills migration [`#133`](https://gitlab.com/olegario96/hireme/merge_requests/133)
- feat(users): DAO class to manage skills for users [`faf5053`](https://gitlab.com/olegario96/hireme/commit/faf505397f3400b071d3d6d7bd7e952a8db77aaa)
- test(users): Automated test for new DAO class [`8d81f1f`](https://gitlab.com/olegario96/hireme/commit/8d81f1f89521c80058b7ee616b9d4c2fd11f9a17)
- feat(users): DAO fetches user along with its skills [`ab6bbd9`](https://gitlab.com/olegario96/hireme/commit/ab6bbd93a0d2af0fab1e88cb790d2ddb5f1530cc)

#### [v0.2.0](https://gitlab.com/olegario96/hireme/compare/v0.1.9...v0.2.0)

> 10 October 2021

- JWT middleware looks for the Bearer word [`#132`](https://gitlab.com/olegario96/hireme/merge_requests/132)
- E2E review [`#131`](https://gitlab.com/olegario96/hireme/merge_requests/131)
- PUT /user/:id [`#130`](https://gitlab.com/olegario96/hireme/merge_requests/130)
- test(all): Automated tests use strict check [`#129`](https://gitlab.com/olegario96/hireme/merge_requests/129)
- UpdatedAt is properly handled by DAOs [`#128`](https://gitlab.com/olegario96/hireme/merge_requests/128)
- chore(job-opportunities): Folder now follow standard for other records names [`#118`](https://gitlab.com/olegario96/hireme/merge_requests/118)
- FeatureFlagDao class [`#117`](https://gitlab.com/olegario96/hireme/merge_requests/117)
- Feature flag migrations [`#116`](https://gitlab.com/olegario96/hireme/merge_requests/116)
- Tests review [`#115`](https://gitlab.com/olegario96/hireme/merge_requests/115)
- docs(core-values): First review [`#114`](https://gitlab.com/olegario96/hireme/merge_requests/114)
- chore(common): Global const for authorization [`#113`](https://gitlab.com/olegario96/hireme/merge_requests/113)
- chore(all): Use table as prefix for every table name [`30ee213`](https://gitlab.com/olegario96/hireme/commit/30ee213eab61e86331400876a8759db12c853a38)
- test(feature-flags): Automated test for DAO class. WIP [`de7a75e`](https://gitlab.com/olegario96/hireme/commit/de7a75ef0190c372d018310df9e7b4f74a4f6748)
- feat(feature-flags): DAO class to manage FF [`677e402`](https://gitlab.com/olegario96/hireme/commit/677e402cf862b3b383f07c7660f475bbce2cc137)

#### [v0.1.9](https://gitlab.com/olegario96/hireme/compare/v0.1.8...v0.1.9)

> 29 August 2021

- Endponit to reset password [`#112`](https://gitlab.com/olegario96/hireme/merge_requests/112)
- Query debugger actually prints raw results [`#111`](https://gitlab.com/olegario96/hireme/merge_requests/111)
- test(users): E2E test for reset password route [`29663d2`](https://gitlab.com/olegario96/hireme/commit/29663d2a8ff3e373bfdff9494ad3ead6b6014232)
- test(users): Use better context for test to reset password [`ff37bfa`](https://gitlab.com/olegario96/hireme/commit/ff37bfab9f773ebc7ea50cafd5d6dfa3df81af56)
- test(users): Integration tests for controller [`f472f42`](https://gitlab.com/olegario96/hireme/commit/f472f42dcaf6b600e715b3533d611f1d5353b56e)

#### [v0.1.8](https://gitlab.com/olegario96/hireme/compare/v0.1.7...v0.1.8)

> 24 August 2021

- `POST /authenticate` uses login attempt middleware [`#110`](https://gitlab.com/olegario96/hireme/merge_requests/110)
- Login attempts middleware [`#109`](https://gitlab.com/olegario96/hireme/merge_requests/109)
- fix(job-opportunity): Use more generic check to avoid flakiness behaviour [`#108`](https://gitlab.com/olegario96/hireme/merge_requests/108)
- Login attempts DAO [`#107`](https://gitlab.com/olegario96/hireme/merge_requests/107)
- chore(migrations): login_attempts table migrations to improve queries performance [`#106`](https://gitlab.com/olegario96/hireme/merge_requests/106)
- Login attempts table [`#105`](https://gitlab.com/olegario96/hireme/merge_requests/105)
- fix(migrations): Uses appropriate way to create indexes [`#104`](https://gitlab.com/olegario96/hireme/merge_requests/104)
- Enum for HTTP status codes [`#103`](https://gitlab.com/olegario96/hireme/merge_requests/103)
- chore(root): Create option to compile with watcher [`#102`](https://gitlab.com/olegario96/hireme/merge_requests/102)
- Create core values [`#101`](https://gitlab.com/olegario96/hireme/merge_requests/101)
- Improve configs for collecting test code coverage [`#100`](https://gitlab.com/olegario96/hireme/merge_requests/100)
- Linter rule to sort imports [`#99`](https://gitlab.com/olegario96/hireme/merge_requests/99)
- chore(all): Files uses enums for Http status codes [`e6d65e5`](https://gitlab.com/olegario96/hireme/commit/e6d65e501acbae184420ec73daa85a6b75dd4983)
- test(middlewares): Automated  test for login attempts middleware [`a01296f`](https://gitlab.com/olegario96/hireme/commit/a01296fb19a50463688d38347e46ae2745de30e4)
- fix(all): Ensure all files are respecting new linter rule [`e0765a0`](https://gitlab.com/olegario96/hireme/commit/e0765a0401be94ab8cfa235bccd8127b38ea41f8)

#### [v0.1.7](https://gitlab.com/olegario96/hireme/compare/v0.1.6...v0.1.7)

> 7 August 2021

- Routes to enable 2FA [`#98`](https://gitlab.com/olegario96/hireme/merge_requests/98)
- Create shared global const for content-type [`#97`](https://gitlab.com/olegario96/hireme/merge_requests/97)
- fix(all): Remove useless linter rules [`#96`](https://gitlab.com/olegario96/hireme/merge_requests/96)
- Allows user to disable 2FA [`#95`](https://gitlab.com/olegario96/hireme/merge_requests/95)
- 2FA check [`#94`](https://gitlab.com/olegario96/hireme/merge_requests/94)
- Controller for 2FA [`#93`](https://gitlab.com/olegario96/hireme/merge_requests/93)
- Two fa refactoring [`#92`](https://gitlab.com/olegario96/hireme/merge_requests/92)
- chore(migrations): Add index to deleted_at field for two_fa_tokens table [`#91`](https://gitlab.com/olegario96/hireme/merge_requests/91)
- TwoFaDao class [`#90`](https://gitlab.com/olegario96/hireme/merge_requests/90)
- feat(migrations): Migration to store 2FA tokens [`#89`](https://gitlab.com/olegario96/hireme/merge_requests/89)
- 2FA Class [`#88`](https://gitlab.com/olegario96/hireme/merge_requests/88)
- chore(migrations): Migration to index deleted_at fields [`#87`](https://gitlab.com/olegario96/hireme/merge_requests/87)
- Queries use deleted_at field to execute operations [`#86`](https://gitlab.com/olegario96/hireme/merge_requests/86)
- Job opportunity handler [`#85`](https://gitlab.com/olegario96/hireme/merge_requests/85)
- Seed procedure for Job Opportunity [`#84`](https://gitlab.com/olegario96/hireme/merge_requests/84)
- Seed return [`#83`](https://gitlab.com/olegario96/hireme/merge_requests/83)
- Script to generate dotenv file [`#82`](https://gitlab.com/olegario96/hireme/merge_requests/82)
- Shared function for E2E tests [`#81`](https://gitlab.com/olegario96/hireme/merge_requests/81)
- Renames test environment to ci [`#80`](https://gitlab.com/olegario96/hireme/merge_requests/80)
- Creates raw type for job opportunity requests [`#79`](https://gitlab.com/olegario96/hireme/merge_requests/79)
- test(security): E2E test for two-fa routes [`eff3c8d`](https://gitlab.com/olegario96/hireme/commit/eff3c8dade00ff84e51ef53067bdbe8bbde3f5b9)
- chore(root): Add packages to manage 2FA [`bc651fc`](https://gitlab.com/olegario96/hireme/commit/bc651fc418465626e80d790949fe40520817c149)
- tests(security): E2E test for 2FA [`e7defa8`](https://gitlab.com/olegario96/hireme/commit/e7defa861729ff82d732ab13391b2022964eaf89)

#### [v0.1.6](https://gitlab.com/olegario96/hireme/compare/v0.1.5...v0.1.6)

> 28 June 2021

- Job opportunity schema validation [`#78`](https://gitlab.com/olegario96/hireme/merge_requests/78)
- Use docker to host Postgres instance while running tests [`#77`](https://gitlab.com/olegario96/hireme/merge_requests/77)
- Job opportunity controller [`#76`](https://gitlab.com/olegario96/hireme/merge_requests/76)
- chore(users): Deletes update method for DAO [`#75`](https://gitlab.com/olegario96/hireme/merge_requests/75)
- test(job-opportunity): Integration test for JobOpportunityDao [`6e347c5`](https://gitlab.com/olegario96/hireme/commit/6e347c54289a61cb55360a3d20f8ba4edd8b5eb7)
- chore(scripts): Use docker image to host postgres for tests execution [`48c1ef1`](https://gitlab.com/olegario96/hireme/commit/48c1ef1f2e60ea763eb9edcd3125c3563a2880d9)
- feat(job-opportunity): Controller class [`259004e`](https://gitlab.com/olegario96/hireme/commit/259004e859e9548d733670f579c946c6a338d8cc)

#### [v0.1.5](https://gitlab.com/olegario96/hireme/compare/v0.1.4...v0.1.5)

> 2 May 2021

- fix(all): Ensure Dao consider deletedAt field on read queries [`#74`](https://gitlab.com/olegario96/hireme/merge_requests/74)
- JobOpportunityDao [`#73`](https://gitlab.com/olegario96/hireme/merge_requests/73)
- Fix for identity reset [`#72`](https://gitlab.com/olegario96/hireme/merge_requests/72)
- chore(skills): Fetch records by array of ids [`#71`](https://gitlab.com/olegario96/hireme/merge_requests/71)
- chore(docker): Use less docker commands to build image [`#70`](https://gitlab.com/olegario96/hireme/merge_requests/70)
- SkillDao uses same interface to create records [`#69`](https://gitlab.com/olegario96/hireme/merge_requests/69)
- Job opportunity skill migration [`#68`](https://gitlab.com/olegario96/hireme/merge_requests/68)
- Job opportunity migration [`#67`](https://gitlab.com/olegario96/hireme/merge_requests/67)
- test(job-opporutnity): Automated test for JobOpportunityDao [`f363d0f`](https://gitlab.com/olegario96/hireme/commit/f363d0f522b51480fe8aa86868269667b48e41ab)
- fix(all): SkillDao uses same interface as otherr Dao classes [`94aaf66`](https://gitlab.com/olegario96/hireme/commit/94aaf66e8cbd4d6cd1518fe6be2665825bcfcde3)
- feat(job-opportunity): DAO method to create JobOpportunity [`9a218a4`](https://gitlab.com/olegario96/hireme/commit/9a218a4f8e0848dade6a6f3c9d5ccde380b9e02e)

#### [v0.1.4](https://gitlab.com/olegario96/hireme/compare/v0.1.3...v0.1.4)

> 11 April 2021

- Populate skills [`#66`](https://gitlab.com/olegario96/hireme/merge_requests/66)
- Seed filter [`#65`](https://gitlab.com/olegario96/hireme/merge_requests/65)
- fix(tests): Address typo [`#64`](https://gitlab.com/olegario96/hireme/merge_requests/64)
- feat(migrations): Migration to populate skills table [`4b26bcb`](https://gitlab.com/olegario96/hireme/commit/4b26bcb2adf12ba2da40a09e0be769486dd72808)
- feat(seed): Ensure right environments for seed procedures [`967f8c6`](https://gitlab.com/olegario96/hireme/commit/967f8c6530c43b596a6c87f9fc5cb4eb29950fee)
- feat(seed): Function to assert environment [`1be4574`](https://gitlab.com/olegario96/hireme/commit/1be457422a1198052060d506596f3cfd1f3e9b96)

#### [v0.1.3](https://gitlab.com/olegario96/hireme/compare/v0.1.2...v0.1.3)

> 9 April 2021

- chore(all): Common procedure to clean tables [`#63`](https://gitlab.com/olegario96/hireme/merge_requests/63)

#### [v0.1.2](https://gitlab.com/olegario96/hireme/compare/v0.1.1...v0.1.2)

> 8 April 2021

- Skills [`#62`](https://gitlab.com/olegario96/hireme/merge_requests/62)
- feat(seed): Seed procedure for companies [`#61`](https://gitlab.com/olegario96/hireme/merge_requests/61)
- test(skills): Integration test for DAO [`db9f80d`](https://gitlab.com/olegario96/hireme/commit/db9f80d6bed37ad34e627326a767596a34bd2840)
- feat(skills): DAO Method to create skills [`d410e79`](https://gitlab.com/olegario96/hireme/commit/d410e7971f7b7920c8437433066322e6678de674)
- feat(migrations): Migration for skills table [`feaf4c5`](https://gitlab.com/olegario96/hireme/commit/feaf4c504338a8b71ca2dce89049982d30cf6d73)

#### [v0.1.1](https://gitlab.com/olegario96/hireme/compare/v0.1.0...v0.1.1)

> 8 April 2021

- chore(root): Create huskyrc file [`#60`](https://gitlab.com/olegario96/hireme/merge_requests/60)
- chore(docker): Remove useless comments from dockerfile [`#59`](https://gitlab.com/olegario96/hireme/merge_requests/59)
- DAO class for companies along with migration [`#58`](https://gitlab.com/olegario96/hireme/merge_requests/58)
- Migration for companies table [`#57`](https://gitlab.com/olegario96/hireme/merge_requests/57)
- docs(root): Minor improvements for README.md [`#56`](https://gitlab.com/olegario96/hireme/merge_requests/56)
- chore(security): Delete token verification [`#54`](https://gitlab.com/olegario96/hireme/merge_requests/54)
- chore(root): Creates .nvmrc file [`#55`](https://gitlab.com/olegario96/hireme/merge_requests/55)
- Jwt middleware [`#53`](https://gitlab.com/olegario96/hireme/merge_requests/53)
- Joi validation middleware [`#52`](https://gitlab.com/olegario96/hireme/merge_requests/52)
- Middlewares [`#51`](https://gitlab.com/olegario96/hireme/merge_requests/51)
- test(middleware): Automated test for JwtMiddleware [`5e8a579`](https://gitlab.com/olegario96/hireme/commit/5e8a5791fa73d0e6ea8a568b1f99f133a019a6aa)
- feat(companies): Crete CompanyDao [`0a21e61`](https://gitlab.com/olegario96/hireme/commit/0a21e61d286ef17ef4fc49aeaa71fac0426f3ce3)
- feat(middlewares): Creates JwtMiddleware [`9bcfa82`](https://gitlab.com/olegario96/hireme/commit/9bcfa82c1c7c13434fdf3a720e2d8214d30ea5c3)

#### [v0.1.0](https://gitlab.com/olegario96/hireme/compare/v0.0.17...v0.1.0)

> 26 March 2021

- Internal server error [`#50`](https://gitlab.com/olegario96/hireme/merge_requests/50)
- Route to create user [`#49`](https://gitlab.com/olegario96/hireme/merge_requests/49)
- Enable E2E tests on CI [`#48`](https://gitlab.com/olegario96/hireme/merge_requests/48)
- Dev can see API documentation on swagger [`#47`](https://gitlab.com/olegario96/hireme/merge_requests/47)
- chore(all): Remove db parameter from seed down [`#46`](https://gitlab.com/olegario96/hireme/merge_requests/46)
- Body parser [`#45`](https://gitlab.com/olegario96/hireme/merge_requests/45)
- Test authenticate [`#44`](https://gitlab.com/olegario96/hireme/merge_requests/44)
- Gitlab ci improvement [`#43`](https://gitlab.com/olegario96/hireme/merge_requests/43)
- docs(root): Add basic configuration for swagger [`d682367`](https://gitlab.com/olegario96/hireme/commit/d682367b98403f28330c0b44298098c7bb96b97d)
- docs(swagger): Remove outdated content from main file [`357ad69`](https://gitlab.com/olegario96/hireme/commit/357ad69a0b35d0a4a40ce9c3fe8da01add0d5ecb)
- chore(root): Remove unrelated dependencies [`62ba191`](https://gitlab.com/olegario96/hireme/commit/62ba191aff29977b7760d7c9b96a794515073681)

#### [v0.0.17](https://gitlab.com/olegario96/hireme/compare/v0.0.16...v0.0.17)

> 21 March 2021

- Validate user for creation [`#42`](https://gitlab.com/olegario96/hireme/merge_requests/42)
- chore(logger): Use different methods to log warn, info and error [`#41`](https://gitlab.com/olegario96/hireme/merge_requests/41)
- Tsyringe [`#40`](https://gitlab.com/olegario96/hireme/merge_requests/40)
- Test configuration for unit and integration tests [`#39`](https://gitlab.com/olegario96/hireme/merge_requests/39)
- Authenticate route [`#37`](https://gitlab.com/olegario96/hireme/merge_requests/37)
- chore(root): Ensure seed folder won't be available on dist [`#36`](https://gitlab.com/olegario96/hireme/merge_requests/36)
- Server setup [`#35`](https://gitlab.com/olegario96/hireme/merge_requests/35)
- feat(commons): Function to check if given string is file [`#34`](https://gitlab.com/olegario96/hireme/merge_requests/34)
- chore(security): Creates JsonWebToken type [`#33`](https://gitlab.com/olegario96/hireme/merge_requests/33)
- chore(all): Use tsyringe to manage singleton design pattern [`963b620`](https://gitlab.com/olegario96/hireme/commit/963b620772484f12a9d8198c28769640ef93ffd9)
- chore(root): Add express and express-router to dependency list [`cf01c68`](https://gitlab.com/olegario96/hireme/commit/cf01c68b695d8b8acc64211f2d7c997d25cbf958)
- chore(all): Use jest config for unit tests [`8c838fa`](https://gitlab.com/olegario96/hireme/commit/8c838fa0ad662fcb6f5b63cc1babd50fc623d783)

#### [v0.0.16](https://gitlab.com/olegario96/hireme/compare/v0.0.15...v0.0.16)

> 28 February 2021

- Use bunyan instead of console.log [`#32`](https://gitlab.com/olegario96/hireme/merge_requests/32)
- chore(root): Add bunyan as dependency [`9bfd6f2`](https://gitlab.com/olegario96/hireme/commit/9bfd6f295b5bf2eed01474978e621ff241e3fb75)
- test(logger): Tweak automated test to use bunyan [`c3788cf`](https://gitlab.com/olegario96/hireme/commit/c3788cf07ddc0eef998cf8c9830b6e66450f96be)
- feat(logger): Use bunyan to manage logs [`2080909`](https://gitlab.com/olegario96/hireme/commit/2080909364c38aeaad86b6b472bb1c5d829a43a2)

#### [v0.0.15](https://gitlab.com/olegario96/hireme/compare/v0.0.14...v0.0.15)

> 18 February 2021

- Authenticate user [`#31`](https://gitlab.com/olegario96/hireme/merge_requests/31)
- feat(users): Method to authenticate user [`4cb65be`](https://gitlab.com/olegario96/hireme/commit/4cb65bec2cf8829ab04a724eff11ee1d79a7d15f)
- chore(seed): Use controller to populate users table [`e76a5fb`](https://gitlab.com/olegario96/hireme/commit/e76a5fb98cea06723d44b90aa92fe906b0f809ce)
- feat(security): Method to validate user password [`f9527a1`](https://gitlab.com/olegario96/hireme/commit/f9527a1908ff4fc91afb342064496bc74875af0c)

#### [v0.0.14](https://gitlab.com/olegario96/hireme/compare/v0.0.13...v0.0.14)

> 17 February 2021

- Add salt to user password [`#30`](https://gitlab.com/olegario96/hireme/merge_requests/30)
- User controller [`#29`](https://gitlab.com/olegario96/hireme/merge_requests/29)
- Use eslint instead of tslint [`#28`](https://gitlab.com/olegario96/hireme/merge_requests/28)
- chore(root): Use eslint instead of tslint [`65a9af1`](https://gitlab.com/olegario96/hireme/commit/65a9af1ede069344037c90f987cf3d751108f2e6)
- chore(all): Use eslint insted of tslint [`b1f9528`](https://gitlab.com/olegario96/hireme/commit/b1f9528069e62631644ad228ddcbfad5a41c69e6)
- test(users): Automated test for UserController [`96565e8`](https://gitlab.com/olegario96/hireme/commit/96565e8d64703bb2d0861739742ab8e7d429bd26)

#### [v0.0.13](https://gitlab.com/olegario96/hireme/compare/v0.0.12...v0.0.13)

> 4 February 2021

- Class to sign and verify JWT [`#27`](https://gitlab.com/olegario96/hireme/merge_requests/27)
- Editorconfig [`#26`](https://gitlab.com/olegario96/hireme/merge_requests/26)
- feat(root): Add jsonwebtoken as dependecy [`7d394b8`](https://gitlab.com/olegario96/hireme/commit/7d394b8a76c180dd77a3349cd6681c5b3e931b35)
- feat(security): Class to sign and verify Jwt [`a227bf3`](https://gitlab.com/olegario96/hireme/commit/a227bf3facac8e058147d8b6a0db8dc0717c3127)
- feat(root): Use editorconfig to ensure standard for formatting files [`2aefaec`](https://gitlab.com/olegario96/hireme/commit/2aefaec392cf5905b5216c958ba1fdf116c2b8b6)

#### [v0.0.12](https://gitlab.com/olegario96/hireme/compare/v0.0.11...v0.0.12)

> 4 February 2021

- Hash [`#25`](https://gitlab.com/olegario96/hireme/merge_requests/25)
- feat(security): Use SHA-256 to hash passwords [`b3de593`](https://gitlab.com/olegario96/hireme/commit/b3de593b0368658237ebbbc1816fb93febcc6b51)
- chore(user): Rename method to validateAttributes [`3c4f6ed`](https://gitlab.com/olegario96/hireme/commit/3c4f6ed0cae1136dba038064284629ce255793cc)
- chore(users): Remove unrelated changes [`b084e74`](https://gitlab.com/olegario96/hireme/commit/b084e7400f1160d26cd0cd6a9953315f8a4f15e3)

#### [v0.0.11](https://gitlab.com/olegario96/hireme/compare/v0.0.10...v0.0.11)

> 3 February 2021

- Cipher [`#24`](https://gitlab.com/olegario96/hireme/merge_requests/24)
- feat(security): Class to encrypt and decrypt strings [`0d8700a`](https://gitlab.com/olegario96/hireme/commit/0d8700af21b232e1c5458e6b4de8367f5a811745)
- test(security): Automated test for Cipher [`0542ab7`](https://gitlab.com/olegario96/hireme/commit/0542ab7ca3ea9f061478d294e19cc234be1fcc8d)
- feat(root): Add configs for AES [`09edde2`](https://gitlab.com/olegario96/hireme/commit/09edde2916aa596bee2132d65e08415f297f1e37)

#### [v0.0.10](https://gitlab.com/olegario96/hireme/compare/v0.0.9...v0.0.10)

> 3 February 2021

- Remove dotenv-safe dependency [`#23`](https://gitlab.com/olegario96/hireme/merge_requests/23)
- feat(config): Ensure .env and .env.example are synced [`f1be712`](https://gitlab.com/olegario96/hireme/commit/f1be712457b2f48fd8cb4318edf58af4b4ccc17e)
- feat(config): Load environment variables with custom implementation [`39fef38`](https://gitlab.com/olegario96/hireme/commit/39fef386c09165454025ff15a827550d000f3a25)
- chore(root): Remove dotenv-safe package [`08e908b`](https://gitlab.com/olegario96/hireme/commit/08e908bb821f837fe1951c4ced2c71a7cc8ce38c)

#### [v0.0.9](https://gitlab.com/olegario96/hireme/compare/v0.0.8...v0.0.9)

> 1 February 2021

- feat(users): DAO method to soft delete records [`#22`](https://gitlab.com/olegario96/hireme/merge_requests/22)

#### [v0.0.8](https://gitlab.com/olegario96/hireme/compare/v0.0.7...v0.0.8)

> 1 February 2021

- User domain logic [`#21`](https://gitlab.com/olegario96/hireme/merge_requests/21)
- feat(users): Method to validate user object [`fcabe3b`](https://gitlab.com/olegario96/hireme/commit/fcabe3b50ab4621fc8a379336912cd09be3f67ad)
- chore(users): Rename files to singular [`0be4a16`](https://gitlab.com/olegario96/hireme/commit/0be4a16586d0e0d607f46ab7d52f64ab44120f8c)
- chore(common): Rename validation metho do getInvalidAttributes [`2cb4579`](https://gitlab.com/olegario96/hireme/commit/2cb4579cbdc540816901d1d7da15011a089fffb8)

#### [v0.0.7](https://gitlab.com/olegario96/hireme/compare/v0.0.6...v0.0.7)

> 1 February 2021

- Add meta column to users table [`#20`](https://gitlab.com/olegario96/hireme/merge_requests/20)
- chore(users): Add metafields to queries [`6b7b0f2`](https://gitlab.com/olegario96/hireme/commit/6b7b0f2c3fe159141825eb2af434f9e16df620df)
- feat(migrations): Metafields for users table [`97aae83`](https://gitlab.com/olegario96/hireme/commit/97aae83d0b8d2cba82fbbb5e8b4307e32293f0e9)
- feat(database): Function to translate timestmap to unix epoch [`0f390e4`](https://gitlab.com/olegario96/hireme/commit/0f390e4a359213b909868f0fcd3c8fda2fe89767)

#### [v0.0.6](https://gitlab.com/olegario96/hireme/compare/v0.0.5...v0.0.6)

> 31 January 2021

- UsersDAO return null when user is not found [`#19`](https://gitlab.com/olegario96/hireme/merge_requests/19)
- chore(users): Return null when user is not found [`17c4a7e`](https://gitlab.com/olegario96/hireme/commit/17c4a7e1abd1d25c6170d9ce68cbb2c9948fc736)
- chore(all): Rename types to PersitedUser [`bdb657c`](https://gitlab.com/olegario96/hireme/commit/bdb657c96f49d9fa46fca000902648385c9d76ae)
- fix(users): Address linter issues [`989e75c`](https://gitlab.com/olegario96/hireme/commit/989e75c91ecc92fdc77d16cfb6826ed0d1c85da9)

#### [v0.0.5](https://gitlab.com/olegario96/hireme/compare/v0.0.4...v0.0.5)

> 31 January 2021

- DAO method to update user [`#18`](https://gitlab.com/olegario96/hireme/merge_requests/18)
- Database utils [`#17`](https://gitlab.com/olegario96/hireme/merge_requests/17)
- fix(seed): Use appropriate value for override [`9cb41ba`](https://gitlab.com/olegario96/hireme/commit/9cb41ba890f4d9185bbb7a1003727843a1d41586)
- feat(seed): Allows override to default seed [`10cb7d1`](https://gitlab.com/olegario96/hireme/commit/10cb7d1969aae5d561508961dd3201d50d35e69f)
- feat(database): Creates function to parse camel case to snake case [`7c54013`](https://gitlab.com/olegario96/hireme/commit/7c54013c86c9f0f712e2a4b6b1f47c3b5936caf0)

#### [v0.0.4](https://gitlab.com/olegario96/hireme/compare/v0.0.3...v0.0.4)

> 27 January 2021

- Automated test for UsersDao [`#16`](https://gitlab.com/olegario96/hireme/merge_requests/16)
- feat(root): Add pg-native as depedency [`fb4d8ec`](https://gitlab.com/olegario96/hireme/commit/fb4d8ec21fcde32a15c120759a613ac7ca09da7b)
- test(users): Automated test for UsersDao [`864dd99`](https://gitlab.com/olegario96/hireme/commit/864dd99fbc6b398563692bd9a98d399f67d56e65)
- fix(seed): Use new query format [`70f371d`](https://gitlab.com/olegario96/hireme/commit/70f371d1b199cc9ff2cec87e19bc345c28c83278)

#### [v0.0.3](https://gitlab.com/olegario96/hireme/compare/v0.0.2...v0.0.3)

> 26 January 2021

- Database seed [`#15`](https://gitlab.com/olegario96/hireme/merge_requests/15)
- feat(seed): Creates seed command along with basic instructions [`b258ce6`](https://gitlab.com/olegario96/hireme/commit/b258ce6494ae7b2be7a34eb365cd6ab999b05861)
- feat(root): Add ts-node as dev dependency [`b3442f6`](https://gitlab.com/olegario96/hireme/commit/b3442f6a16706bda10a48d208cedd7fbd47e1cb7)
- chore(loger): Use early return instead of break [`c30cac0`](https://gitlab.com/olegario96/hireme/commit/c30cac077ebbd2b5e3e99d056396c54dc7d6ad3e)

#### [v0.0.2](https://gitlab.com/olegario96/hireme/compare/v0.0.1...v0.0.2)

> 26 January 2021

- Users DAO class [`#14`](https://gitlab.com/olegario96/hireme/merge_requests/14)
- Config improvement [`#13`](https://gitlab.com/olegario96/hireme/merge_requests/13)
- Postgres Database common class [`#12`](https://gitlab.com/olegario96/hireme/merge_requests/12)
- DB Migration [`#11`](https://gitlab.com/olegario96/hireme/merge_requests/11)
- Docker configuration [`#10`](https://gitlab.com/olegario96/hireme/merge_requests/10)
- fix(root): Remove unecessary parathenses [`#9`](https://gitlab.com/olegario96/hireme/merge_requests/9)
- fix(root): Allow bump version only on master [`#8`](https://gitlab.com/olegario96/hireme/merge_requests/8)
- chore(root): Ensure coverage is collected from right files [`#5`](https://gitlab.com/olegario96/hireme/merge_requests/5)
- Automated changelog [`#7`](https://gitlab.com/olegario96/hireme/merge_requests/7)
- feat(root): Add db-migrate as dependency [`c4ac01e`](https://gitlab.com/olegario96/hireme/commit/c4ac01ea8a2f10631e6a280cea0f48e9ecd1c59c)
- feat(root): Add db-migrate-pg as dependency [`91dcf5b`](https://gitlab.com/olegario96/hireme/commit/91dcf5b1cf8d5331cd3c5b54cf937a9600b61b9a)
- feat(users): DAO class [`4421a1c`](https://gitlab.com/olegario96/hireme/commit/4421a1c83b5cc8046af273e813aeaf29367d2a26)

#### v0.0.1

> 20 January 2021

- feat(root): Add husky as dev dependency [`#6`](https://gitlab.com/olegario96/hireme/merge_requests/6)
- Gitlab CI setup [`#4`](https://gitlab.com/olegario96/hireme/merge_requests/4)
- Jest setup [`#3`](https://gitlab.com/olegario96/hireme/merge_requests/3)
- Create class to manage logs for API [`#1`](https://gitlab.com/olegario96/hireme/merge_requests/1)
- Typescript basic settup [`#2`](https://gitlab.com/olegario96/hireme/merge_requests/2)
- feat(root): Add jest dependency [`9d1f4cd`](https://gitlab.com/olegario96/hireme/commit/9d1f4cd3f09e3a9e172918842ecb97f89bcd3b99)
- feat(root): Setup for jest [`cbddc6d`](https://gitlab.com/olegario96/hireme/commit/cbddc6d594f5e823b032bc53ba45049e4c70f19d)
- feat(root): Typescript basic configuration [`c6f4a95`](https://gitlab.com/olegario96/hireme/commit/c6f4a956689fcb29831214a83920fc74d6f7503a)
