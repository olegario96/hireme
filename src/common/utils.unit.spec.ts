import { isFile } from './utils';

describe('Utils', () => {
  describe('isFile', () => {
    describe('when file is provided', () => {
      it('should return true', () => {
        const file = 'hireme.log';
        expect(isFile(file)).toStrictEqual(true);
      });
    });
  });
});
