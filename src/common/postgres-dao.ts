import { container } from 'tsyringe';

import { PostgresDatabase } from '../database/postgres-database';
import { dateToUnixEpoch } from '../database/utils';
import { DateTimestamp } from './types';

export class PostgresDao {
  protected readonly db: PostgresDatabase;

  constructor() {
    this.db = container.resolve(PostgresDatabase);
  }

  protected getDateTimestamp(): DateTimestamp {
    const currentDate = new Date();
    const timestamp = currentDate.toISOString();
    const unixEpoch = dateToUnixEpoch(currentDate);
    return { timestamp, unixEpoch };
  }
}
