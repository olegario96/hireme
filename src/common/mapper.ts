export abstract class Mapper {
  public abstract map<T>(record: Record<string, unknown>): T;
}
