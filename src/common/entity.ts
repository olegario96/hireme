import { CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from 'typeorm';

import { Company, PersistedCompany } from '../companies/entity';
import { LoginAttempt, PersistedLoginAttempt } from '../server/entity';
import { PersistedSkill, Skill } from '../skills/entity';
import { PersistedUser, User } from '../users/entity';

type Entity = Company | LoginAttempt | Skill | User;
type PersistedEntity = PersistedCompany | PersistedLoginAttempt | PersistedSkill | PersistedUser;

export class MetaFields {
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at', nullable: true })
  deletedAt: Date | null;

  static from(record: Entity): PersistedEntity {
    return { ...record } as PersistedEntity;
  }
}
