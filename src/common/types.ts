export type UnixEpoch = number;

export interface MetaFields {
  createdAt: UnixEpoch;

  updatedAt: UnixEpoch;

  deletedAt: UnixEpoch | null;
}

export enum HttpStatusCode {
  OK = 200,
  NO_CONTENT = 204,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  CONFLICT = 409,
  TOO_MANY_REQUESTS = 429,
  INTERNAL_SERVER_ERROR = 500,
}

export interface TokenizedDaoArrayResponse<T> {
  data: T[];
  nextPageToken: number | null;
}

export interface DateTimestamp {
  timestamp: string;
  unixEpoch: UnixEpoch;
}
