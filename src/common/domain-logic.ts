export abstract class DomainLogic<T> {
  /**
   * Returns a list of invalid attributes
   */
  abstract validateAttributes(domain: T): string[];
}
