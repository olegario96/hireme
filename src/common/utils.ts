import { extname } from 'path';

export const isFile = (path: string): boolean => !!extname(path);
