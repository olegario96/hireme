import 'reflect-metadata';

import { container } from 'tsyringe';

import { Jwt } from './jwt';

describe('Jwt', () => {
  let instance: Jwt;

  beforeAll(() => {
    instance = container.resolve(Jwt);
    process.env.JWT_SECRET = 'THIS_IS_POD_RACE';
  });

  describe('signToken|verifyToken', () => {
    it('should sign and verify token', () => {
      const payload = { userId: 1 };
      const signedToken = instance.signToken(payload);
      const verifiedToken = instance.verifyToken(signedToken);
      expect(verifiedToken).toMatchObject(payload);
    });
  });
});
