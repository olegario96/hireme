/* eslint-disable jest/expect-expect, no-underscore-dangle */
import 'reflect-metadata';

import { Writable } from 'stream';
import { container } from 'tsyringe';

import { resetTable } from '../../../seed/helper';
import { populateUsersTable } from '../../../seed/users';
import { endConnections } from '../../../tests/utils';
import { connection } from '../../database/connection';
import { PersistedUser, TABLE_USERS } from '../../users/entity';
import { TABLE_TWO_FA_TOKENS } from './entity';
import { TwoFaController } from './two-fa-controller';

describe('TwoFaController', () => {
  let user: PersistedUser;
  let controller: TwoFaController;

  beforeAll(async () => {
    await connection.initialize();
    controller = container.resolve(TwoFaController);
    user = (await populateUsersTable())!;
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_USERS),
      resetTable(TABLE_TWO_FA_TOKENS),
    ]);

    await endConnections();
  });

  describe('respondWithQrCode', () => {
    it('should return QR code', async () => {
      const writableStream = new Writable();
      writableStream._write = jest.fn();
      await controller.respondWithQrCode(user.id, writableStream);
    });
  });

  describe('isTwoFaCodeValid', () => {
    describe('when it can not find token for userId', () => {
      it('should return false', async () => {
        const userId = 30;
        const isValid = await controller.isTwoFaCodeValid('order66', userId);
        expect(isValid).toStrictEqual(false);
      });
    });

    describe('when it can find token for userId', () => {
      describe('when 2FA code is invalid', () => {
        it('should return false', async () => {
          const isValid = await controller.isTwoFaCodeValid('order66', user.id);
          expect(isValid).toStrictEqual(false);
        });
      });
    });
  });

  describe('isTwoFaEnabled', () => {
    describe('when 2FA is enabled', () => {
      it('should return true', async () => {
        const isEnabled = await controller.isTwoFaEnabled(user.id);
        expect(isEnabled).toStrictEqual(true);
      });
    });
  });

  describe('disableTwoFa', () => {
    describe('when 2FA is not enabled', () => {
      it('should early return', async () => {
        const userId = 66;
        const isTwoFaEnabled = await controller.isTwoFaEnabled(userId);
        await controller.disableTwoFa(userId);
        const targetIsTwoFaEnabled = await controller.isTwoFaEnabled(userId);
        expect(isTwoFaEnabled).toStrictEqual(false);
        expect(targetIsTwoFaEnabled).toStrictEqual(false);
      });
    });

    describe('when 2FA is enabled', () => {
      it('should disable 2FA', async () => {
        const { id: userId } = user;
        const isTwoFaEnabled = await controller.isTwoFaEnabled(userId);
        await controller.disableTwoFa(userId);
        const targetIsTwoFaEnabled = await controller.isTwoFaEnabled(userId);
        expect(isTwoFaEnabled).toStrictEqual(true);
        expect(targetIsTwoFaEnabled).toStrictEqual(false);
      });
    });
  });
});
