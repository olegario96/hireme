import * as speakeasy from 'speakeasy';
import { Writable } from 'stream';
import { injectable, singleton } from 'tsyringe';

import { UserId } from '../../users/entity';
import { UserDao } from '../../users/user-dao';
import { BASE_32 } from './entity';
import { TwoFaDao } from './two-fa-dao';
import { TwoFaDomainLogic } from './two-fa-domain-logic';

@injectable()
@singleton()
export class TwoFaController {
  constructor(
    private readonly twoFaDao: TwoFaDao,
    private readonly twoFaDomainLogic: TwoFaDomainLogic,
    private readonly userDao: UserDao,
  ) {}

  public async respondWithQrCode(userId: UserId, response: Writable): Promise<void> {
    const { base32, otpAuthUrl } = this.twoFaDomainLogic.createAuthenticationCode();
    /**
     * Middleware will ensure user exists
     */
    const user = (await this.userDao.getUser({ id: userId }))!;
    await this.twoFaDao.createTwoFaToken(user, base32);
    return this.twoFaDomainLogic.respondWithQrCode(response, otpAuthUrl);
  }

  public async isTwoFaCodeValid(twoFaCode: string, userId: UserId): Promise<boolean> {
    const twoFaToken = await this.twoFaDao.getTwoFaToken(userId);
    if (!twoFaToken) {
      return false;
    }

    return speakeasy.totp.verify({
      secret: twoFaToken.token,
      encoding: BASE_32,
      token: twoFaCode,
    });
  }

  public async isTwoFaEnabled(userId: UserId): Promise<boolean> {
    const twoFaToken = await this.twoFaDao.getTwoFaToken(userId);
    return twoFaToken ? true : false;
  }

  public async disableTwoFa(userId: UserId): Promise<void> {
    const isTwoFaEnabled = await this.isTwoFaEnabled(userId);
    if (!isTwoFaEnabled) {
      return;
    }

    await this.twoFaDao.deleteTwoFaToken(userId);
  }
}
