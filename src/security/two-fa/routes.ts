import { Router } from 'express';
import { container } from 'tsyringe';

import { HttpStatusCode } from '../../common/types';
import { Logger } from '../../logger';
import {
  middlewareContentType,
  middlewareJwt,
} from '../../middlewares';
import { CustomRequest } from '../../middlewares/types';
import { TwoFaController } from '../two-fa/two-fa-controller';

const loadRoutes = (router: Router): void => {
  const controller = container.resolve(TwoFaController);
  const logger = container.resolve(Logger);

  /**
   * @api [post] /enable-two-fa
   * bodyContentType: "application/json"
   * description: "Enable 2FA for a given user"
   * parameters:
   *   - in: header
   *     name: Content-Type
   *     schema:
   *       type: string
   *       example: application/json
   *     required: true
   *   - in: header
   *     name: authorization
   *     schema:
   *       type: string
   *       example: Bearer my-token
   *     required: true
   * responses:
   *   "200":
   *     description: "QR code to create 2FA"
   *   "401":
   *     description: "Message indicating JWT is invalid"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Invalid token provided!"
   *   "500":
   *     description: "Message indicating failure"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Could not enable 2FA"
   */
  router.post('/enable-two-fa', middlewareContentType(), middlewareJwt(), async (req: CustomRequest, res) => {
    try {
      const { userId } = req;
      const targetUserId = userId!;
      const isTwoFaEnabled = await controller.isTwoFaEnabled(targetUserId);
      if (isTwoFaEnabled) {
        res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'Impossible to activate 2FA now' });
        return;
      }

      res.status(HttpStatusCode.OK);
      return controller.respondWithQrCode(targetUserId, res);
    } catch (e) {
      const exception = e as Error;
      logger.error(`POST /enable-two-fa exception: ${exception.message}`);
      res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not enable 2FA' });
    }
  });

  /**
   * @api [post] /authenticate-two-fa
   * bodyContentType: "application/json"
   * description: "Verify 2FA code for a given user"
   * parameters:
   *   - in: header
   *     name: Content-Type
   *     schema:
   *       type: string
   *       example: application/json
   *     required: true
   *   - in: header
   *     name: authorization
   *     schema:
   *       type: string
   *       example: Bearer my-token
   *     required: true
   *   - name: body
   *     in: body
   *     description: HTTP body request
   *     schema:
   *       type: object
   *       required:
   *         - twoFaCode
   *       properties:
   *         twoFaCode:
   *           type: integer
   *           example: 123456
   * responses:
   *   "200":
   *     description: Successful authentication
   *     schema:
   *       type: "object"
   *   "400":
   *     description: "Invalid 2FA code provided"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Invalid 2FA code"
   *   "401":
   *     description: "Message indicating JWT is invalid"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Invalid token provided!"
   *   "500":
   *     description: "Message indicating failure"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Could not confirm 2FA code"
   */
  router.post('/authenticate-two-fa', middlewareContentType(), middlewareJwt(), async (req: CustomRequest, res) => {
    try {
      const { userId } = req;
      const targetUserId = userId!;
      const isTwoFaEnabled = await controller.isTwoFaEnabled(targetUserId);
      if (!isTwoFaEnabled) {
        res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'Invalid 2FA code' });
        return;
      }

      const { twoFaCode } = req.body;
      const isTwoFaCodeValid = await controller.isTwoFaCodeValid(twoFaCode, targetUserId);
      if (!isTwoFaCodeValid) {
        res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'Invalid 2FA code' });
        return;
      }

      res.status(HttpStatusCode.OK).send({});
    } catch (e) {
      const exception = e as Error;
      logger.error(`POST /authenticate-two-fa: ${exception.message}`);
      res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not confirm 2FA code' });
    }
  });

  /**
   * @api [post] /disable-two-fa
   * bodyContentType: "application/json"
   * description: "Disable 2FA for a given user"
   * parameters:
   *   - in: header
   *     name: Content-Type
   *     schema:
   *       type: string
   *       example: application/json
   *     required: true
   *   - in: header
   *     name: authorization
   *     schema:
   *       type: string
   *       example: Bearer my-token
   *     required: true
   *   - name: body
   *     in: body
   *     description: HTTP body request
   *     schema:
   *       type: object
   *       required:
   *         - twoFaCode
   *       properties:
   *         twoFaCode:
   *           type: integer
   *           example: 123456
   * responses:
   *   "204":
   *     description: 2FA has been disabled
   *     schema:
   *       type: "object"
   *   "400":
   *     description: "Invalid 2FA code provided"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Invalid 2FA code"
   *   "401":
   *     description: "Message indicating JWT is invalid"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Invalid token provided!"
   *   "500":
   *     description: "Message indicating failure"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Could not confirm 2FA code"
   */
  router.post('/disable-two-fa', middlewareContentType(), middlewareJwt(), async (req: CustomRequest, res) => {
    try {
      const { userId } = req;
      const targetUserId = userId!;
      const isTwoFaEnabled = await controller.isTwoFaEnabled(targetUserId);
      if (!isTwoFaEnabled) {
        res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'Invalid 2FA code' });
        return;
      }

      const { twoFaCode } = req.body;
      const isTwoFaCodeValid = await controller.isTwoFaCodeValid(twoFaCode, targetUserId);
      if (!isTwoFaCodeValid) {
        res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'Invalid 2FA code' });
        return;
      }

      await controller.disableTwoFa(targetUserId);
      res.status(HttpStatusCode.NO_CONTENT).send({});
    } catch (e) {
      const exception = e as Error;
      logger.error(`POST /disable-two-fa: ${exception.message}`);
      res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not confirm 2FA code' });
    }
  });
};

module.exports = loadRoutes;
