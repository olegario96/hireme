import 'reflect-metadata';

import { container } from 'tsyringe';

import { resetTable } from '../../../seed/helper';
import { populateUsersTable } from '../../../seed/users';
import { endConnections } from '../../../tests/utils';
import { connection } from '../../database/connection';
import { PersistedUser, TABLE_USERS } from '../../users/entity';
import { TABLE_TWO_FA_TOKENS } from './entity';
import { TwoFaDao } from './two-fa-dao';

describe('TwoFaDao', () => {
  let dao: TwoFaDao;
  let user: PersistedUser;

  beforeAll(async () => {
    await connection.initialize();
    dao = container.resolve(TwoFaDao);
    user = (await populateUsersTable())!;
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_USERS),
      resetTable(TABLE_TWO_FA_TOKENS),
    ]);

    await endConnections();
  });

  describe('createTwoFaToken', () => {
    it('should create token', async () => {
      const base32 = 'AAAAAAAAAAAA';
      const { id } = await dao.createTwoFaToken(user, base32);
      expect(id).not.toBeNull();
      expect(typeof id).toBe('number');
    });
  });

  describe('getTwoFaToken', () => {
    describe('when userId does not exist', () => {
      it('should return nullptr', async () => {
        const invalidUserId = 7;
        const nullptr = await dao.getTwoFaToken(invalidUserId);
        expect(nullptr).toStrictEqual(null);
      });
    });

    describe('when userId exists', () => {
      it('should return 2FA token', async () => {
        const validUserId = 1;
        const { id } = (await dao.getTwoFaToken(validUserId))!;
        expect(id).not.toBeNull();
        expect(typeof id).toBe('number');
      });
    });
  });

  describe('deleteTwoFaToken', () => {
    describe('when invalid userId is provided', () => {
      it('should early return', async () => {
        const targetUserId = 66;
        const isTwoFaEnabled = await dao.getTwoFaToken(targetUserId);
        await dao.deleteTwoFaToken(targetUserId);
        const targetIsTwoFaEnabled = await dao.getTwoFaToken(targetUserId);
        expect(isTwoFaEnabled).toStrictEqual(null);
        expect(targetIsTwoFaEnabled).toStrictEqual(null);
      });
    });

    describe('when valid userId is provided', () => {
      it('should delete 2FA token', async () => {
        const { id } = user;
        const token = await dao.getTwoFaToken(id);
        await dao.deleteTwoFaToken(id);
        const targetToken = await dao.getTwoFaToken(id);
        expect(token).not.toStrictEqual(null);
        expect(targetToken).toStrictEqual(null);
      });
    });
  });
});
