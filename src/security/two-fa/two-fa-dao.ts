import { injectable, singleton } from 'tsyringe';
import { IsNull } from 'typeorm';

import { connection } from '../../database/connection';
import { PersistedUser, UserId } from '../../users/entity';
import { PersistedTwoFaToken, TABLE_TWO_FA_TOKENS } from './entity';

@injectable()
@singleton()
export class TwoFaDao {
  private readonly repository;

  constructor() {
    this.repository = connection.getRepository(PersistedTwoFaToken);
  }

  async createTwoFaToken(user: PersistedUser, token: string): Promise<PersistedTwoFaToken> {
    const targetToken = { token } as PersistedTwoFaToken;
    targetToken.user = user;
    return this.repository.save(targetToken);
  }

  async getTwoFaToken(userId: UserId): Promise<PersistedTwoFaToken | null> {
    return this.repository.findOne({
      where: {
        user: { id: userId },
        deletedAt: IsNull(),
      },
    });
  }

  async deleteTwoFaToken(userId: UserId): Promise<void> {
    const token = await this.getTwoFaToken(userId);
    if (!token) {
      return;
    }

    await this.repository.createQueryBuilder(TABLE_TWO_FA_TOKENS)
      .softDelete()
      .where('id = :tokenId', { tokenId: token.id })
      .execute();
  }
}
