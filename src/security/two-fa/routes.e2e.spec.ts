/* eslint-disable jest/expect-expect */
import 'reflect-metadata';

import { Application } from 'express';
import request from 'supertest';
import { container } from 'tsyringe';

import { resetTable } from '../../../seed/helper';
import { populateUsersTable } from '../../../seed/users';
import { buildAppForTests, endConnections } from '../../../tests/utils';
import { APPLICATION_JSON, AUTHORIZATION, CONTENT_TYPE } from '../../common/constants';
import { HttpStatusCode } from '../../common/types';
import { TABLE_USERS, UserId } from '../../users/entity';
import { Jwt } from '../jwt';
import { TABLE_TWO_FA_TOKENS } from './entity';
import { TwoFaController } from './two-fa-controller';

describe('User routes', () => {
  let app: Application;
  let jwt: Jwt;
  let userId: UserId;
  let controller: TwoFaController;

  beforeAll(async () => {
    const context = __dirname;
    app = await buildAppForTests(context);
    jwt = container.resolve(Jwt);
    const { id } = (await populateUsersTable())!;
    userId = id;
    controller = container.resolve(TwoFaController);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_USERS),
      resetTable(TABLE_TWO_FA_TOKENS),
    ]);

    await endConnections();
  });

  describe('POST /enable-two-fa', () => {
    describe('when accept header is not JSON', () => {
      it('should return bad request', async () => {
        await request(app).post('/enable-two-fa')
          .set(CONTENT_TYPE, 'foo')
          .send()
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when invalid JWT is provided', () => {
      it('should return unauthorized', async () => {
        await request(app).post('/enable-two-fa')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .set(AUTHORIZATION, 'Bearer token')
          .send()
          .expect(HttpStatusCode.UNAUTHORIZED);
      });
    });

    describe('when valid JWT is provided', () => {
      describe('when 2FA is not enabled', () => {
        it('should return ok status', async () => {
          const token = jwt.signToken({ userId });
          await request(app).post('/enable-two-fa')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .set(AUTHORIZATION, `Bearer ${token}`)
            .send()
            .expect(HttpStatusCode.OK);
        });
      });

      describe('when 2FA is enabled already', () => {
        it('should return bad request', async () => {
          const token = jwt.signToken({ userId });
          await request(app).post('/enable-two-fa')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .set(AUTHORIZATION, `Bearer ${token}`)
            .send()
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });
    });

    describe('when it can not access database', () => {
      beforeEach(() => {
        jest.spyOn(controller, 'isTwoFaEnabled').mockRejectedValueOnce(new Error('Mocked'));
      });

      it('should internal server error', async () => {
        const token = jwt.signToken({ userId });
        await request(app).post('/enable-two-fa')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .set(AUTHORIZATION, `Bearer ${token}`)
          .send()
          .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
      });
    });
  });

  describe('POST /authenticate-two-fa', () => {
    describe('when accept header is not JSON', () => {
      it('should return bad request', async () => {
        await request(app).post('/authenticate-two-fa')
          .set(CONTENT_TYPE, 'foo')
          .send()
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when invalid JWT is provided', () => {
      it('should return unauthorized', async () => {
        await request(app).post('/authenticate-two-fa')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .set(AUTHORIZATION, 'Bearer token')
          .send()
          .expect(HttpStatusCode.UNAUTHORIZED);
      });
    });

    describe('when valid JWT is provided', () => {
      describe('when 2FA is not enabled', () => {
        beforeEach(() => {
          jest.spyOn(controller, 'isTwoFaEnabled').mockResolvedValueOnce(false);
        });

        it('should return bad request status', async () => {
          const token = jwt.signToken({ userId });
          await request(app).post('/authenticate-two-fa')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .set(AUTHORIZATION, `Bearer ${token}`)
            .send()
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });

      describe('when 2FA is enabled', () => {
        describe('when provided code is invalid', () => {
          it('should return bad request', async () => {
            const token = jwt.signToken({ userId });
            await request(app).post('/authenticate-two-fa')
              .set(CONTENT_TYPE, APPLICATION_JSON)
              .set(AUTHORIZATION, `Bearer ${token}`)
              .send({ twoFaCode: 123 })
              .expect(HttpStatusCode.BAD_REQUEST);
          });
        });

        describe('when provided code is valid', () => {
          beforeEach(() => {
            // Even though is an anti pattern to use mocks for E2E tests
            // we currently do not have a work around to create valid codes
            // during runtime.
            jest.spyOn(controller, 'isTwoFaCodeValid').mockResolvedValueOnce(true);
          });

          it('should return ok status', async () => {
            const token = jwt.signToken({ userId });
            await request(app).post('/authenticate-two-fa')
              .set(CONTENT_TYPE, APPLICATION_JSON)
              .set(AUTHORIZATION, `Bearer ${token}`)
              .send({ twoFaCode: 123 })
              .expect(HttpStatusCode.OK);
          });
        });
      });
    });

    describe('when it can not use database', () => {
      beforeEach(() => {
        jest.spyOn(controller, 'isTwoFaEnabled').mockRejectedValueOnce(new Error('Mocked'));
      });

      it('should internal server error', async () => {
        const token = jwt.signToken({ userId });
        await request(app).post('/authenticate-two-fa')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .set(AUTHORIZATION, `Bearer ${token}`)
          .send()
          .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
      });
    });
  });

  describe('POST /disable-two-fa', () => {
    describe('when accept header is not JSON', () => {
      it('should return bad request', async () => {
        await request(app).post('/authenticate-two-fa')
          .set(CONTENT_TYPE, 'foo')
          .send()
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when invalid JWT is provided', () => {
      it('should return unauthorized', async () => {
        await request(app).post('/authenticate-two-fa')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .set(AUTHORIZATION, 'Bearer token')
          .send()
          .expect(HttpStatusCode.UNAUTHORIZED);
      });
    });

    describe('when valid JWT is provided', () => {
      describe('when 2FA is not enabled', () => {
        beforeEach(() => {
          jest.spyOn(controller, 'isTwoFaEnabled').mockResolvedValueOnce(false);
        });

        it('should return bad request status', async () => {
          const token = jwt.signToken({ userId });
          await request(app).post('/disable-two-fa')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .set(AUTHORIZATION, `Bearer ${token}`)
            .send()
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });

      describe('when 2FA code is not valid', () => {
        it('should return bad request status', async () => {
          const token = jwt.signToken({ userId });
          await request(app).post('/disable-two-fa')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .set(AUTHORIZATION, `Bearer ${token}`)
            .send({ twoFaCode: 123 })
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });

      describe('when 2FA code is valid', () => {
        beforeEach(() => {
          // Even though is an anti pattern to use mocks for E2E tests
          // we currently do not have a workaround to create valid 2FA codes
          // during runtime.
          jest.spyOn(controller, 'isTwoFaCodeValid').mockResolvedValueOnce(true);
        });

        it('should return no content status', async () => {
          const token = jwt.signToken({ userId });
          await request(app).post('/disable-two-fa')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .set(AUTHORIZATION, `Bearer ${token}`)
            .send({ twoFaCode: 123 })
            .expect(HttpStatusCode.NO_CONTENT);
        });
      });
    });

    describe('when it can not use the database', () => {
      beforeEach(() => {
        jest.spyOn(controller, 'isTwoFaEnabled').mockRejectedValueOnce(new Error('Mocked'));
      });

      it('should internal server error', async () => {
        const token = jwt.signToken({ userId });
        await request(app).post('/disable-two-fa')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .set(AUTHORIZATION, `Bearer ${token}`)
          .send()
          .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
      });
    });
  });
});
