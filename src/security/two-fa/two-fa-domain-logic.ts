import { toFileStream as qrCodeToFileStream } from 'qrcode';
import * as speakeasy from 'speakeasy';
import { Writable } from 'stream';
import { injectable, singleton } from 'tsyringe';

import { Config } from '../../config';
import { TwoFaAuthCode } from './entity';

@injectable()
@singleton()
export class TwoFaDomainLogic {
  constructor(private readonly config: Config) {
    this.config.ensureEnvironmentVariableIsDefined([
      'APP_NAME',
    ]);
  }

  createAuthenticationCode(): TwoFaAuthCode {
    const { otpauth_url: otpAuthUrl, base32 } = speakeasy.generateSecret({ name: process.env.APP_NAME! });
    return {
      otpAuthUrl,
      base32,
    };
  }

  async respondWithQrCode(response: Writable, otpAuthUrl: string | undefined): Promise<void> {
    if (!otpAuthUrl) {
      throw new Error('Invalid OTP Auth URL provided!');
    }

    await qrCodeToFileStream(response, otpAuthUrl);
  }
}
