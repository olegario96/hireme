/* eslint-disable jest/expect-expect, @typescript-eslint/no-floating-promises, no-underscore-dangle */
import 'reflect-metadata';

import { Writable } from 'stream';
import { container } from 'tsyringe';

import { TwoFaDomainLogic } from './two-fa-domain-logic';

describe('TwoFaDomainLogic', () => {
  let twoFaDomainLogic: TwoFaDomainLogic;

  beforeAll(() => {
    process.env.APP_NAME = 'hireme';
    twoFaDomainLogic = container.resolve(TwoFaDomainLogic);
  });

  describe('createAuthenticationCode', () => {
    it('should create authentication code', () => {
      const { base32, otpAuthUrl } = twoFaDomainLogic.createAuthenticationCode();
      expect(base32).not.toStrictEqual(undefined);
      expect(otpAuthUrl).toContain('otpauth://totp/hireme');
    });
  });

  describe('respondWithQrCode', () => {
    describe('when invalid OTP auth is provided', () => {
      it('should throw an error', () => {
        const writableStream = new Writable();
        const otp = undefined;
        expect(twoFaDomainLogic.respondWithQrCode(writableStream, otp)).rejects.toThrow();
      });
    });

    describe('when valid OTP auth is provided', () => {
      it('should return QR code', () => {
        const { otpAuthUrl } = twoFaDomainLogic.createAuthenticationCode();
        const writableStream = new Writable();
        writableStream._write = jest.fn();
        twoFaDomainLogic.respondWithQrCode(writableStream, otpAuthUrl);
      });
    });
  });
});
