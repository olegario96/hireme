import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { MetaFields } from '../../common/entity';
import { PersistedUser } from '../../users/entity';

export const TABLE_TWO_FA_TOKENS = 'two_fa_tokens';
export const BASE_32 = 'base32';

export interface TwoFaAuthCode {
  base32: string;
  otpAuthUrl?: string;
}

@Entity(TABLE_TWO_FA_TOKENS)
export class PersistedTwoFaToken extends MetaFields {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => PersistedUser)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: PersistedUser;

  @Column()
  token: string;
}

export type TwoFaToken = Omit<PersistedTwoFaToken, 'id' | 'user' | 'createdAt' | 'updatedAt' | 'deletedAt'>;
