export interface JwtPayloadToSign {
  userId: number;
}

export interface JwtPayloadVerified extends JwtPayloadToSign {
  iat: number;
  exp: number;
}

export type JsonWebToken = string;
