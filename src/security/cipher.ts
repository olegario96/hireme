import { BinaryLike, createCipheriv, createDecipheriv, createHash, randomBytes } from 'crypto';
import { injectable, singleton } from 'tsyringe';

import { HEX, UTF_8 } from '../common/constants';
import { Config } from '../config';
import { PersistedUser } from '../users/entity';

@injectable()
@singleton()
export class Cipher {
  private readonly aes256key: string;
  private readonly iv: BinaryLike;
  private readonly AES_256_CTR = 'aes-256-ctr';
  private readonly SHA_256 = 'sha256';

  constructor(private readonly config: Config) {
    this.config.ensureEnvironmentVariableIsDefined([
      'AES_256_KEY',
      'AES_256_IV',
    ]);

    this.aes256key = process.env.AES_256_KEY!;
    this.iv = Buffer.from(process.env.AES_256_IV!);
  }

  isPasswordCorrect(password: string, persistedUser: PersistedUser): boolean {
    const { password: hashedPassword, salt: encryptedSalt } = persistedUser;
    const decryptedSalt = this.decrypt(encryptedSalt);
    const saltedPassword = this.saltPassword(password, decryptedSalt);
    return hashedPassword === this.hash(saltedPassword);
  }

  createSalt(): string {
    const numBytes = 16;
    return randomBytes(numBytes).toString(HEX);
  }

  hash(password: string): string {
    return createHash(this.SHA_256).update(password)
      .digest(HEX);
  }

  saltPassword(password: string, salt: string): string {
    return `${password}${salt}`;
  }

  encrypt(decryptedString: string): string {
    const cipher = createCipheriv(this.AES_256_CTR, this.aes256key, this.iv);
    const encrypted = cipher.update(decryptedString, UTF_8, HEX);
    return encrypted + cipher.final(HEX);
  }

  decrypt(encryptedString: string): string {
    const decipher = createDecipheriv(this.AES_256_CTR, this.aes256key, this.iv);
    const decrypted = decipher.update(encryptedString, HEX, UTF_8);
    return decrypted + decipher.final(UTF_8);
  }
}
