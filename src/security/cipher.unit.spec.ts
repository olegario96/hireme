import 'reflect-metadata';

import { container } from 'tsyringe';

import { PersistedUser } from '../users/entity';
import { Cipher } from './cipher';

/* eslint-disable @typescript-eslint/no-magic-numbers */
describe('Cipher', () => {
  let cipher: Cipher;

  beforeAll(() => {
    cipher = container.resolve(Cipher);
  });

  describe('isPasswordCorrect', () => {
    describe('when credentials are valid', () => {
      it('should return true', () => {
        const password = 'K3nOb1__';
        const user = {
          password: 'e3f9f7aa13cc2b2712b5d2970655df40e3b27572f02dd1d0c8f18f282e13a21a',
          salt: '7d515eac',
        } as PersistedUser;

        const isPasswordCorrect = cipher.isPasswordCorrect(password, user);
        expect(isPasswordCorrect).toStrictEqual(true);
      });
    });
  });

  describe('createSalt', () => {
    it('creates a random salt', () => {
      const salt = cipher.createSalt();
      const hexRegex = /[0-9A-Fa-f]{6}/g;
      expect(hexRegex.test(salt)).toStrictEqual(true);
    });
  });

  describe('hash', () => {
    it('should hash a password', () => {
      const password = 'order66';
      const expectedPassword = '2ba65a6ce000ba58dcfc501eeff94d26063ebb310279d704ba85dcad930f84fc';
      const hashedPassword = cipher.hash(password);
      expect(expectedPassword).toStrictEqual(hashedPassword);
    });
  });

  describe('saltPassword', () => {
    it('should mix password and userId', () => {
      const password = 'order6';
      const salt = '6';
      expect('order66').toStrictEqual(cipher.saltPassword(password, salt));
    });
  });

  describe('encrypt|decrypt', () => {
    it('should encrypt and decrypt a string', () => {
      const password = 'order66';
      const encryptedPassword = cipher.encrypt(password);
      expect(password).toStrictEqual(cipher.decrypt(encryptedPassword));
    });
  });
});
