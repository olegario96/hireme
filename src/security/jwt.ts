import { sign as signJwt, verify as verifyJwt } from 'jsonwebtoken';
import { injectable, singleton } from 'tsyringe';

import { Config } from '../config';
import { JsonWebToken, JwtPayloadToSign, JwtPayloadVerified } from './types';

@injectable()
@singleton()
export class Jwt {
  private readonly JWT_SECRET: string;

  constructor(private readonly config: Config) {
    this.config.ensureEnvironmentVariableIsDefined(['JWT_SECRET']);
    this.JWT_SECRET = process.env.JWT_SECRET!;
  }

  signToken(token: JwtPayloadToSign): JsonWebToken {
    return signJwt(token, this.JWT_SECRET, { expiresIn: '1h' });
  }

  verifyToken(signedToken: JsonWebToken): JwtPayloadVerified {
    return verifyJwt(signedToken, this.JWT_SECRET) as JwtPayloadVerified;
  }
}
