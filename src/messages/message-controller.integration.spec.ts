/* eslint-disable @typescript-eslint/no-magic-numbers */
import 'reflect-metadata';

import { container } from 'tsyringe';

import { populateConversationsTable } from '../../seed/conversations';
import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { ConversationDao } from '../conversations/conversation-dao';
import { ConversationId, PersistedConversation, TABLE_CONVERSATIONS } from '../conversations/types';
import { connection } from '../database/connection';
import { PersistedUser, TABLE_USERS, UserId } from '../users/entity';
import { MessageController } from './message-controller';
import { Message, TABLE_MESSAGES } from './types';

describe('MessageController', () => {
  let controller: MessageController;
  let dao: ConversationDao;
  let userId: UserId;

  beforeAll(async () => {
    await connection.initialize();
    controller = container.resolve(MessageController);
    dao = container.resolve(ConversationDao);
    userId = 1;
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_MESSAGES),
      resetTable(TABLE_CONVERSATIONS),
      resetTable(TABLE_USERS),
    ]);

    await endConnections();
  });

  describe('createMessage', () => {
    describe('when provided conversation does not exist', () => {
      it('should return a nullptr', async () => {
        const message = { conversationId: -1, responseTo: null, userIdSender: 1, userIdReceiver: 2, content: 'Hello, there!' };
        const conversation = await controller.createMessage(userId, message);
        expect(conversation).toStrictEqual(null);
      });
    });

    describe('when provided conversation exists', () => {
      let conversationId: ConversationId;
      let message: Message;

      beforeAll(async () => {
        conversationId = 1;
        message = { conversationId, responseTo: null, userIdSender: 1, userIdReceiver: 2, content: 'Hello, there!' };
        const override = { email: 'general@grievous.com' } as PersistedUser;
        await populateUsersTable();
        await populateUsersTable(override);
        await populateConversationsTable();
      });

      describe('when conversation is not active', () => {
        beforeAll(async () => {
          const conversation = { id: conversationId } as PersistedConversation;
          await dao.deactivateConversation(conversation);
        });

        it('should return a nullptr', async () => {
          const persistedMessage = await controller.createMessage(userId, message);
          expect(persistedMessage).toStrictEqual(null);
        });
      });

      describe('when provided conversation is active', () => {
        beforeAll(async () => {
          const conversation = { userIdSender: 1, userIdReceiver: 1, isActive: 1 } as unknown as PersistedConversation;
          message = { ...message, conversationId: 2 };
          await populateConversationsTable(conversation);
        });

        describe('when user can not send message', () => {
          it('should return a nullptr', async () => {
            userId = 3;
            const persistedMessage = await controller.createMessage(userId, message);
            expect(persistedMessage).toStrictEqual(null);
          });
        });

        describe('when user can send a message', () => {
          it('should create the message', async () => {
            userId = 1;
            message = { ...message, userIdReceiver: 1, userIdSender: 1 };
            const persistedMessage = (await controller.createMessage(userId, message))!;
            expect(persistedMessage).not.toStrictEqual(null);
            expect(persistedMessage).toHaveProperty('id');
            expect(persistedMessage).toHaveProperty('createdAt');
            expect(persistedMessage).toHaveProperty('updatedAt');
            expect(persistedMessage).toHaveProperty('deletedAt');
            expect(persistedMessage.userIdReceiver).toStrictEqual(message.userIdReceiver);
            expect(persistedMessage.userIdSender).toStrictEqual(message.userIdSender);
            expect(persistedMessage.content).toStrictEqual(message.content);
            expect(persistedMessage.conversationId).toStrictEqual(message.conversationId);
          });
        });
      });
    });
  });
});
