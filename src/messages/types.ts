import { MetaFields, UnixEpoch } from '../common/types';
import { ConversationId } from '../conversations/types';
import { UserId } from '../users/entity';

export type MessageId = number;
export interface PersistedMessage extends MetaFields {
  id: MessageId;
  conversationId: ConversationId;
  responseTo: MessageId | null;
  userIdSender: UserId;
  userIdReceiver: UserId;
  content: string;
  sentAt: UnixEpoch | null;
}

export type Message = Omit<PersistedMessage, 'id' | 'sentAt' | 'createdAt' | 'updatedAt' | 'deletedAt'>;
export const TABLE_MESSAGES = 'messages';
