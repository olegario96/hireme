import { createMessageSchema } from '.';

describe('SchemaValidation', () => {
  let message;

  beforeAll(() => {
    message = { conversationId: 1, userIdSender: 1, userIdReceiver: 2, content: 'asdf' };
  });

  describe('when "response to" is not provided', () => {
    it('should validate message', () => {
      const validatedMessage = createMessageSchema.validate(message);
      expect(validatedMessage.error).toStrictEqual(undefined);
    });
  });

  describe('when "response to" is provided', () => {
    it('should validate message', () => {
      message = { ...message, responseTo: 3 };
      const validatedMessage = createMessageSchema.validate(message);
      expect(validatedMessage.error).toStrictEqual(undefined);
    });
  });
});
