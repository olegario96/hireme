import * as Joi from 'joi';

export const createMessageSchema = Joi.object({
  conversationId: Joi.number()
    .positive()
    .required(),
  responseTo: Joi.number()
    .positive()
    .optional(),
  userIdSender: Joi.number()
    .positive()
    .required(),
  userIdReceiver: Joi.number()
    .positive()
    .required(),
  content: Joi.string()
    .required(),
});
