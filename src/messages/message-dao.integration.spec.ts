import 'reflect-metadata';

import flushPromises from 'flush-promises';
import { container } from 'tsyringe';

import { populateConversationsTable } from '../../seed/conversations';
import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { ConversationId, TABLE_CONVERSATIONS } from '../conversations/types';
import { connection } from '../database/connection';
import { PersistedUser, TABLE_USERS } from '../users/entity';
import { MessageDao } from './message-dao';
import { TABLE_MESSAGES } from './types';

describe('MessageDao', () => {
  let dao: MessageDao;
  let conversationId: ConversationId;

  beforeAll(async () => {
    await connection.initialize();
    dao = container.resolve(MessageDao);
    conversationId = 1;
    const override = { email: 'general@grivous.com' } as PersistedUser;
    await populateUsersTable();
    await populateUsersTable(override);
    await populateConversationsTable();
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_MESSAGES),
      resetTable(TABLE_CONVERSATIONS),
      resetTable(TABLE_USERS),
    ]);

    await endConnections();
  });

  describe('createMessage|getMessages', () => {
    it('should create message properly', async () => {
      const message = {
        content: 'Hello, there!',
        conversationId,
        responseTo: null,
        userIdSender: 1,
        userIdReceiver: 2,
      };

      await dao.createMessage(message);
      const { data: messages, nextPageToken } = await dao.getMessages(conversationId);
      expect(messages).toStrictEqual([]);
      expect(nextPageToken).toStrictEqual(null);
    });
  });

  describe('markMessagesAsSent', () => {
    describe('when empty array is provided', () => {
      it('should not mark messages as sent', async () => {
        const messageIds = [];
        await dao.markMessagesAsSent(messageIds);
        const { data: messages } = await dao.getMessages(conversationId);
        expect(messages).toStrictEqual([]);
      });
    });

    describe('when non empty array is provided', () => {
      it('should mark messages as sent', async () => {
        const messageIds = [1];
        await dao.markMessagesAsSent(messageIds);
        const { data: messages } = await dao.getMessages(conversationId);
        expect(messages).toHaveLength(1);
      });
    });
  });

  describe('getMessages', () => {
    describe('when DAO needs to paginate response', () => {
      beforeAll(async () => {
        const messages = Array(MessageDao.PAGE_SIZE_MESSAGES + 1).fill({
          content: 'Hello, there!',
          conversationId,
          responseTo: null,
          userIdSender: 1,
          userIdReceiver: 2,
        });

        messages.map((message) => dao.createMessage(message));
        await flushPromises();
        const messageIds = [...Array(MessageDao.PAGE_SIZE_MESSAGES + 1).keys()].map(el => Number(el) + 1);
        await dao.markMessagesAsSent(messageIds);
      });

      it('should return page token different than nullptr', async () => {
        const { nextPageToken } = await dao.getMessages(conversationId);
        expect(nextPageToken).not.toStrictEqual(null);
      });
    });
  });
});
