import 'reflect-metadata';

import { container } from 'tsyringe';

import { PersistedConversation } from '../conversations/types';
import { MessageDomainLogic } from './message-domain-logic';
import { Message } from './types';

describe('MessageDomainLogic', () => {
  let domainLogic: MessageDomainLogic;
  beforeAll(() => {
    domainLogic = container.resolve(MessageDomainLogic);
  });

  describe('canUserSendMessage', () => {
    describe('when authenticated user does not match the original sender', () => {
      it('should return false', () => {
        const userId = 1;
        const message = { userIdSender: 2, userIdReceiver: 3 } as Message;
        const conversation = { ...message } as unknown as PersistedConversation;
        expect(domainLogic.canUserSendMessage(userId, conversation, message)).toStrictEqual(false);
      });
    });

    describe('when authenticated user matches the original sender', () => {
      describe('when receiver does not belong to the conversation', () => {
        it('should return false', () => {
          const userId = 1;
          const message = { userIdSender: userId, userIdReceiver: 2 } as Message;
          const conversation = { userIdSender: 3, userIdReceiver: 4 } as PersistedConversation;
          expect(domainLogic.canUserSendMessage(userId, conversation, message)).toStrictEqual(false);
        });
      });

      describe('when receiver belongs to the conversation', () => {
        describe('when sender does not belong to the conversation', () => {
          it('should return false', () => {
            const userId = 1;
            const message = { userIdSender: userId, userIdReceiver: 2 } as Message;
            const conversation = { userIdSender: 3, userIdReceiver: 2 } as PersistedConversation;
            expect(domainLogic.canUserSendMessage(userId, conversation, message)).toStrictEqual(false);
          });
        });

        describe('when sender does belongs to the conversation', () => {
          it('should return true', () => {
            const userId = 1;
            const message = { userIdSender: userId, userIdReceiver: 2 } as Message;
            const conversation = { userIdSender: 2, userIdReceiver: userId } as PersistedConversation;
            expect(domainLogic.canUserSendMessage(userId, conversation, message)).toStrictEqual(true);
          });
        });
      });
    });
  });
});
