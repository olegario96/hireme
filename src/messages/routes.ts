import { Router } from 'express';
import { container } from 'tsyringe';

import { HttpStatusCode } from '../common/types';
import { Logger } from '../logger';
import {
  middlewareContentType,
  middlewareJoi,
  middlewareJwt,
} from '../middlewares';
import { CustomRequest } from '../middlewares/types';
import { MessageController } from './message-controller';
import { createMessageSchema } from './schema-validation';

const loadRoutes = (router: Router): void => {
  const controller = container.resolve(MessageController);
  const logger = container.resolve(Logger);

  router.post(
    '/message',
    middlewareContentType(),
    middlewareJoi(createMessageSchema),
    middlewareJwt(),
    async (req: CustomRequest, res) => {
      try {
        const userId = req.userId!;
        const message = req.body;
        if (userId !== message.userIdSender) {
          res.status(HttpStatusCode.FORBIDDEN).json({ error: 'You must be the sender when creating a message' });
          return;
        }

        const persistedMessage = await controller.createMessage(userId, message);
        if (!persistedMessage) {
          res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'You can not create a message for the provided conversation' });
          return;
        }

        res.status(HttpStatusCode.OK).json(persistedMessage);
      } catch (e) {
        const exception = e as Error;
        logger.error(`POST /message exception: ${exception.message}`);
        res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not authenticate user' });
      }
    });
};

module.exports = loadRoutes;
