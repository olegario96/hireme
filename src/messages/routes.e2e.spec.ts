/* eslint-disable jest/expect-expect */
import 'reflect-metadata';

import { Application } from 'express';
import request from 'supertest';
import { container } from 'tsyringe';

import { populateConversationsTable } from '../../seed/conversations';
import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { buildAppForTests, endConnections } from '../../tests/utils';
import { APPLICATION_JSON, AUTHORIZATION, CONTENT_TYPE } from '../common/constants';
import { HttpStatusCode } from '../common/types';
import { TABLE_CONVERSATIONS } from '../conversations/types';
import { Jwt } from '../security/jwt';
import { PersistedUser, TABLE_USERS, UserId } from '../users/entity';
import { MessageController } from './message-controller';
import { TABLE_MESSAGES } from './types';

describe('Conversations routes', () => {
  let app: Application;

  beforeAll(async () => {
    const context = __dirname;
    app = await buildAppForTests(context);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_MESSAGES),
      resetTable(TABLE_CONVERSATIONS),
      resetTable(TABLE_USERS),
    ]);

    await endConnections();
  });

  describe('POST /message', () => {
    describe('when invalid content type is provided', () => {
      it('should return bad request', async () => {
        await request(app).post('/message')
          .set(CONTENT_TYPE, 'application/javascript')
          .send()
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when valid content type is provided', () => {
      describe('when body request is invalid', () => {
        it('should return bad request', async () => {
          const body = { foo: 'bar' };
          await request(app).post('/message')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send(body)
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });

      describe('when body request is valid', () => {
        describe('when invalid JWT is provided', () => {
          it('should return unauthorized', async () => {
            const body = { userIdSender: 1, userIdReceiver: 2, conversationId: 1, content: 'foo' };
            await request(app).post('/message')
              .set(CONTENT_TYPE, APPLICATION_JSON)
              .set(AUTHORIZATION, 'Bearer token')
              .send(body)
              .expect(HttpStatusCode.UNAUTHORIZED);
          });
        });

        describe('when valid JWT is provided', () => {
          let jwt: Jwt;
          let userId: UserId;

          beforeAll(async () => {
            jwt = container.resolve(Jwt);
            const { id } = (await populateUsersTable())!;
            userId = id;
            const user = { email: 'general@kenobi.com' } as PersistedUser;
            await populateUsersTable(user);
            await populateConversationsTable();
          });

          describe('when sender is different from the authenticated user', () => {
            it('should return forbidden status', async () => {
              const body = { userIdSender: 3, userIdReceiver: 2, conversationId: 1, content: 'foo' };
              const token = jwt.signToken({ userId });
              await request(app).post('/message')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .set(AUTHORIZATION, `Bearer ${token}`)
                .send(body)
                .expect(HttpStatusCode.FORBIDDEN);
            });
          });

          describe('when sender is the same one who is authenticated', () => {
            describe('when conversation does not exist', () => {
              it('should return bad request', async () => {
                const body = { userIdSender: 1, userIdReceiver: 2, conversationId: 10, content: 'foo' };
                const token = jwt.signToken({ userId });
                await request(app).post('/message')
                  .set(CONTENT_TYPE, APPLICATION_JSON)
                  .set(AUTHORIZATION, `Bearer ${token}`)
                  .send(body)
                  .expect(HttpStatusCode.BAD_REQUEST);
              });
            });

            describe('when conversation exists', () => {
              it('should return ok', async () => {
                const body = { userIdSender: 1, userIdReceiver: 2, conversationId: 1, content: 'foo' };
                const token = jwt.signToken({ userId });
                await request(app).post('/message')
                  .set(CONTENT_TYPE, APPLICATION_JSON)
                  .set(AUTHORIZATION, `Bearer ${token}`)
                  .send(body)
                  .expect(HttpStatusCode.OK);
              });

              describe('when there is an exception', () => {
                let controller: MessageController;
                beforeAll(() => {
                  controller = container.resolve(MessageController);
                  jest.spyOn(controller, 'createMessage')
                    .mockRejectedValueOnce(() => {
                      throw new Error('Mocked');
                    });
                });

                it('should return internal server error', async () => {
                  const body = { userIdSender: 1, userIdReceiver: 2, conversationId: 1, content: 'foo' };
                  const token = jwt.signToken({ userId });
                  await request(app).post('/message')
                    .set(CONTENT_TYPE, APPLICATION_JSON)
                    .set(AUTHORIZATION, `Bearer ${token}`)
                    .send(body)
                    .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
                });
              });
            });
          });
        });
      });
    });
  });
});
