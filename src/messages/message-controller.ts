import { injectable, singleton } from 'tsyringe';

import { ConversationDao } from '../conversations/conversation-dao';
import { Logger } from '../logger';
import { UserId } from '../users/entity';
import { MessageDao } from './message-dao';
import { MessageDomainLogic } from './message-domain-logic';
import { Message, PersistedMessage } from './types';

@injectable()
@singleton()
export class MessageController {
  constructor(
    private readonly conversationDao: ConversationDao,
    private readonly logger: Logger,
    private readonly messageDao: MessageDao,
    private readonly messageDomainLogic: MessageDomainLogic,
  ) {}

  async createMessage(userId: UserId, message: Message): Promise<PersistedMessage | null> {
    const { conversationId } = message;
    const conversation = await this.conversationDao.getConversation(conversationId);
    if (!conversation) {
      return null;
    }

    const logPrefix = 'MessageController@createMessage';
    if (!conversation.isActive) {
      this.logger.warn(
        `${logPrefix} - Attempt to create messages for a inactive conversation detected. ConversationId: ${conversationId}`,
      );

      return null;
    }

    if (!this.messageDomainLogic.canUserSendMessage(userId, conversation, message)) {
      const context = `Conversation: ${JSON.stringify(conversation)} Message: ${JSON.stringify(message)}`;
      this.logger.warn(
        `${logPrefix} - Attempt to create messages for an unappropriated conversation. ${context}`,
      );

      return null;
    }

    return this.messageDao.createMessage(message);
  }
}
