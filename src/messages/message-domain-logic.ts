import { injectable, singleton } from 'tsyringe';

import { PersistedConversation } from '../conversations/types';
import { UserId } from '../users/entity';
import { Message } from './types';

@injectable()
@singleton()
export class MessageDomainLogic {
  canUserSendMessage(
    userId: UserId,
    conversation: PersistedConversation,
    message: Message,
  ): boolean {
    const { userIdSender: messageUserIdSender, userIdReceiver: messageUserIdReceiver } = message;
    if (userId !== messageUserIdSender) {
      return false;
    }

    const { userIdSender: conversationUserIdSender, userIdReceiver: conversationUserIdReceiver } = conversation;
    if (messageUserIdReceiver !== conversationUserIdSender && messageUserIdReceiver !== conversationUserIdReceiver) {
      return false;
    }

    return messageUserIdSender === conversationUserIdSender || messageUserIdSender === conversationUserIdReceiver;
  }
}
