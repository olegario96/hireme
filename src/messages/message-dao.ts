import { injectable, singleton } from 'tsyringe';

import { POSTGRES_INTEGER_MAX_VALUE } from '../common/constants';
import { PostgresDao } from '../common/postgres-dao';
import { TokenizedDaoArrayResponse } from '../common/types';
import { ConversationId } from '../conversations/types';
import { Message, MessageId, PersistedMessage, TABLE_MESSAGES } from './types';

@injectable()
@singleton()
export class MessageDao extends PostgresDao {
  static readonly PAGE_SIZE_MESSAGES = 50;

  async createMessage(message: Message): Promise<PersistedMessage> {
    const query = `
      INSERT INTO
        ${TABLE_MESSAGES} (
          conversation_id,
          response_to,
          user_id_sender,
          user_id_receiver,
          content,
          created_at,
          updated_at
        )
      VALUES (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7
      )
      RETURNING
        id
    `;

    const { timestamp, unixEpoch } = this.getDateTimestamp();
    const { content, conversationId, responseTo, userIdSender, userIdReceiver } = message;
    const values = [conversationId, responseTo, userIdSender, userIdReceiver, content, timestamp, timestamp];
    const [{ id }] = await this.db.executeQuery({ query, values });
    return {
      ...message,
      id,
      sentAt: null,
      createdAt: unixEpoch,
      updatedAt: unixEpoch,
      deletedAt: null,
    };
  }

  async getMessages(
    conversationId: ConversationId,
    nextPageToken: number = POSTGRES_INTEGER_MAX_VALUE,
  ): Promise<TokenizedDaoArrayResponse<PersistedMessage>> {
    const query = `
      SELECT
        *
      FROM
        ${TABLE_MESSAGES}
      WHERE
        id <= $1
        AND conversation_id = $2
        AND sent_at IS NOT NULL
        AND deleted_at IS NULL
      ORDER BY
        id DESC
      LIMIT
        $3
    `;

    // +1 to fetch next page token
    const values = [nextPageToken, conversationId, MessageDao.PAGE_SIZE_MESSAGES + 1];
    const messages = await this.db.executeQuery({ query, values });
    const pageToken = messages.length > MessageDao.PAGE_SIZE_MESSAGES ? messages.pop()!.id : null;
    return {
      data: messages,
      nextPageToken: pageToken,
    };
  }

  async markMessagesAsSent(messageIds: MessageId[]): Promise<void> {
    if (!messageIds.length) {
      return;
    }

    const query = `
      UPDATE
        ${TABLE_MESSAGES}
      SET
        sent_at = $1,
        updated_at = $2
      WHERE
        id = ANY($3::int[])
        AND deleted_at IS NULL
    `;

    const { timestamp } = this.getDateTimestamp();
    const values = [timestamp, timestamp, messageIds];
    await this.db.executeQuery({ query, values });
  }
}
