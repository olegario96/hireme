import 'reflect-metadata';

import { startServer } from './server';

const main = async (): Promise<void> => {
  await startServer();
};

const noop = () => ({});
main()
  .then(noop)
  .catch(noop);
