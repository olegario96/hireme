import { injectable, singleton } from 'tsyringe';

import { PostgresDao } from '../common/postgres-dao';
import { PersistedSkill, TABLE_SKILLS } from '../skills/entity';
import { JobOpportunity, JobOpportunityId, PersistedJobOpportunity, TABLE_JOB_OPPORTUNITIES, TABLE_JOB_OPPORTUNITIES_SKILLS } from './types';

@injectable()
@singleton()
export class JobOpportunityDao extends PostgresDao {
  private readonly OFFSET_JOB_OPPORTUNITY_ID = 1;
  private readonly OFFSET_SKILL_ID = 2;
  private readonly OFFSET_RECORD = 2;

  async createJobOpportunity(jobOpportunity: JobOpportunity): Promise<PersistedJobOpportunity> {
    const query = `
      INSERT INTO
        ${TABLE_JOB_OPPORTUNITIES} (
          company_id,
          name,
          description,
          is_remote,
          created_at,
          updated_at
        )
      VALUES (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6
      )
      RETURNING
        id
    `;

    const { name, description, isRemote, companyId, skills } = jobOpportunity;
    const { timestamp, unixEpoch } = this.getDateTimestamp();
    const values = [companyId, name, description, isRemote, timestamp, timestamp];
    const [{ id }] = await this.db.executeQuery({ query, values });
    const persistedJobOpportunity = {
      ...jobOpportunity,
      id,
      createdAt: unixEpoch,
      updatedAt: unixEpoch,
      deletedAt: null,
    };

    if (!skills || !skills.length) {
      return persistedJobOpportunity;
    }

    await this.linkJobOpportunityToSkills(persistedJobOpportunity.id, skills);
    return persistedJobOpportunity;
  }

  async getJobOpportunity(jobOpportunityId: JobOpportunityId): Promise<PersistedJobOpportunity | null> {
    const query = `
      SELECT
        jo.*,
        s.id AS "skillId",
        s.name AS "skillName",
        s.created_at AS "skillCreatedAt",
        s.updated_at AS "skillUpdatedAt",
        s.deleted_at AS "skillDeletedAt"
      FROM
        ${TABLE_JOB_OPPORTUNITIES} AS jo
      LEFT JOIN ${TABLE_JOB_OPPORTUNITIES_SKILLS} AS jos
        ON jo.id = jos.job_opportunity_id
        AND jos.deleted_at IS NULL
      LEFT JOIN ${TABLE_SKILLS} AS s
        ON s.id = jos.skill_id
        AND s.deleted_at IS NULL
      WHERE
        jo.id = $1
        AND jo.deleted_at IS NULL
    `;

    const values = [jobOpportunityId];
    const persistedJobOpportunity = await this.db.executeQuery({ query, values });
    if (!persistedJobOpportunity.length) {
      return null;
    }

    const [{ id, companyId, name, description, isRemote, createdAt, updatedAt, deletedAt }] = persistedJobOpportunity;
    return {
      /**
       * @FIXME
       */
      skills: [{} as PersistedSkill, {} as PersistedSkill],
      id,
      companyId,
      name,
      description,
      isRemote,
      createdAt,
      updatedAt,
      deletedAt,
    };
  }

  async deleteJobOpportunity(jobOpportunityId: JobOpportunityId): Promise<void> {
    const query = `
      UPDATE
        ${TABLE_JOB_OPPORTUNITIES}
      SET
        updated_at = $1,
        deleted_at = $2
      WHERE
        id = $3
        AND deleted_at IS NULL
    `;

    const { timestamp } = this.getDateTimestamp();
    const values = [timestamp, timestamp, jobOpportunityId];
    await this.db.executeQuery({ query, values });
    await this.unlinkJobOpportunitySkills(jobOpportunityId);
  }

  private async linkJobOpportunityToSkills(
    jobOpportunityId: JobOpportunityId,
    skills: PersistedSkill[],
  ): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const values: any[] = [];
    const queryPlaceholder: string[] = [];
    let query = `
      INSERT INTO
        ${TABLE_JOB_OPPORTUNITIES_SKILLS}
      VALUES
    `;

    skills.forEach((skill, idx) => {
      queryPlaceholder.push(`
        ($${idx * this.OFFSET_RECORD + this.OFFSET_JOB_OPPORTUNITY_ID}, $${idx * this.OFFSET_RECORD + this.OFFSET_SKILL_ID})
      `);

      values.push(jobOpportunityId, skill.id);
    });

    query += queryPlaceholder.join(',');
    await this.db.executeQuery({ query, values });
  }

  private async unlinkJobOpportunitySkills(jobOpportunityId: JobOpportunityId): Promise<void> {
    const query = `
      UPDATE
        ${TABLE_JOB_OPPORTUNITIES_SKILLS}
      SET
        updated_at = $1,
        deleted_at = $2
      WHERE
        job_opportunity_id = $3
        AND deleted_at IS NULL
    `;

    const { timestamp } = this.getDateTimestamp();
    const values = [timestamp, timestamp, jobOpportunityId];
    await this.db.executeQuery({ query, values });
  }
}
