import { injectable, singleton } from 'tsyringe';

import { SkillDao } from '../skills/skill-dao';
import { JobOpportunityDao } from './job-opportunity-dao';
import { JobOpportunityId, JobOpportunityRaw, PersistedJobOpportunity } from './types';

@injectable()
@singleton()
export class JobOpportunityController {
  constructor(
    private readonly jobOpportunityDao: JobOpportunityDao,
    private readonly skillDao: SkillDao,
  ) {}

  async createJobOpportunity(jobOpportunity: JobOpportunityRaw): Promise<PersistedJobOpportunity> {
    const { skillIds } = jobOpportunity;
    if (!skillIds || !skillIds.length) {
      return this.jobOpportunityDao.createJobOpportunity(jobOpportunity);
    }

    const skills = await this.skillDao.getSkills(skillIds);
    const targetJobOpportunity = { ...jobOpportunity, skills, skillIds: undefined };
    return this.jobOpportunityDao.createJobOpportunity(targetJobOpportunity);
  }

  getJobOpportunity(jobOpportunityDaoId: JobOpportunityId): Promise<PersistedJobOpportunity | null> {
    return this.jobOpportunityDao.getJobOpportunity(jobOpportunityDaoId);
  }

  deleteJobOpportunity(jobOpportunityDaoId: JobOpportunityId): Promise<void> {
    return this.jobOpportunityDao.deleteJobOpportunity(jobOpportunityDaoId);
  }
}
