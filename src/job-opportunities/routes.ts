import { Router } from 'express';
import { container } from 'tsyringe';

import { HttpStatusCode } from '../common/types';
import { Logger } from '../logger';
import { contentTypeJsonHeader } from '../middlewares/content-type';
import { JobOpportunityController } from './job-opportunity-controller';

const loadRoutes = (router: Router): void => {
  const controller = container.resolve(JobOpportunityController);
  const logger = container.resolve(Logger);

  /**
   * @api [get] /job-opportunity/{jobOpportunityId}
   * bodyContentType: "application/json"
   * description: "Get specific job opportunity"
   * parameters:
   *   - name: jobOpportunityId
   *     in: path
   *     description: "Job opportunity id"
   *     required: true
   *     schema:
   *       type: "integer"
   *       example: 1
   * responses:
   *   "200":
   *     description: "Job opportunity"
   *     schema:
   *       type: "object"
   *       properties:
   *         id:
   *           type: "integer"
   *           example: "1"
   *         companyId:
   *           type: "integer"
   *           example: "1"
   *         name:
   *           type: "string"
   *           example: "Senior Software Engineer"
   *         skills:
   *           type: "array"
   *           items:
   *             type: "object"
   *             properties:
   *               id:
   *                 type: "integer"
   *                 example: "1"
   *               name:
   *                 type: "string"
   *                 example: "Typescript"
   *               createdAt:
   *                 type: "integer"
   *                 example: 1642710245
   *               updatedAt:
   *                 type: "integer"
   *                 example: 1642710245
   *               deletedAt:
   *                 type: "integer"
   *                 example: 1642710245
   *         description:
   *           type: "string"
   *           example: "Be the best!"
   *         isRemote:
   *           type: "boolean"
   *           example: true
   *         createdAt:
   *           type: "integer"
   *           example: 1642710245
   *         updatedAt:
   *           type: "integer"
   *           example: 1642710245
   *         deletedAt:
   *           type: "integer"
   *           example: 1642710245
   *   "404":
   *     description: "Message indicating inexistent job opportunity"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Invalid job opportunityId provided"
   *   "500":
   *     description: "Message indicating failure"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Could not retrieve job opportunity"
   */
  router.get('/job-opportunity/:id', contentTypeJsonHeader(), async (req, res) => {
    const { id: jobOpportunityId } = req.params;
    try {
      const jobOpportunity = await controller.getJobOpportunity(Number(jobOpportunityId));
      if (!jobOpportunity) {
        res.status(HttpStatusCode.NOT_FOUND).json({ error: 'Invalid job opportunityId provided' });
        return;
      }

      res.status(HttpStatusCode.OK).json(jobOpportunity);
    } catch (e) {
      const exception = e as Error;
      logger.error(`POST /job-opportunity exception: ${exception.message}`);
      res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not retrieve job opportunity' });
    }
  });
};

module.exports = loadRoutes;
