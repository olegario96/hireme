/* eslint-disable @typescript-eslint/no-magic-numbers */
import 'reflect-metadata';

import { container } from 'tsyringe';

import { populateCompaniesTable } from '../../seed/companies';
import { resetTable } from '../../seed/helper';
import { populateSkillsTable } from '../../seed/skills';
import { endConnections } from '../../tests/utils';
import { CompanyId, TABLE_COMPANIES } from '../companies/entity';
import { connection } from '../database/connection';
import { SkillDao } from '../skills/skill-dao';
import { PersistedSkill, SkillId } from '../skills/types';
import { JobOpportunityController } from './job-opportunity-controller';
import { TABLE_JOB_OPPORTUNITIES, TABLE_JOB_OPPORTUNITIES_SKILLS } from './types';

describe('JobOpportunityController', () => {
  let companyId: CompanyId;
  let controller: JobOpportunityController;

  beforeAll(async () => {
    controller = container.resolve(JobOpportunityController);
    await connection.initialize();
    await populateCompaniesTable();
    companyId = 1;
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_JOB_OPPORTUNITIES_SKILLS),
      resetTable(TABLE_JOB_OPPORTUNITIES),
      resetTable(TABLE_COMPANIES),
    ]);

    await endConnections();
  });

  describe('createJobOpportunity', () => {
    describe('when job opportunity has no skills', () => {
      it('should create job opportunity with no skills', async () => {
        const jobOpportunity = { companyId, name: 'Senior Software Engineer', description: 'Don\'t', isRemote: true };
        const persistedJobOpportunity= await controller.createJobOpportunity(jobOpportunity);
        expect(persistedJobOpportunity).toHaveProperty('id');
        expect(persistedJobOpportunity.skills).toStrictEqual(undefined);
      });
    });

    describe('when job opportunity requires skills', () => {
      let skillIds: SkillId[];

      beforeAll(async () => {
        const override = { name: 'C++' } as PersistedSkill;
        await Promise.all([
          populateSkillsTable(),
          populateSkillsTable(override),
        ]);

        const skills = (await container.resolve(SkillDao).getSkills([1, 2]))!;
        skillIds = skills.map(skill => skill.id);
      });

      it('should create job opportunity with skills', async () => {
        const jobOpportunity = { companyId, name: 'Senior Software Engineer', description: 'Don\'t', isRemote: true, skillIds };
        const persistedJobOpportunity= await controller.createJobOpportunity(jobOpportunity);
        expect(persistedJobOpportunity).toHaveProperty('skills');
        expect(persistedJobOpportunity.skills).toHaveLength(2);
      });
    });
  });

  describe('getJobOpportunity', () => {
    it('should return persisted job opportunity', async () => {
      const jobOpportunityId = 1;
      const persistedJobOpportunity = (await controller.getJobOpportunity(jobOpportunityId))!;
      expect(persistedJobOpportunity.id).toStrictEqual(jobOpportunityId);
    });
  });

  describe('deleteJobOpportunity', () => {
    it('should soft delete persisted job opportunity', async () => {
      const jobOpportunityId = 1;
      await controller.deleteJobOpportunity(jobOpportunityId);
      const persistedJobOpportunity = await controller.getJobOpportunity(jobOpportunityId);
      expect(persistedJobOpportunity).toStrictEqual(null);
    });
  });
});
