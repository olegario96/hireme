/* eslint-disable jest/expect-expect */
import 'reflect-metadata';

import { Application } from 'express';
import request from 'supertest';
import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateJobOpportunitiesTable } from '../../seed/job-opportunities';
import { buildAppForTests, endConnections } from '../../tests/utils';
import { APPLICATION_JSON, CONTENT_TYPE } from '../common/constants';
import { HttpStatusCode } from '../common/types';
import { TABLE_COMPANIES } from '../companies/entity';
import { JobOpportunityController } from './job-opportunity-controller';
import { TABLE_JOB_OPPORTUNITIES } from './types';

describe('Job opportunities routes', () => {
  let app: Application;

  beforeAll(async () => {
    const context = __dirname;
    app = await buildAppForTests(context);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_JOB_OPPORTUNITIES),
      resetTable(TABLE_COMPANIES),
    ]);

    await endConnections();
  });

  describe('GET /job-opportunity/:id', () => {
    describe('when id is not provided', () => {
      it('should return not found', async () => {
        await request(app).get('/job-opportunity/1')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send({})
          .expect(HttpStatusCode.NOT_FOUND);
      });
    });

    describe('when provided id exists', () => {
      beforeAll(async () => {
        await populateJobOpportunitiesTable();
      });

      it('should return ok status', async () => {
        await request(app).get('/job-opportunity/1')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send({})
          .expect(HttpStatusCode.OK);
      });
    });

    describe('when database is down', () => {
      let controller: JobOpportunityController;

      beforeAll(() => {
        controller = container.resolve(JobOpportunityController);
        jest.spyOn(controller, 'getJobOpportunity')
          .mockRejectedValueOnce(() => {
            throw new Error('Mocked');
          });
      });

      it('should return internal server error', async () => {
        await request(app).get('/job-opportunity/1')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send({})
          .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
      });
    });
  });
});
