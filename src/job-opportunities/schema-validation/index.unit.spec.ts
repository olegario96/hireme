import { createJobOpportunitySchema } from '.';

describe('SchemaValidation', () => {
  describe('createJobOpportunitySchema', () => {
    describe('when invalid object is provided', () => {
      it('should provide details about failure', () => {
        const jobOpportunity = { companyId: 1, name: 'hireme', skillIds: ['a'], description: 'foo', isRemote: true };
        const validatedObj = createJobOpportunitySchema.validate(jobOpportunity);
        expect(validatedObj.error.details).toHaveLength(1);
      });
    });

    describe('when valid object is provided', () => {
      it('should return validated object', () => {
        const jobOpportunity = { companyId: 1, name: 'hireme', description: 'foo', isRemote: true };
        const validatedObj = createJobOpportunitySchema.validate(jobOpportunity);
        expect(validatedObj.value).toMatchObject(jobOpportunity);
      });
    });
  });
});
