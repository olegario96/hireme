import * as Joi from 'joi';

const SKILLS_HARD_LIMIT = 20;

export const createJobOpportunitySchema = Joi.object({
  companyId: Joi.number().required(),
  name: Joi.string().required(),
  skillIds: Joi.array()
    .items(Joi.number().integer())
    .max(SKILLS_HARD_LIMIT)
    .optional(),
  description: Joi.string().required(),
  isRemote: Joi.boolean().required(),
});
