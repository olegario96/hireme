import 'reflect-metadata';

import { container } from 'tsyringe';

import { populateCompaniesTable } from '../../seed/companies';
import { resetTable } from '../../seed/helper';
import { endConnections } from '../../tests/utils';
import { CompanyId, TABLE_COMPANIES } from '../companies/entity';
import { connection } from '../database/connection';
import { PersistedSkill } from '../skills/entity';
import { SkillDao } from '../skills/skill-dao';
import { JobOpportunityDao } from './job-opportunity-dao';
import { TABLE_JOB_OPPORTUNITIES, TABLE_JOB_OPPORTUNITIES_SKILLS } from './types';

/* eslint-disable @typescript-eslint/no-magic-numbers */
describe('JobOpportunityDao', () => {
  let companyId: CompanyId;
  let dao: JobOpportunityDao;

  beforeAll(async () => {
    dao = container.resolve(JobOpportunityDao);
    await connection.initialize();
    await populateCompaniesTable();
    companyId = 1;
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_JOB_OPPORTUNITIES_SKILLS),
      resetTable(TABLE_JOB_OPPORTUNITIES),
      resetTable(TABLE_COMPANIES),
    ]);

    await endConnections();
  });

  describe('createJobOpportunity', () => {
    describe('when no skill is provided', () => {
      it('should persist job opportunity', async () => {
        const jobOpportunity = { companyId, name: 'Senior Software Engineer', description: 'Don\'t', isRemote: true };
        const persistedJobOpportunity= await dao.createJobOpportunity(jobOpportunity);
        expect(persistedJobOpportunity).toHaveProperty('id');
        expect(persistedJobOpportunity.skills).toStrictEqual(undefined);
      });
    });

    describe('when skills are provided', () => {
      let skills: PersistedSkill[];

      beforeAll(async () => {
        // @see migrations/1673542811579-populate-skills.ts
        skills = (await container.resolve(SkillDao).getSkills([1, 2]))!;
      });

      it('should return persisted job opportunity along with its skills', async () => {
        const jobOpportunity = { companyId, name: 'Senior Software Engineer', description: 'Don\'t', isRemote: true, skills };
        const persistedJobOpportunity= await dao.createJobOpportunity(jobOpportunity);
        const { skills: persistedSkills } = persistedJobOpportunity;
        const skillsNames = persistedSkills!.map(s => s.name);
        expect(skillsNames).toHaveLength(2);
        expect(skillsNames).toContain('Typescript');
        expect(skillsNames).toContain('Postgres');
      });
    });
  });

  describe('getJobOpportunity', () => {
    describe('when invalid jobId is provided', () => {
      it('should return nullptr', async () => {
        const jobOpportunityId = 10;
        const persistedJobOpportunity = await dao.getJobOpportunity(jobOpportunityId);
        expect(persistedJobOpportunity).toStrictEqual(null);
      });
    });

    describe('when valid jobId is provided', () => {
      it('should return persisted job opportunity', async () => {
        const jobOpportunityId = 2;
        const persistedJobOpportunity = await dao.getJobOpportunity(jobOpportunityId);
        expect(persistedJobOpportunity!.skills).toHaveLength(2);
      });
    });
  });

  describe('deleteJobOpportunity', () => {
    it('should soft delete records', async () => {
      const jobOpportunityId = 2;
      await dao.deleteJobOpportunity(jobOpportunityId);
      const persistedJobOpportunity = await dao.getJobOpportunity(jobOpportunityId);
      expect(persistedJobOpportunity).toStrictEqual(null);
    });
  });
});
