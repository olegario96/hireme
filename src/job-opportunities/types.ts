import { MetaFields } from '../common/types';
import { CompanyId } from '../companies/entity';
import { PersistedSkill, SkillId } from '../skills/entity';

export type JobOpportunityId = number;
export interface PersistedJobOpportunity extends MetaFields {
  id: JobOpportunityId;
  companyId: CompanyId;
  name: string;
  skills?: PersistedSkill[] | null;
  description: string;
  isRemote: boolean;
}

export type JobOpportunity = Omit<PersistedJobOpportunity, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt'>;
export type JobOpportunityRaw = Omit<JobOpportunity, 'skills'> & {
  skillIds?: SkillId[];
};

export const TABLE_JOB_OPPORTUNITIES = 'job_opportunities';
export const TABLE_JOB_OPPORTUNITIES_SKILLS = 'job_opportunities_skills';
