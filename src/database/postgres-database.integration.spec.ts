import 'reflect-metadata';

import { container } from 'tsyringe';

import { endConnections } from '../../tests/utils';
import { PostgresDatabase } from './postgres-database';

describe('PostgresDatabase', () => {
  let db: PostgresDatabase;
  beforeAll(() => {
    db = container.resolve(PostgresDatabase);
  });

  afterAll(async () => {
    await endConnections();
  });

  describe('executeQuery', () => {
    it('should execute query and return results', async () => {
      const [{ now }] = await db.executeQuery({ query: 'SELECT NOW()' });
      expect(Date.parse(now)).not.toStrictEqual(NaN);
    });
  });
});
