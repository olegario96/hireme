/* eslint-disable @typescript-eslint/no-magic-numbers */
import { ONE_SECOND_IN_MS } from '../common/constants';
import { UnixEpoch } from '../common/types';
import { dateMinutesAgo, dateToUnixEpoch, normalizeDbResult, unixEpochToISOString } from './utils';

describe('Utils', () => {
  let now: UnixEpoch;
  describe('normalizeDbResult', () => {
    it.each([
      { very_long_snake_case: 'asdf', very_long_camel_case: 1 },
      { very_long_snake_case: 'asdf', very_long_camelCase: 1 },
      { very_long_snakeCase: 'asdf', veryLong_camel_case: 1 },
      { very_long_SnakeCase: 'asdf', very_LongCamel_case: 1 },
      { very_longSnakeCase: 'asdf', veryLongCamel_case: 1 },
      { veryLongSnakeCase: 'asdf', veryLongCamelCase: 1 },
    ])('converts snake case fields to camel case', (dbResult) => {
      const normalizedObject = normalizeDbResult(dbResult);
      expect(normalizedObject.veryLongSnakeCase).toStrictEqual('asdf');
      expect(normalizedObject.veryLongCamelCase).toStrictEqual(1);
    });

    describe('when includes META_FIELDS', () => {
      it('should parse date fields to unix epoch', () => {
        const currentDate = new Date();
        const dbResult = { created_at: currentDate };
        const normalizedResult = normalizeDbResult(dbResult);
        expect(normalizedResult.createdAt).toStrictEqual(Math.floor(currentDate.getTime() / ONE_SECOND_IN_MS));
      });
    });
  });

  describe('dateToUnixEpoch', () => {
    beforeAll(() => {
      // 2021-02-01 01:48:33
      now = 1612144113000;
      Date.now = jest.fn(() => now);
    });

    it('should return date translated to unix epoch', () => {
      const expectedResult = Math.floor(now / ONE_SECOND_IN_MS);
      expect(dateToUnixEpoch(new Date(Date.now()))).toStrictEqual(expectedResult);
    });
  });

  describe('dateMinutesAgo', () => {
    beforeAll(() => {
      // 2021-02-01 01:48:33
      now = 1612144113000;
      Date.now = jest.fn(() => now);
    });

    it('should calculate timestamp for date minutes ago', () => {
      const minutesAgo = 5;
      const currentDate = new Date(Date.now());
      const targetDate = dateMinutesAgo(currentDate, minutesAgo);
      expect(targetDate.toISOString()).toStrictEqual('2021-02-01T01:43:33.000Z');
    });
  });

  describe('unixEpochToISOString', () => {
    it('should convert unix epoch to ISO string', () => {
      const unixEpoch = 1612144113;
      const result = unixEpochToISOString(unixEpoch);
      const expectedResult = '2021-02-01T01:48:33.000Z';
      expect(expectedResult).toStrictEqual(result);
    });
  });
});
