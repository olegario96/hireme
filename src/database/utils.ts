import { ONE_SECOND_IN_MS, SECONDS_IN_A_MINUTE } from '../common/constants';
import { UnixEpoch } from '../common/types';
import { META_FIELDS, QueryResult } from './types';

export const normalizeDbResult = (dbResult: QueryResult): QueryResult => {
  const normalizedResult: QueryResult = {};
  Object.keys(dbResult).forEach((field) => {
    if (!isSnakeCase(field) && isCamelCase(field)) {
      normalizedResult[field] = dbResult[field];
      return;
    }

    const fieldToNormalize = field.split('_');
    let fieldNormalized = fieldToNormalize.shift()!;
    fieldToNormalize.forEach((targetField) => {
      const characters = targetField.split('');
      fieldNormalized += characters.shift()!.toUpperCase();
      characters.forEach((character) => (fieldNormalized += character));
    });

    if (META_FIELDS.includes(fieldNormalized) && dbResult[field]) {
      normalizedResult[fieldNormalized] = dateToUnixEpoch(new Date(dbResult[field]));
      return;
    }

    normalizedResult[fieldNormalized] = dbResult[field];
  });

  return normalizedResult;
};

export const dateToUnixEpoch = (date: Date): UnixEpoch => Math.floor(date.getTime() / ONE_SECOND_IN_MS);

export const dateMinutesAgo = (date: Date, minutes: number): Date => {
  const unixEpoch = dateToUnixEpoch(date);
  const targetUnixEpoch = unixEpoch - minutes * SECONDS_IN_A_MINUTE;
  return new Date(targetUnixEpoch * ONE_SECOND_IN_MS);
};

export const unixEpochToISOString = (epoch: UnixEpoch): string => new Date(epoch * ONE_SECOND_IN_MS).toISOString();

const isSnakeCase = (field: string): boolean => field.includes('_');

const isCamelCase = (field: string): boolean => field !== field.toUpperCase() && field !== field.toLowerCase();
