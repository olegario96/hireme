import { container } from 'tsyringe';
import { DataSource } from 'typeorm';

import { PersistedCompany } from '../companies/entity';
import { Config } from '../config/index';
import { PersistedTwoFaToken } from '../security/two-fa/entity';
import { PersistedLoginAttempt } from '../server/entity';
import { PersistedSkill } from '../skills/entity';
import { PersistedUser } from '../users/entity';

const config = container.resolve(Config);
config.ensureEnvironmentVariableIsDefined([
  'POSTGRES_USER',
  'POSTGRES_HOST',
  'POSTGRES_PASSWORD',
  'POSTGRES_DB',
  'POSTGRES_PORT',
]);

export const connection = new DataSource({
  type: 'postgres',
  host: process.env.POSTGRES_HOST!,
  port: Number(process.env.POSTGRES_PORT!),
  username: process.env.POSTGRES_USER!,
  password: process.env.POSTGRES_PASSWORD!,
  database: process.env.POSTGRES_DB!,
  synchronize: false,
  logging: false,
  entities: [
    PersistedCompany,
    PersistedLoginAttempt,
    PersistedSkill,
    PersistedTwoFaToken,
    PersistedUser,
  ],
  migrations: [
    'migrations/*.ts',
  ],
});
