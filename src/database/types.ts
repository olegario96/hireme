/* eslint-disable @typescript-eslint/no-explicit-any */
export type QueryParams = any[];
export type QueryResult = any;
export interface QueryConfig {
  query: string;
  values?: QueryParams;
}

export const META_FIELDS = ['createdAt', 'updatedAt', 'deletedAt'];
export const ENTITIES = [];
