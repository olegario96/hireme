import 'reflect-metadata';

import { connection } from './connection';

describe('connection', () => {
  it('should execute query', async () => {
    const query = 'SELECT NOW()';
    await connection.initialize();
    const [{ now }] = await connection.query(query);
    expect(Date.parse(now)).not.toStrictEqual(NaN);
  });
});
