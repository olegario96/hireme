import { Pool, types } from 'pg';
import { container, injectable, singleton } from 'tsyringe';

import { Config } from '../config';
import { QueryConfig, QueryResult } from './types';
import { normalizeDbResult } from './utils';

@injectable()
@singleton()
export class PostgresDatabase {
  private readonly pool: Pool;

  constructor() {
    this.ensureEnvironmentVariablesAreDefined();
    this.pool = new Pool({
      user: process.env.POSTGRES_USER!,
      host: process.env.POSTGRES_HOST!,
      password: process.env.POSTGRES_PASSWORD!,
      database: process.env.POSTGRES_DB!,
      port: Number(process.env.POSTGRES_PORT!),
    });

    const timestampWithoutTimezone = 1114;
    types.setTypeParser(timestampWithoutTimezone, (stringValue) => new Date(`${stringValue  }+0000`));
  }

  async executeQuery(queryConfig: QueryConfig): Promise<QueryResult> {
    const { query, values } = queryConfig;
    const result = await this.pool.query({ text: query, values });
    return result.rows.map((row: QueryResult) => normalizeDbResult(row));
  }

  /**
   * WARNING: This method should not be used out of automated
   * tests context.
   */
  async endConnections(): Promise<void> {
    await this.pool.end();
  }

  private ensureEnvironmentVariablesAreDefined(): void {
    const config = container.resolve(Config);
    config.ensureEnvironmentVariableIsDefined([
      'POSTGRES_USER',
      'POSTGRES_HOST',
      'POSTGRES_PASSWORD',
      'POSTGRES_DB',
      'POSTGRES_PORT',
    ]);
  }
}
