import 'reflect-metadata';

import { container } from 'tsyringe';

const mockInfo = jest.fn();
const mockWarn = jest.fn();
const mockError = jest.fn();
jest.mock('bunyan', () => ({
  createLogger: jest.fn().mockImplementation(() => ({
    info: mockInfo,
    warn: mockWarn,
    error: mockError,
  })),
}));

import { Logger } from '.';

describe('Logger', () => {
  let logger: Logger;
  beforeAll(() => {
    logger = container.resolve(Logger);
  });

  describe('log', () => {
    it('print message', () => {
      const message = 'message';
      logger.log(message);
      expect(mockInfo).toHaveBeenCalledWith(message);
    });
  });

  describe('warn', () => {
    it('print message', () => {
      const message = 'message';
      logger.warn(message);
      expect(mockWarn).toHaveBeenCalledWith(message);
    });
  });

  describe('error', () => {
    it('print message', () => {
      const message = 'message';
      logger.error(message);
      expect(mockError).toHaveBeenCalledWith(message);
    });
  });
});
