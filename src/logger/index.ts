import * as Bunyan from 'bunyan';
import { injectable, singleton } from 'tsyringe';

import { Config } from '../config';

@injectable()
@singleton()
export class Logger {
  private readonly bunyan: Bunyan;

  constructor(private readonly config: Config) {
    this.config.ensureEnvironmentVariableIsDefined(['APP_NAME']);
    this.bunyan = Bunyan.createLogger({ name: process.env.APP_NAME! });
  }

  log(message: string): void {
    this.bunyan.info(message);
  }

  warn(message: string): void {
    this.bunyan.warn(message);
  }

  error(message: string):void {
    this.bunyan.error(message);
  }
}
