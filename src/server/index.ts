/* eslint-disable @typescript-eslint/no-require-imports, @typescript-eslint/no-var-requires */
import { readdirSync } from 'fs';
import express = require('express');
import boom = require('express-boom');
import { Router } from 'express';
import PromiseRouter from 'express-promise-router';
import { container } from 'tsyringe';

import { isFile } from '../common/utils';
import { Config } from '../config';
import { connection } from '../database/connection';
import { Logger } from '../logger';
import { AppRouter } from './types';

const ROUTES_FILENAME_SUFFIX = 'routes';
export const startServer = async (): Promise<void> => {
  const logger = container.resolve(Logger);
  try {
    await connection.initialize();
  } catch (e) {
    logger.error(`API could not be started due to the DB connection issue: ${e.message}`);
    return;
  }

  container.resolve(Config).ensureEnvironmentVariableIsDefined([
    'HOST',
    'NODE_ENV',
    'NODE_PORT',
  ]);

  const { app, router } = createAppRouter();
  loadServerRoutes(router);
  app.bind(process.env.HOST!);
  logLoadedRoutes(router);
  const port = Number(process.env.NODE_PORT!);
  app.listen(port, () => {
    logger.log(`API is listening on port ${port}`);
  });
};

const createAppRouter = (): AppRouter => {
  const app = express();
  app.use(express.json());
  app.use(boom());
  const router = PromiseRouter();
  app.use(router);
  return { app, router };
};

const logLoadedRoutes = (router: Router): void => {
  const logger = container.resolve(Logger);
  router.stack.forEach(layer => {
    const { methods, path } = layer.route;
    Object.keys(methods).forEach(method =>
      logger.log(`${method.toUpperCase()} ${path} has been loaded!`)
    );
  });
};

const loadServerRoutes = (router: Router, directory: string|null = null): void => {
  const targetDirectory = !directory ? `${__dirname}/..` : directory;
  const directoryContent = readdirSync(targetDirectory);
  if (!directoryContent.length) {
    return;
  }

  for (const content of directoryContent) {
    const targetPath = `${targetDirectory}/${content}`;
    if (!isFile(targetPath)) {
      loadServerRoutes(router, targetPath);
      continue;
    }

    if (!targetPath.includes(ROUTES_FILENAME_SUFFIX)) {
      continue;
    }

    require(targetPath)(router);
  }
};
