import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { MetaFields } from '../common/entity';

export type LoginAttemptId = number;
export const TABLE_LOGIN_ATTEMPTS = 'login_attempts';

@Entity(TABLE_LOGIN_ATTEMPTS)
export class PersistedLoginAttempt extends MetaFields {
  @PrimaryGeneratedColumn()
  id: LoginAttemptId;

  @Column()
  ip: string;
}

export type LoginAttempt = Omit<PersistedLoginAttempt, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt'>;
