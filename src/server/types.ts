import { Router } from 'express';
import { Express } from 'express-serve-static-core';

export interface AppRouter {
  app: Express;
  router: Router;
}
