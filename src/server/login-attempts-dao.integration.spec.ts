import 'reflect-metadata';

import MockDate from 'mockdate';
import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { TABLE_LOGIN_ATTEMPTS } from './entity';
import { LoginAttemptsDao } from './login-attempts-dao';

describe('LoginAttemptsDao', () => {
  let dao: LoginAttemptsDao;
  let ip: string;

  beforeAll(async () => {
    await connection.initialize();
    dao = container.resolve(LoginAttemptsDao);
    ip = '127.0.0.1';
  });

  afterAll(async () => {
    await resetTable(TABLE_LOGIN_ATTEMPTS);
    await endConnections();
  });

  describe('createLoginAttemptForIp', () => {
    describe('when new ip is provided', () => {
      it('should return persisted login attempt', async () => {
        const persistedLoginAttempt = await dao.createLoginAttemptForIp(ip);
        expect(persistedLoginAttempt.ip).toStrictEqual(ip);
        expect(persistedLoginAttempt.deletedAt).toStrictEqual(null);
      });
    });
  });

  describe('getLoginAttemptsForIp', () => {
    describe('when ip is not available', () => {
      it('should return empty list', async () => {
        const targetIp = '192.30.127.35';
        const persistedLoginAttempts = await dao.getLoginAttemptsForIp(targetIp);
        expect(persistedLoginAttempts).toStrictEqual([]);
      });
    });

    describe('when ip is available', () => {
      beforeEach(async () => {
        // 2021-02-01 01:48:33
        // eslint-disable-next-line @typescript-eslint/no-magic-numbers
        MockDate.set(new Date(1466424490000));
        await dao.createLoginAttemptForIp(ip);
        MockDate.reset();
      });

      it('should return created login attempts during last minutes', async () => {
        const expectedAttempts = 2;
        const persistedLoginAttempts = await dao.getLoginAttemptsForIp(ip);
        expect(persistedLoginAttempts).toHaveLength(expectedAttempts);
      });
    });
  });
});
