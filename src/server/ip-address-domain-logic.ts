import { injectable, singleton } from 'tsyringe';

// based on the following implementation
// @see https://github.com/validatorjs/validator.js/blob/master/src/lib/isIP.js#L31-L45
const IPV4_SEGMENT_FORMAT = '(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])';
const IPV4_ADDRESS_FORMAT = `(${IPV4_SEGMENT_FORMAT}[.]){3}${IPV4_SEGMENT_FORMAT}`;
const IPV4_ADDRESS_REGEXP = new RegExp(`^${IPV4_ADDRESS_FORMAT}$`);

const IPV6_SEGMENT_FORMAT = '(?:[0-9a-fA-F]{1,4})';
const IPV6_ADDRESS_REGEXP = new RegExp('^(' +
  `(?:${IPV6_SEGMENT_FORMAT}:){7}(?:${IPV6_SEGMENT_FORMAT}|:)|` +
  `(?:${IPV6_SEGMENT_FORMAT}:){6}(?:${IPV4_ADDRESS_FORMAT}|:${IPV6_SEGMENT_FORMAT}|:)|` +
  `(?:${IPV6_SEGMENT_FORMAT}:){5}(?::${IPV4_ADDRESS_FORMAT}|(:${IPV6_SEGMENT_FORMAT}){1,2}|:)|` +
  `(?:${IPV6_SEGMENT_FORMAT}:){4}(?:(:${IPV6_SEGMENT_FORMAT}){0,1}:${IPV4_ADDRESS_FORMAT}|(:${IPV6_SEGMENT_FORMAT}){1,3}|:)|` +
  `(?:${IPV6_SEGMENT_FORMAT}:){3}(?:(:${IPV6_SEGMENT_FORMAT}){0,2}:${IPV4_ADDRESS_FORMAT}|(:${IPV6_SEGMENT_FORMAT}){1,4}|:)|` +
  `(?:${IPV6_SEGMENT_FORMAT}:){2}(?:(:${IPV6_SEGMENT_FORMAT}){0,3}:${IPV4_ADDRESS_FORMAT}|(:${IPV6_SEGMENT_FORMAT}){1,5}|:)|` +
  `(?:${IPV6_SEGMENT_FORMAT}:){1}(?:(:${IPV6_SEGMENT_FORMAT}){0,4}:${IPV4_ADDRESS_FORMAT}|(:${IPV6_SEGMENT_FORMAT}){1,6}|:)|` +
  `(?::((?::${IPV6_SEGMENT_FORMAT}){0,5}:${IPV4_ADDRESS_FORMAT}|(?::${IPV6_SEGMENT_FORMAT}){1,7}|:))` +
  ')(%[0-9a-zA-Z-.:]{1,})?$');

@injectable()
@singleton()
export class IpAddressDomainLogic {
  isIpValid(ip: string|undefined): boolean {
    if (!ip) {
      return false;
    }

    if (IPV6_ADDRESS_REGEXP.test(ip)) {
      return true;
    }

    if (!IPV4_ADDRESS_REGEXP.test(ip)) {
      return false;
    }

    const ipv4MaxLimit = 255;
    const lastIndex = 3;
    const ipNumbers = ip
      .split('.')
      .map(Number)
      .sort((a, b) => a - b);

    return ipNumbers[lastIndex]! <= ipv4MaxLimit;
  }
}
