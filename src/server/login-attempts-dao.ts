import { injectable, singleton } from 'tsyringe';
import { IsNull, MoreThanOrEqual } from 'typeorm';

import { connection } from '../database/connection';
import { dateMinutesAgo } from '../database/utils';
import { PersistedLoginAttempt } from './entity';

@injectable()
@singleton()
export class LoginAttemptsDao {
  private readonly repository;

  constructor() {
    this.repository = connection.getRepository(PersistedLoginAttempt);
  }

  async createLoginAttemptForIp(ip: string): Promise<PersistedLoginAttempt> {
    const loginAttempt = new PersistedLoginAttempt();
    loginAttempt.ip = ip;
    return this.repository.save(loginAttempt);
  }

  async getLoginAttemptsForIp(ip: string, timeIntervalInMinutes = 5): Promise<PersistedLoginAttempt[]> {
    const minutesAgo = dateMinutesAgo(new Date(), timeIntervalInMinutes);
    return this.repository.find({
      where: {
        ip,
        createdAt: MoreThanOrEqual(minutesAgo),
        deletedAt: IsNull(),
      },
    });
  }
}
