import 'reflect-metadata';

import { container } from 'tsyringe';

import { IpAddressDomainLogic } from './ip-address-domain-logic';

describe('IpAddressDomainLogic', () => {
  let domainLogic: IpAddressDomainLogic;
  beforeAll(() => {
    domainLogic = container.resolve(IpAddressDomainLogic);
  });

  describe('isIpValid', () => {
    it.each([
      { ip: undefined, expectedOutput: false },
      { ip: '127.0.0.1', expectedOutput: true },
      { ip: '192.30.127.35', expectedOutput: true },
      { ip: 'localhost', expectedOutput: false },
      { ip: '127..0.1', expectedOutput: false },
      { ip: '43ea:f00:24bd:c702:2775:ffff:e038:89e4', expectedOutput: true },
      { ip: '8569:7f26:b4f9:ca93:115d:efa0:b968:ccb4', expectedOutput: true },
      { ip: '8569:7f26:b4f9:ca93:115d:efa0:XXX:ccb4', expectedOutput: false },
    ])('validates ips', ({ ip, expectedOutput }) => {
      expect(domainLogic.isIpValid(ip)).toStrictEqual(expectedOutput);
    });
  });
});
