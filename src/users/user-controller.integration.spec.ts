import 'reflect-metadata';

import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { Jwt } from '../security/jwt';
import { PersistedUser, TABLE_USERS } from './entity';
import { UserController } from './user-controller';

describe('UserController', () => {
  let controller: UserController;

  beforeAll(async () => {
    await connection.initialize();
    controller = container.resolve(UserController);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_USERS),
    ]);

    await endConnections();
  });

  describe('authenticateUser', () => {
    let email: string;
    let password: string;

    describe('when user does not exist', () => {
      beforeAll(() => {
        email = 'luke@skywalker.com';
        password = 'princessLeia';
      });

      it('should return null', async () => {
        const result = await controller.authenticateUser(email, password);
        expect(result).toStrictEqual(null);
      });
    });

    describe('when user exists', () => {

      beforeAll(async () => {
        email = 'darth@sidious.com';
        await populateUsersTable();
      });

      afterAll(async () => {
        await resetTable(TABLE_USERS);
      });

      describe('when password is incorrect', () => {
        it('should return null', async () => {
          password = 'order65';
          const result = await controller.authenticateUser(email, password);
          expect(result).toStrictEqual(null);
        });
      });

      describe('when password is correct', () => {
        it('should jwt token', async () => {
          password = 'ExecuteOrder66';
          const jwt = (await controller.authenticateUser(email, password))!;
          const payload = container.resolve(Jwt).verifyToken(jwt);
          expect(payload.userId).toBeGreaterThan(0);
        });
      });
    });
  });

  describe('isPasswordCorrect', () => {
    describe('when password is correct', () => {
      let user: PersistedUser;
      beforeAll(async () => {
        user = (await populateUsersTable())!;
      });

      afterAll(async () => {
        await resetTable(TABLE_USERS);
      });

      it('should return true', () => {
        const password = 'ExecuteOrder66';
        const isPasswordCorrect = controller.isPasswordCorrect(password, user);
        expect(isPasswordCorrect).toStrictEqual(true);
      });
    });
  });

  describe('isPasswordValid', () => {
    describe('when provided password is valid', () => {
      it('should return true', () => {
        const password = 'ExecuteOrder66';
        const isPasswordValid = controller.isPasswordValid(password);
        expect(isPasswordValid).toStrictEqual(true);
      });
    });
  });

  describe('updateUser', () => {
    let user: PersistedUser;

    beforeAll(async () => {
      user = (await populateUsersTable())!;
    });

    afterAll(async () => {
      await resetTable(TABLE_USERS);
    });

    it('should update user', async () => {
      const targetUser = { ...user, firstName: 'Anakin', lastName: 'Skywalker' };
      const persistedUser = await controller.updateUser(targetUser);
      expect(targetUser.firstName).toStrictEqual(persistedUser.firstName);
      expect(targetUser.lastName).toStrictEqual(persistedUser.lastName);
      expect(user.updatedAt).not.toStrictEqual(persistedUser.updatedAt);
    });
  });

  describe('updatePassword', () => {
    let user: PersistedUser;

    beforeAll(async () => {
      user = (await populateUsersTable())!;
    });

    afterAll(async () => {
      await resetTable(TABLE_USERS);
    });

    describe('when provided password is invalid', () => {
      it('should not update user\'s password', async () => {
        const password = 'asdf';
        await controller.updatePassword(password, user);
        const persistedUser = (await controller.getUser(user.id))!;
        expect(persistedUser.password).toStrictEqual(user.password);
      });
    });

    describe('when provided password is valid', () => {
      it('should update user\'s password', async () => {
        const password = 'f01Mea.xlz';
        await controller.updatePassword(password, user);
        const persistedUser = (await controller.getUser(user.id))!;
        expect(persistedUser.password).not.toStrictEqual(user.password);
      });
    });
  });

  describe('createUser', () => {
    afterAll(async () => {
      await resetTable(TABLE_USERS);
    });

    describe('when invalid user is provided', () => {
      it('should not create user', async () => {
        const user = { firstName: '', lastName: 'Kenobi', email: 'obi@kenobi.com', password: 'K3nOb1__', salt: 'salt' };
        const nullptr = await controller.createUser(user);
        expect(nullptr).toStrictEqual(null);
      });
    });

    describe('when valid user is provided', () => {
      it('should create an user', async () => {
        const user = { firstName: 'General', lastName: 'Kenobi', email: 'obi@kenobi.com', password: 'K3nOb1__', salt: 'salt' };
        const dbUser = (await controller.createUser(user))!;
        expect(dbUser.firstName).toStrictEqual(user.firstName);
        expect(dbUser.lastName).toStrictEqual(user.lastName);
        expect(dbUser.email).toStrictEqual(user.email);
      });
    });
  });

  describe('getUser', () => {
    beforeAll(async () => {
      await populateUsersTable();
    });

    afterAll(async () => {
      await resetTable(TABLE_USERS);
    });

    it('should get user by id', async () => {
      const id = 1;
      const user = await controller.getUser(id);
      expect(user).toHaveProperty('id');
      expect(user).toHaveProperty('firstName');
      expect(user).toHaveProperty('lastName');
      expect(user).toHaveProperty('email');
      expect(user).toHaveProperty('password');
    });
  });

  describe('getUsers', () => {
    beforeAll(async () => {
      await populateUsersTable();
    });

    afterAll(async () => {
      await resetTable(TABLE_USERS);
    });

    describe('when provided ratio is 0', () => {
      it('should return empty array', async () => {
        const ratio = 0.0;
        const users = await controller.getUsers(ratio);
        expect(users).toStrictEqual([]);
      });
    });

    describe('when provided ratio is greater than 0', () => {
      it('should return users array', async () => {
        const ratio = 1;
        const users = await controller.getUsers(ratio);
        expect(users.length).toStrictEqual(1);
      });
    });
  });

  describe('deleteUser', () => {
    beforeAll(async () => {
      await populateUsersTable();
    });

    afterAll(async () => {
      await resetTable(TABLE_USERS);
    });

    it('should delete user by id', async () => {
      const userId = 1;
      await controller.deleteUser(userId);
      const deletedUser = await controller.getUser(userId);
      expect(deletedUser).toStrictEqual(null);
    });
  });
});
