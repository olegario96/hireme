import { injectable, singleton } from 'tsyringe';

import { Cipher } from '../security/cipher';
import { Jwt } from '../security/jwt';
import { JsonWebToken } from '../security/types';
import { PersistedUser, User, UserId } from './entity';
import { UserDao } from './user-dao';
import { UserDomainLogic } from './user-domain-logic';

@injectable()
@singleton()
export class UserController {
  constructor(
    private cipher: Cipher,
    private readonly jwt: Jwt,
    private readonly userDao: UserDao,
    private readonly userDomainLogic: UserDomainLogic,
  ) {}

  async authenticateUser(email: string, password: string): Promise<JsonWebToken | null> {
    const persistedUser = await this.userDao.getUser({ email });
    if (!persistedUser) {
      return null;
    }

    const isPasswordCorrect = this.cipher.isPasswordCorrect(password, persistedUser);
    if (!isPasswordCorrect) {
      return null;
    }

    return this.jwt.signToken({ userId: persistedUser.id });
  }

  isPasswordCorrect(password: string, user: PersistedUser): boolean {
    return this.cipher.isPasswordCorrect(password, user);
  }

  isPasswordValid(password: string): boolean {
    return this.userDomainLogic.isPasswordValid(password);
  }

  createUser(user: User): Promise<PersistedUser | null> {
    const targetUser = { ...user };
    const invalidAttributes = this.userDomainLogic.validateAttributes(targetUser);
    if (invalidAttributes.length) {
      return Promise.resolve(null);
    }

    const salt = this.cipher.createSalt();
    targetUser.password = this.getHashedPassword(targetUser.password, salt);
    targetUser.salt = this.cipher.encrypt(salt);
    return this.userDao.createUser(targetUser);
  }

  getUser(userId: UserId): Promise<PersistedUser | null> {
    const user = { id: userId };
    return this.userDao.getUser(user);
  }

  async getUsers(ratio: number): Promise<PersistedUser[]> {
    if (ratio === 0) {
      return [];
    }

    return this.userDao.getUsers(ratio);
  }

  updateUser(updatedUser: PersistedUser): Promise<PersistedUser> {
    return this.userDao.updateUser(updatedUser);
  }

  async updatePassword(password: string, user: PersistedUser): Promise<void> {
    const isPasswordValid = this.userDomainLogic.isPasswordValid(password);
    if (!isPasswordValid) {
      return;
    }

    const decryptedPassword = this.cipher.decrypt(user.salt);
    const hashedPassword = this.getHashedPassword(password, decryptedPassword);
    await this.userDao.updatePassword(hashedPassword, user.id);
  }

  deleteUser(userId: UserId): Promise<void> {
    return this.userDao.deleteUser(userId);
  }

  private getHashedPassword(password: string, salt: string): string {
    const saltedPassword = this.cipher.saltPassword(password, salt);
    return this.cipher.hash(saltedPassword);
  }
}
