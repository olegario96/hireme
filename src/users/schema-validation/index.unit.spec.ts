import {
  authenticateSchema,
  createUserSchema,
  resetUserPasswordSchema,
  updateUserSchema,
} from '.';

describe('SchemaValidation', () => {
  describe('authenticateSchema', () => {
    it('should validate schema', () => {
      const body = { email: 'gustavo@olegario.com', password: 'mypassword' };
      const validatedCredentials = authenticateSchema.validate(body);
      expect(validatedCredentials.error).toStrictEqual(undefined);
    });
  });

  describe('createUserSchema', () => {
    it('should validate schema', () => {
      const body = { email: 'darth@sidious.com', password: 'ExecuteOrder66', firstName: 'Darth', lastName: 'Sidious' };
      const validatedUser = createUserSchema.validate(body);
      expect(validatedUser.error).toStrictEqual(undefined);
    });
  });

  describe('updateUserSchema', () => {
    it('should validate schema', () => {
      const body = { firstName: 'Anakin' };
      const validatedUser = updateUserSchema.validate(body);
      expect(validatedUser.error).toStrictEqual(undefined);
    });
  });

  describe('resetUserPasswordSchema', () => {
    it('should validate schema', () => {
      const body = { currentPassword: 'ExecuteOrder66', targetPassword: 'Did you ever hear the tragedy' };
      const validatedUser = resetUserPasswordSchema.validate(body);
      expect(validatedUser.error).toStrictEqual(undefined);
    });
  });
});
