import * as Joi from 'joi';

export const authenticateSchema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
});

export const createUserSchema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.string().required(),
});

export const updateUserSchema = Joi.object({
  firstName: Joi.string(),
  lastName: Joi.string(),
});

export const resetUserPasswordSchema = Joi.object({
  currentPassword: Joi.string().required(),
  targetPassword: Joi.string().required(),
});
