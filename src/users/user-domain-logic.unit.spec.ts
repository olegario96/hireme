import 'reflect-metadata';

import { container } from 'tsyringe';

import { User } from './types';
import { UserDomainLogic } from './user-domain-logic';

describe('UserDomainLogic', () => {
  let instance: UserDomainLogic;
  beforeAll(() => {
    instance = container.resolve(UserDomainLogic);
  });

  describe('validateAttributes', () => {
    let validUser: User;

    beforeAll(() => {
      validUser = {
        firstName: 'Darth',
        lastName: 'Sidious',
        email: 'darth@sidious.com',
        password: 'order66MotherFucker',
        salt: 'salt',
      };
    });

    describe('when one field is invalid', () => {
      describe('when first name is invalid', () => {
        it('should include first name in result', () => {
          const invalidUser = { ...validUser, firstName: '' };
          const invalidAttributes = instance.validateAttributes(invalidUser);
          expect(invalidAttributes).toContain('firstName');
          expect(invalidAttributes).toHaveLength(1);
        });
      });

      describe('when last name is invalid', () => {
        it('should include last name in result', () => {
          const invalidUser = { ...validUser, lastName: '' };
          const invalidAttributes = instance.validateAttributes(invalidUser);
          expect(invalidAttributes).toContain('lastName');
          expect(invalidAttributes).toHaveLength(1);
        });
      });

      describe('when email is invalid', () => {
        it('should include email in result', () => {
          const invalidUser = { ...validUser, email: 'darth sidious.com' };
          const invalidAttributes = instance.validateAttributes(invalidUser);
          expect(invalidAttributes).toContain('email');
          expect(invalidAttributes).toHaveLength(1);
        });
      });

      describe('when password is invalid', () => {
        it.each([['invalid'], ['invalid password']])('should include password in result', (password) => {
          const invalidUser = { ...validUser, password };
          const invalidAttributes = instance.validateAttributes(invalidUser);
          expect(invalidAttributes).toContain('password');
          expect(invalidAttributes).toHaveLength(1);
        });
      });
    });

    describe('when multiple fields are invalid', () => {
      it('should include all invalid fields', () => {
        const invalidUser = { ...validUser, email: 'order66', firstName: '' };
        const invalidAttributes = instance.validateAttributes(invalidUser);
        expect(invalidAttributes).toContain('email');
        expect(invalidAttributes).toContain('firstName');
      });
    });
  });
});
