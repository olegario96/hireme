import { injectable, singleton } from 'tsyringe';

import { DomainLogic } from '../common/domain-logic';
import { User } from './entity';

@injectable()
@singleton()
export class UserDomainLogic extends DomainLogic<User> {
  private readonly EMAIL_RFC22_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  private readonly NUMBER_REGEX = /\d/;
  private readonly LOWER_CASE_REGEX = /[a-z]/;
  private readonly UPPER_CASE_REGEX = /[A-Z]/;

  private readonly PASSWORD_MINIMUM_LENGTH = 8;

  validateAttributes(domain: User): string[] {
    const invalidAttributes = [];
    if (!domain.firstName.length) {
      invalidAttributes.push('firstName');
    }

    if (!domain.lastName.length) {
      invalidAttributes.push('lastName');
    }

    if (!this.isEmailValid(domain.email)) {
      invalidAttributes.push('email');
    }

    if (!this.isPasswordValid(domain.password)) {
      invalidAttributes.push('password');
    }

    return invalidAttributes;
  }

  public isPasswordValid(password: string): boolean {
    if (!password.length || password.length < this.PASSWORD_MINIMUM_LENGTH) {
      return false;
    }

    if (!this.NUMBER_REGEX.test(password)) {
      return false;
    }

    return this.LOWER_CASE_REGEX.test(password) && this.UPPER_CASE_REGEX.test(password);
  }

  private isEmailValid(email: string): boolean {
    return this.EMAIL_RFC22_REGEX.test(email);
  }
}
