import { injectable, singleton } from 'tsyringe';
import { IsNull } from 'typeorm';

import { connection } from '../database/connection';
import { PersistedSkill } from '../skills/entity';
import { PersistedUser, User, UserId } from './entity';

@injectable()
@singleton()
export class UserDao {
  private readonly repository;

  constructor() {
    this.repository = connection.getRepository(PersistedUser);
  }

  async createUser(user: User): Promise<PersistedUser> {
    const targetUser = PersistedUser.from(user);
    return this.repository.save(targetUser);
  }

  async getUser(attributes: { id?: number; email?: string }): Promise<PersistedUser | null> {
    if (!attributes.id && !attributes.email) {
      throw new Error('No attribute provided!');
    }

    const attributeName = Object.keys(attributes).pop()!;
    const attributeValue = Object.values(attributes).pop()!;
    return this.repository.findOne({
      where: {
        [attributeName]: attributeValue,
      },
    });
  }

  async getUsers(ratio: number): Promise<PersistedUser[]> {
    if (ratio === 0) {
      return [];
    }

    const usersCount = await this.getUsersCount();
    const usersToBeFetched = Math.ceil(ratio * usersCount);
    return this.repository.find({
      take: usersToBeFetched,
    });
  }

  async updateUser(targetUser: PersistedUser): Promise<PersistedUser> {
    const { id, firstName, lastName } = targetUser;
    return this.repository.save({ id, firstName, lastName });
  }

  async updatePassword(hashedPassword: string, userId: UserId): Promise<void> {
    await this.repository.save({ id: userId, password: hashedPassword });
  }

  async deleteUser(userId: UserId): Promise<void> {
    await this.repository.save({ id: userId, deletedAt: new Date().toISOString() });
  }

  async addSkillsToUsers(user: PersistedUser, skills: PersistedSkill[]): Promise<PersistedUser> {
    user.skills = skills;
    return this.repository.save(user);
  }

  private async getUsersCount(): Promise<number> {
    return this.repository.count({
      where: {
        deletedAt: IsNull(),
      },
    });
  }
}
