import { Router } from 'express';
import { container } from 'tsyringe';

import { HttpStatusCode } from '../common/types';
import { Logger } from '../logger';
import {
  middlewareContentType,
  middlewareJoi,
  middlewareJwt,
  middlewareLoginAttempt,
} from '../middlewares';
import { CustomRequest } from '../middlewares/types';
import { authenticateSchema, createUserSchema, resetUserPasswordSchema, updateUserSchema } from './schema-validation';
import { UserController } from './user-controller';

const loadRoutes = (router: Router): void => {
  const controller = container.resolve(UserController);
  const logger = container.resolve(Logger);

  router.post(
    '/authenticate',
    middlewareLoginAttempt(),
    middlewareContentType(),
    middlewareJoi(authenticateSchema),
    async (req, res) => {
      try {
        const { email, password } = req.body;
        const jwt = await controller.authenticateUser(email, password);
        if (!jwt) {
          res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'Invalid credentials provided!' });
          return;
        }

        res.status(HttpStatusCode.OK).json(jwt);
      } catch (e) {
        const exception = e as Error;
        logger.error(`POST /authenticate exception: ${exception.message}`);
        res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not authenticate user' });
      }
    });

  router.post('/user', middlewareContentType(), middlewareJoi(createUserSchema), async (req, res) => {
    try {
      const createdUser = await controller.createUser(req.body);
      if (!createdUser) {
        res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'Invalid user provided' });
        return;
      }

      const { email, password } = req.body;
      const jwt = await controller.authenticateUser(email, password);
      res.status(HttpStatusCode.OK).json(jwt);
    } catch (e) {
      const exception = e as Error;
      logger.error(`POST /user exception: ${exception.message}`);
      res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not create user' });
    }
  });

  router.put(
    '/user/:id',
    middlewareContentType(),
    middlewareJoi(updateUserSchema),
    middlewareJwt(),
    async (req: CustomRequest, res) => {
      try {
        const { userId } = req;
        const { firstName, lastName } = req.body;
        // jwtValidation ensures user exists
        const user = (await controller.getUser(userId!))!;
        const targetFirstName = firstName || user.firstName;
        const targetLastName = lastName || user.lastName;
        const updatedUser = { ...user, firstName: targetFirstName, lastName: targetLastName };
        await controller.updateUser(updatedUser);
        res.status(HttpStatusCode.OK).json(updatedUser);
      } catch (e) {
        const exception = e as Error;
        logger.error(`PUT /user/:id exception: ${exception.message}`);
        res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'It was not possible to update user' });
      }
    });

  router.put(
    '/user/:id/password',
    middlewareContentType(),
    middlewareJoi(resetUserPasswordSchema),
    middlewareJwt(),
    async (req: CustomRequest, res) => {
      try {
        const { userId } = req;
        const { currentPassword, targetPassword } = req.body;
        const isPasswordValid = controller.isPasswordValid(targetPassword);
        if (!isPasswordValid) {
          res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'New password does not match password\'s criteria' });
          return;
        }

        // jwtValidation ensures user exists
        const user = (await controller.getUser(userId!))!;
        const isPasswordCorrect = controller.isPasswordCorrect(currentPassword, user);
        if (!isPasswordCorrect) {
          res.status(HttpStatusCode.BAD_REQUEST).json({ error: 'Current password does not match' });
          return;
        }

        await controller.updatePassword(targetPassword, user);
        res.status(HttpStatusCode.NO_CONTENT).json({});
      } catch (e) {
        const exception = e as Error;
        logger.error(`PUT /user/password exception: ${exception.message}`);
        res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'It was not possible to reset password' });
      }
    });
};

module.exports = loadRoutes;
