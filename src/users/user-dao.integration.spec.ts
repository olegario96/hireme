import 'reflect-metadata';

import MockDate from 'mockdate';
import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { PersistedSkill } from '../skills/entity';
import { SkillDao } from '../skills/skill-dao';
import { PersistedUser, TABLE_USERS } from './entity';
import { UserDao } from './user-dao';

/* eslint-disable @typescript-eslint/no-floating-promises, @typescript-eslint/no-magic-numbers */
describe('UserDao', () => {
  let dao: UserDao;

  beforeAll(async () => {
    await connection.initialize();
    dao = container.resolve(UserDao);
    // 2021-02-01 01:48:33
    MockDate.set(new Date(1612144113000));
  });

  afterAll(async () => {
    await resetTable(TABLE_USERS);
    await endConnections();
    MockDate.reset();
  });

  describe('createUser', () => {
    it('should create an user', async () => {
      const user = {
        firstName: 'General',
        lastName: 'Grievous',
        email: 'general@grievous.com',
        password: 'order66',
        salt: 'salt',
      };

      const dbUser = await dao.createUser(user);
      expect(dbUser.firstName).toStrictEqual(user.firstName);
      expect(dbUser.lastName).toStrictEqual(user.lastName);
      expect(dbUser.email).toStrictEqual(user.email);
      expect(typeof dbUser.id).toStrictEqual('number');
      expect(dbUser.createdAt).not.toBeNull();
      expect(dbUser.updatedAt).not.toBeNull();
      expect(dbUser.deletedAt).toStrictEqual(null);
    });
  });

  describe('getUser', () => {
    beforeAll(async () => {
      await populateUsersTable();
    });

    describe('when invalid parameters are provided', () => {
      it('should throw an error', () => {
        const invalidParameters = { id: undefined, email: undefined };
        expect(dao.getUser(invalidParameters)).rejects.toThrow();
      });
    });

    describe('when valid parameters are provided', () => {
      describe('when id is provided', () => {
        it('should return user by id', async () => {
          const parameters = { id: 1 };
          const user = await dao.getUser(parameters);
          expect(user!.id).toStrictEqual(1);
        });
      });

      describe('when email is provided', () => {
        it('should return user by email', async () => {
          const parameters = { email: 'darth@sidious.com' };
          const user = await dao.getUser(parameters);
          expect(user!.email).toStrictEqual('darth@sidious.com');
        });
      });

      describe('when unique identifiers do not exist', () => {
        it('should return null', async () => {
          const parameters = { email: 'darth@maul.com' };
          const user = await dao.getUser(parameters);
          expect(user).toStrictEqual(null);
        });
      });
    });
  });

  describe('getUsers', () => {
    describe('when ratio is 0', () => {
      it('should return an empty array', async () => {
        const ratio = 0;
        const users = await dao.getUsers(ratio);
        expect(users).toStrictEqual([]);
      });
    });

    describe('when ratio is greater than 0', () => {
      beforeAll(async () => {
        await Promise.all([
          populateUsersTable({ email: 'darth@vader.com' } as PersistedUser),
          populateUsersTable({ email: 'commandre@rex.com' } as PersistedUser),
          populateUsersTable({ email: 'mace@windu.com' } as PersistedUser),
          populateUsersTable({ email: 'master@yoda.com' } as PersistedUser),
        ]);
      });

      it('should fetch users based on the provided ratio', async () => {
        const users = await dao.getUsers(0.4);
        expect(users.length).toStrictEqual(3);
      });
    });
  });

  describe('updateUser', () => {
    let user: PersistedUser;
    beforeAll(async () => {
      user = (await dao.getUser({ email: 'darth@sidious.com' }))!;
    });

    it('should update user with new values', async () => {
      const targetUser = { ...user, firstName: 'Anakin', lastName: 'Skywalker' };
      const persistedUser = await dao.updateUser(targetUser);
      expect(targetUser.firstName).toStrictEqual(persistedUser.firstName);
      expect(targetUser.lastName).toStrictEqual(persistedUser.lastName);
      expect(user.updatedAt).not.toStrictEqual(persistedUser.updatedAt);
    });
  });

  describe('updatePassword', () => {
    let user: PersistedUser;
    beforeAll(async () => {
      user = (await dao.getUser({ email: 'darth@sidious.com' }))!;
    });

    it('should update user\'s password', async () => {
      await dao.updatePassword('foo123', user.id);
      const persistedUser = (await dao.getUser({ email: 'darth@sidious.com' }))!;
      expect(persistedUser.password).toStrictEqual('foo123');
    });
  });

  describe('addSkillsToUsers', () => {
    let skills: PersistedSkill[];
    let user: PersistedUser;

    beforeAll(async () => {
      const skillIds = [1, 2, 3];
      skills = await container.resolve(SkillDao).getSkills(skillIds)!;
      user = (await dao.getUser({ email: 'darth@sidious.com' }))!;
    });

    it('should add skills to users', async () => {
      const persistedUser = await dao.addSkillsToUsers(user, skills);
      expect(persistedUser.skills!).toHaveLength(3);
    });
  });

  describe('deleteUser', () => {
    it('should delete user', async () => {
      const parameters = { email: 'darth@sidious.com' };
      const user = (await dao.getUser(parameters))!;
      await dao.deleteUser(user.id);
      const updatedUser = await dao.getUser(parameters);
      expect(updatedUser).toStrictEqual(null);
    });
  });
});
