import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

import { MetaFields } from '../common/entity';
import { PersistedSkill } from '../skills/entity';

export type UserId = number;
export const TABLE_USERS = 'users';
export const TABLE_USERS_SKILLS = 'users_skills';

@Entity(TABLE_USERS)
export class PersistedUser extends MetaFields {
  @PrimaryGeneratedColumn()
  id: UserId;

  @Column({ name: 'first_name' })
  firstName: string;

  @Column({ name: 'last_name' })
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  @ManyToMany(() => PersistedSkill)
  @JoinTable({
    name: TABLE_USERS_SKILLS,
    joinColumn: {
      name: 'user_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'skill_id',
      referencedColumnName: 'id',
    },
  })
  skills?: PersistedSkill[];
}

export type User = Omit<PersistedUser, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt'>;
