/* eslint-disable jest/expect-expect */
import 'reflect-metadata';

import { Application } from 'express';
import request from 'supertest';
import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { buildAppForTests, endConnections } from '../../tests/utils';
import { APPLICATION_JSON, AUTHORIZATION, CONTENT_TYPE } from '../common/constants';
import { HttpStatusCode } from '../common/types';
import { Jwt } from '../security/jwt';
import { PersistedUser, TABLE_USERS, UserId } from './entity';
import { UserController } from './user-controller';

describe('User routes', () => {
  let app: Application;

  beforeAll(async () => {
    const context = __dirname;
    app = await buildAppForTests(context);
    await populateUsersTable();
  });

  afterAll(async () => {
    await resetTable(TABLE_USERS);
    await endConnections();
  });

  describe('POST /authenticate', () => {
    describe('when body is malformed', () => {
      it('should return bad request', async () => {
        await request(app).post('/authenticate')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send({ password: '' })
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when credentials are invalid', () => {
      it('should return bad request', async () => {
        await request(app).post('/authenticate')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send({ email: 'test@test.com', password: '1234' })
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when valid credentials are provided', () => {
      describe('when application can connect to database', () => {
        it('should return ok', async () => {
          await request(app).post('/authenticate')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send({ email: 'darth@sidious.com', password: 'ExecuteOrder66' })
            .expect(HttpStatusCode.OK);
        });
      });

      describe('when application is not able to connect to database', () => {
        beforeAll(() => {
          const controller = container.resolve(UserController);
          jest.spyOn(controller, 'authenticateUser')
            .mockRejectedValueOnce(new Error('Mock'));
        });

        afterAll(() => {
          jest.clearAllMocks();
        });

        it('should return internal server error', async () => {
          await request(app).post('/authenticate')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send({ email: 'darth@sidious.com', password: 'ExecuteOrder66' })
            .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
        });
      });
    });

    describe('when too many login attempts are done in a short period of time', () => {
      it('should return too many requests', async () => {
        await request(app).post('/authenticate')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send({ email: 'darth@sidious.com', password: 'ExecuteOrder66' })
          .expect(HttpStatusCode.OK);

        await request(app).post('/authenticate')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send({ email: 'darth@sidious.com', password: 'ExecuteOrder66' })
          .expect(HttpStatusCode.TOO_MANY_REQUESTS);
      });
    });
  });

  describe('POST /user', () => {
    describe('when body is malformed', () => {
      it('should return bad request', async () => {
        const body = { firstName: '' };
        await request(app).post('/user')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send(body)
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when invalid user is provided', () => {
      it('should return bad request', async () => {
        const body = { firstName: 'General', lastName: 'Kenobi', email: 'general', password: 'CaptainR3x' };
        await request(app).post('/user')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .send(body)
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when valid user is provided', () => {
      describe('when app can connect to database', () => {
        it('should return ok', async () => {
          const body = { firstName: 'General', lastName: 'Kenobi', email: 'general@kenobi.com', password: 'CaptainR3x' };
          await request(app).post('/user')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send(body)
            .expect(HttpStatusCode.OK);
        });
      });

      describe('when app can not connect to database', () => {
        let controller: UserController;

        beforeAll(() => {
          controller = container.resolve(UserController);
          jest.spyOn(controller, 'createUser')
            .mockRejectedValueOnce(() => {
              throw new Error('Mocked');
            });
        });

        it('should return internal server error', async () => {
          const body = { firstName: 'General', lastName: 'Kenobi', email: 'general@kenobi.com', password: 'CaptainR3x' };
          await request(app).post('/user')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send(body)
            .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
        });
      });
    });
  });

  describe('PUT /user/:id', () => {
    describe('when invalid content type is provided', () => {
      it('should return bad request', async () => {
        await request(app).put('/user/1')
          .set(CONTENT_TYPE, 'application/javascript')
          .send()
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when valid content type is provided', () => {
      describe('when body request is invalid', () => {
        it('should return bad request', async () => {
          const body = { foo: 'bar' };
          await request(app).put('/user/1')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send(body)
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });
    });

    describe('when invalid JWT is provided', () => {
      it('should return unauthorized', async () => {
        const body = { firstName: 'Anakin' };
        await request(app).put('/user/1')
          .set(CONTENT_TYPE, APPLICATION_JSON)
          .set(AUTHORIZATION, 'Bearer token')
          .send(body)
          .expect(HttpStatusCode.UNAUTHORIZED);
      });
    });

    describe('when valid JWT is provided', () => {
      let jwt: Jwt;
      let userId: UserId;

      beforeAll(async () => {
        jwt = container.resolve(Jwt);
        const override = { email: 'anakin@skywalker.com' } as PersistedUser;
        const { id } = (await populateUsersTable(override))!;
        userId = id;
      });

      afterAll(async () => {
        await resetTable(TABLE_USERS);
      });

      describe('when there is no exception', () => {
        it('should return ok status', async () => {
          const body = { firstName: 'Anakin' };
          const token = jwt.signToken({ userId });
          await request(app).put('/user/1')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .set(AUTHORIZATION, `Bearer ${token}`)
            .send(body)
            .expect(HttpStatusCode.OK);
        });
      });

      describe('when there is an exception', () => {
        let controller: UserController;
        beforeAll(() => {
          controller = container.resolve(UserController);
          jest.spyOn(controller, 'updateUser')
            .mockRejectedValueOnce(new Error('Mocked'));
        });

        it('should return internal server error', async () => {
          const body = { lastName: 'Skywalker' };
          const token = jwt.signToken({ userId });
          await request(app).put('/user/1')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .set(AUTHORIZATION, `Bearer ${token}`)
            .send(body)
            .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
        });
      });
    });
  });

  describe('PUT /user/:id/password', () => {
    describe('when invalid content type is provided', () => {
      it('should return bad request', async () => {
        await request(app).put('/user/1/password')
          .set(CONTENT_TYPE, 'application/javascript')
          .send()
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when valid content type is provided', () => {
      describe('when request body is invalid', () => {
        it('should return bad request', async () => {
          const body = { currentPassword: '1234' };
          await request(app).put('/user/1/password')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send(body)
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });

      describe('when request body is valid', () => {
        describe('when invalid JWT is provided', () => {
          it('should return unauthorized', async () => {
            const body = { currentPassword: '1234', targetPassword: '4321' };
            await request(app).put('/user/1/password')
              .set(CONTENT_TYPE, APPLICATION_JSON)
              .set(AUTHORIZATION, 'Bearer token')
              .send(body)
              .expect(HttpStatusCode.UNAUTHORIZED);
          });
        });

        describe('when valid JWT is provided', () => {
          let jwt: Jwt;
          let userId: UserId;

          beforeAll(async () => {
            jwt = container.resolve(Jwt);
            const override = { email: 'general@grievous.com' } as PersistedUser;
            const { id } = (await populateUsersTable(override))!;
            userId = id;
          });

          afterAll(async () => {
            await resetTable(TABLE_USERS);
          });

          describe('when target password is not valid', () => {
            it('should return bad request', async () => {
              const body = { currentPassword: '1234', targetPassword: '4321' };
              const token = jwt.signToken({ userId });
              await request(app).put('/user/1/password')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .set(AUTHORIZATION, `Bearer ${token}`)
                .send(body)
                .expect(HttpStatusCode.BAD_REQUEST);
            });
          });

          describe('when target password is valid', () => {
            describe('when current password is incorrect', () => {
              it('should return bad request', async () => {
                const body = { currentPassword: 'asdf', targetPassword: 'ExecuteOrder66' };
                const token = jwt.signToken({ userId });
                await request(app).put('/user/1/password')
                  .set(CONTENT_TYPE, APPLICATION_JSON)
                  .set(AUTHORIZATION, `Bearer ${token}`)
                  .send(body)
                  .expect(HttpStatusCode.BAD_REQUEST);
              });
            });

            describe('when current password is correct', () => {
              it('should update password', async () => {
                const body = { currentPassword: 'ExecuteOrder66', targetPassword: 'ExecuteOrder65' };
                const token = jwt.signToken({ userId });
                await request(app).put('/user/1/password')
                  .set(CONTENT_TYPE, APPLICATION_JSON)
                  .set(AUTHORIZATION, `Bearer ${token}`)
                  .send(body)
                  .expect(HttpStatusCode.NO_CONTENT);
              });
            });
          });

          describe('when it can not use database', () => {
            let controller: UserController;
            beforeAll(() => {
              controller = container.resolve(UserController);
              jest.spyOn(controller, 'isPasswordValid')
                .mockImplementationOnce(() => {
                  throw new Error('Mocked');
                });
            });

            it('should return internal server error', async () => {
              const body = { currentPassword: 'ExecuteOrder66', targetPassword: 'ExecuteOrder65' };
              const token = jwt.signToken({ userId });
              await request(app).put('/user/1/password')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .set(AUTHORIZATION, `Bearer ${token}`)
                .send(body)
                .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
            });
          });
        });
      });
    });
  });
});
