import { injectable, singleton } from 'tsyringe';
import { IsNull } from 'typeorm';

import { connection } from '../database/connection';
import { Company, CompanyId, PersistedCompany } from './entity';

@injectable()
@singleton()
export class CompanyDao {
  private readonly repository;

  constructor() {
    this.repository = connection.getRepository(PersistedCompany);
  }

  async createCompany(company: Company): Promise<PersistedCompany> {
    const persistedCompany = PersistedCompany.from(company);
    return this.repository.save(persistedCompany);
  }

  async getCompany(companyId: CompanyId): Promise<PersistedCompany | null> {
    return this.repository.findOne({
      where: {
        id: companyId,
        deletedAt: IsNull(),
      },
    });
  }
}
