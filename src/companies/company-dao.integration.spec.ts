import 'reflect-metadata';

import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { CompanyDao } from './company-dao';
import { TABLE_COMPANIES } from './entity';

describe('CompanyDao', () => {
  let dao: CompanyDao;

  beforeAll(async () => {
    await connection.initialize();
    dao = container.resolve(CompanyDao);
  });

  afterAll(async () => {
    await resetTable(TABLE_COMPANIES);
    await endConnections();
  });

  describe('createCompany', () => {
    it('should create company', async () => {
      const company = { name: 'hireme', description: null, logoUrl: 'http://pudim.com.br/pudim.jpg' };
      const persistedCompany = await dao.createCompany(company);
      expect(persistedCompany).toHaveProperty('id');
      expect(persistedCompany).toMatchObject(company);
    });
  });

  describe('getCompany', () => {
    describe('when invalid id is provided', () => {
      it('should return nullptr', async () => {
        const companyId = 3;
        const persistedCompany = await dao.getCompany(companyId);
        expect(persistedCompany).toStrictEqual(null);
      });
    });

    describe('when valid id is provided', () => {
      it('should return persisted company', async () => {
        const companyId = 1;
        const persistedCompany = await dao.getCompany(companyId);
        expect(persistedCompany).not.toStrictEqual(null);
        expect(persistedCompany).toHaveProperty('name');
        expect(persistedCompany).toHaveProperty('description');
        expect(persistedCompany).toHaveProperty('logoUrl');
        expect(persistedCompany!.description).toStrictEqual(null);
      });
    });
  });
});
