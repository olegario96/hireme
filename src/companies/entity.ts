import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { MetaFields } from '../common/entity';

export type CompanyId = number;
export const TABLE_COMPANIES = 'companies';

@Entity(TABLE_COMPANIES)
export class PersistedCompany extends MetaFields {
  @PrimaryGeneratedColumn()
  id: CompanyId;

  @Column()
  name: string;

  @Column({ name: 'logo_url', type: 'varchar', nullable: true })
  logoUrl: string | null;

  @Column({ name: 'description', type: 'text', nullable: true })
  description: string | null;
}

export type Company = Omit<PersistedCompany, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt'>;
