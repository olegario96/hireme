import 'reflect-metadata';

import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { PersistedUser, TABLE_USERS } from '../users/entity';
import { UserDao } from '../users/user-dao';
import { SkillDao } from './skill-dao';

/* eslint-disable @typescript-eslint/no-magic-numbers */
describe('SkillDao', () => {
  let dao: SkillDao;
  let user: PersistedUser;

  beforeAll(async () => {
    await connection.initialize();
    user = (await populateUsersTable())!;
    dao = container.resolve(SkillDao);
  });

  afterAll(async () => {
    await resetTable(TABLE_USERS);
    await endConnections();
  });

  describe('createSkill', () => {
    it('should return persisted skill', async () => {
      const skill = { name: 'typescript' };
      const persistedSkill = await dao.createSkill(skill);
      expect(persistedSkill).toHaveProperty('id');
      expect(persistedSkill).toHaveProperty('name');
      expect(persistedSkill).toHaveProperty('createdAt');
      expect(persistedSkill).toHaveProperty('updatedAt');
      expect(persistedSkill).toHaveProperty('deletedAt');
      expect(persistedSkill.name).toStrictEqual('typescript');
    });
  });

  describe('getSkills', () => {
    describe('when empty array is provided', () => {
      it('should return empty array', async () => {
        const skillIds = [];
        const skills = await dao.getSkills(skillIds);
        expect(skills).toStrictEqual([]);
      });
    });

    describe('when provided id is valid', () => {
      it('should get skills', async () => {
        const skillIds = [1];
        const [skill] = (await dao.getSkills(skillIds))!;
        expect(skill).toHaveProperty('id');
        expect(skill).toHaveProperty('name');
        expect(skill).toHaveProperty('createdAt');
      });
    });
  });

  describe('getProficiencies', () => {
    let skillIds: number[];
    beforeAll(async () => {
      skillIds = [1, 2];
      const skills = await dao.getSkills(skillIds);
      await container.resolve(UserDao).addSkillsToUsers(user, skills);
    });

    it('should return proficiencies', async () => {
      const proficiencies = await dao.getProficiencies(user.id);
      const [{ skillProficiency }] = proficiencies;
      expect(proficiencies).toHaveLength(2);
      expect(skillProficiency).toStrictEqual(0);
    });
  });
});
