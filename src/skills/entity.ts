import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { MetaFields } from '../common/entity';

export type SkillId = number;
export const TABLE_SKILLS = 'skills';

@Entity(TABLE_SKILLS)
export class PersistedSkill extends MetaFields {
  @PrimaryGeneratedColumn()
  id: SkillId;

  @Column()
  name: string;

  skillProficiency?: number;
}

export type Skill = Omit<PersistedSkill, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt'>;

