import { injectable, singleton } from 'tsyringe';
import { In, IsNull } from 'typeorm';

import { connection } from '../database/connection';
import { TABLE_USERS, TABLE_USERS_SKILLS, UserId } from '../users/entity';
import { PersistedSkill, Skill, SkillId, TABLE_SKILLS } from './entity';

@injectable()
@singleton()
export class SkillDao {
  private readonly repository;

  constructor() {
    this.repository = connection.getRepository(PersistedSkill);
  }

  async createSkill(skill: Skill): Promise<PersistedSkill> {
    const targetSkill = PersistedSkill.from(skill);
    return this.repository.save(targetSkill);
  }

  async getSkills(ids: SkillId[]): Promise<PersistedSkill[]> {
    if (!ids.length) {
      return [];
    }

    return this.repository.find({
      where: {
        id: In(ids),
        deletedAt: IsNull(),
      },
    });
  }

  async getProficiencies(userId: UserId): Promise<PersistedSkill[]> {
    return this.repository.createQueryBuilder(TABLE_SKILLS)
      .select('skills.id, skills.name, us.skill_proficiency as "skillProficiency", us.created_at, us.updated_at, us.deleted_at')
      .innerJoin(
        TABLE_USERS_SKILLS,
        'us',
        'skills.id = us.skill_id'
      )
      .innerJoin(
        TABLE_USERS,
        'u',
        'u.id = us.user_id'
      )
      .where('u.id = :userId', { userId })
      .getRawMany();
  }
}
