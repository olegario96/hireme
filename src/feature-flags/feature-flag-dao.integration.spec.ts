/* eslint-disable @typescript-eslint/no-floating-promises, @typescript-eslint/no-magic-numbers */
import 'reflect-metadata';

import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { PersistedUser, TABLE_USERS, UserId } from '../users/entity';
import { FeatureFlagDao } from './feature-flag-dao';
import { FeatureFlag, FeatureFlagId, TABLE_FEATURE_FLAGS, TABLE_FEATURE_FLAGS_USERS } from './types';

describe('FeatureFlagDao', () => {
  let dao: FeatureFlagDao;
  beforeAll(async () => {
    await connection.initialize();
    dao = container.resolve(FeatureFlagDao);
    const persistedUser = { email: 'general@grievous.com' } as PersistedUser;
    await Promise.all([
      populateUsersTable(),
      populateUsersTable(persistedUser),
    ]);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_USERS),
      resetTable(TABLE_FEATURE_FLAGS),
      resetTable(TABLE_FEATURE_FLAGS_USERS),
    ]);

    await endConnections();
  });

  describe('createFeatureFlag', () => {
    let featureFlag: FeatureFlag;
    beforeAll(() => {
      featureFlag = { name: 'create.companies', description: 'Permission to create companies' };
    });

    it('should create feature flag', async () => {
      const persistedFeatureFlag = await dao.createFeatureFlag(featureFlag);
      expect(persistedFeatureFlag.name).toStrictEqual(featureFlag.name);
      expect(persistedFeatureFlag.description).toStrictEqual(featureFlag.description);
      expect(persistedFeatureFlag).toHaveProperty('id');
      expect(persistedFeatureFlag).toHaveProperty('createdAt');
      expect(persistedFeatureFlag).toHaveProperty('updatedAt');
      expect(persistedFeatureFlag.deletedAt).toStrictEqual(null);
    });
  });

  describe('getFeatureFlagsByNames', () => {
    describe('when empty array is provided', () => {
      it('should return an empty array', async () => {
        const featureFlagNames = [];
        const persistedFeatureFlags = await dao.getFeatureFlagsByNames(featureFlagNames);
        expect(persistedFeatureFlags).toStrictEqual([]);
      });
    });

    describe('when non empty array is provided', () => {
      it('should return the appropriate FFs', async () => {
        const featureFlagNames = ['create.companies'];
        const [persistedFeatureFlag] = await dao.getFeatureFlagsByNames(featureFlagNames);
        expect(persistedFeatureFlag.name).toStrictEqual('create.companies');
        expect(persistedFeatureFlag.description).toStrictEqual('Permission to create companies');
        expect(persistedFeatureFlag.deletedAt).toStrictEqual(null);
      });
    });
  });

  describe('getFeatureFlagByIndexedAttributes', () => {
    describe('when invalid parameters are provided', () => {
      it('should throw an error', () => {
        const attributes = {};
        expect(dao.getFeatureFlagByIndexedAttributes(attributes)).rejects.toThrow('No attribute provided!');
      });
    });

    describe('when valid attribute is provided', () => {
      describe('when it can not find record', () => {
        it('should return nullptr', async () => {
          const attributes = { id: -1 };
          const persistedFeatureFlag = await dao.getFeatureFlagByIndexedAttributes(attributes);
          expect(persistedFeatureFlag).toStrictEqual(null);
        });
      });

      describe('when it can find record', () => {
        it('should return persisted feature flag', async () => {
          const attributes = { name: 'create.companies' };
          const persistedFeatureFlag = (await dao.getFeatureFlagByIndexedAttributes(attributes))!;
          expect(persistedFeatureFlag.id).toStrictEqual(1);
          expect(persistedFeatureFlag.name).toStrictEqual(attributes.name);
          expect(persistedFeatureFlag.description).toStrictEqual('Permission to create companies');
        });
      });
    });
  });

  describe('areFeatureFlagsTurnedOnForUser', () => {
    describe('when it can not find featureFlagId', () => {
      it('should throw an error', () => {
        const featureFlagId = -1;
        const userId = 1;
        expect(dao.areFeatureFlagsTurnedOnForUser([featureFlagId], userId)).rejects.toThrow('Provided FF or user does not exist');
      });
    });

    describe('when it can find featureFlagId', () => {
      it('should return FF status for user', async () => {
        const featureFlagId = 1;
        const userId = 1;
        const isFeatureFlagTurnedOnForUser = await dao.areFeatureFlagsTurnedOnForUser([featureFlagId], userId);
        expect(isFeatureFlagTurnedOnForUser).toStrictEqual(false);
      });
    });
  });

  describe('changeFeatureFlagStatus', () => {
    it('should update FF status to target user', async () => {
      const targetStatus = true;
      const featureFlagId = 1;
      const userId = 1;
      await dao.changeFeatureFlagStatus(targetStatus, featureFlagId, userId);
      const areFeatureFlagsTurnedOnForUser = await dao.areFeatureFlagsTurnedOnForUser([featureFlagId], userId);
      expect(areFeatureFlagsTurnedOnForUser).toStrictEqual(true);
    });
  });

  describe('rolloutFeatureFlagToUsers', () => {
    let featureFlagId: FeatureFlagId;
    let userId: UserId;
    beforeAll(() => {
      featureFlagId = 1;
      userId = 2;
    });

    describe('when provided array is empty', () => {
      it('should not update status for FF', async () => {
        const areFeatureFlagsTurnedOnForUser = await dao.areFeatureFlagsTurnedOnForUser([featureFlagId], userId);
        await dao.rolloutFeatureFlagToUsers(featureFlagId, []);
        const updatedFeatureFlagStatus = await dao.areFeatureFlagsTurnedOnForUser([featureFlagId], userId);
        expect(areFeatureFlagsTurnedOnForUser).toStrictEqual(false);
        expect(updatedFeatureFlagStatus).toStrictEqual(false);
      });
    });

    describe('when provided array is not empty', () => {
      it('should update status for FF', async () => {
        const areFeatureFlagsTurnedOnForUser = await dao.areFeatureFlagsTurnedOnForUser([featureFlagId], userId);
        await dao.rolloutFeatureFlagToUsers(featureFlagId, [1, 2]);
        const updatedFeatureFlagStatus = await dao.areFeatureFlagsTurnedOnForUser([featureFlagId], userId);
        expect(areFeatureFlagsTurnedOnForUser).toStrictEqual(false);
        expect(updatedFeatureFlagStatus).toStrictEqual(true);
      });
    });
  });
});
