/* eslint-disable @typescript-eslint/no-magic-numbers */
import 'reflect-metadata';

import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { PersistedUser, TABLE_USERS } from '../users/entity';
import { FeatureFlagController } from './feature-flag-controller';
import { TABLE_FEATURE_FLAGS, TABLE_FEATURE_FLAGS_USERS } from './types';

describe('FeatureFlagController', () => {
  let controller: FeatureFlagController;
  beforeAll(async () => {
    await connection.initialize();
    controller = container.resolve(FeatureFlagController);
    const persistedUser = { email: 'general@grievous.com' } as PersistedUser;
    await Promise.all([
      populateUsersTable(),
      populateUsersTable(persistedUser),
    ]);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_USERS),
      resetTable(TABLE_FEATURE_FLAGS),
      resetTable(TABLE_FEATURE_FLAGS_USERS),
    ]);

    await endConnections();
  });

  describe('createFeatureFlag', () => {
    describe('when provided feature flag has an invalid name', () => {
      it('should return a nullptr', async () => {
        const featureFlag = { name: '', description: '' };
        const persistedFeatureFlag = await controller.createFeatureFlag(featureFlag);
        expect(persistedFeatureFlag).toStrictEqual(null);
      });
    });

    describe('when provided feature flag is valid', () => {
      it('should return persisted feature flag', async () => {
        const featureFlag = { name: 'create.companies', description: '' };
        const persistedFeatureFlag = (await controller.createFeatureFlag(featureFlag))!;
        expect(persistedFeatureFlag.name).toStrictEqual(featureFlag.name);
        expect(persistedFeatureFlag.description).toStrictEqual(featureFlag.description);
      });
    });
  });

  describe('getFeatureFlagsByNames', () => {
    it('should read FFs by their names', async () => {
      const featureFlagNames = ['create.companies'];
      const [persistedFeatureFlag] = await controller.getFeatureFlagsByNames(featureFlagNames);
      expect(persistedFeatureFlag.name).toStrictEqual('create.companies');
      expect(persistedFeatureFlag.description).toStrictEqual('');
      expect(persistedFeatureFlag.deletedAt).toStrictEqual(null);
    });
  });

  describe('getFeatureFlagByIndexedAttributes', () => {
    it('should read FF by attribute', async () => {
      const attributes = { name: 'create.companies' };
      const persistedFeatureFlag = (await controller.getFeatureFlagByIndexedAttributes(attributes))!;
      expect(persistedFeatureFlag.name).toStrictEqual(attributes.name);
    });
  });

  describe('areFeatureFlagsTurnedOnForUser', () => {
    it('should check FF status given an user', async () => {
      const featureFlagId = 1;
      const userId = 1;
      const areFeatureFlagsTurnedOnForUser = await controller.areFeatureFlagsTurnedOnForUser([featureFlagId], userId);
      expect(areFeatureFlagsTurnedOnForUser).toStrictEqual(false);
    });
  });

  describe('changeFeatureFlagStatus', () => {
    it('should change FF status given an user', async () => {
      const featureFlagId = 1;
      const userId = 1;
      await controller.changeFeatureFlagStatus(true, featureFlagId, userId);
      const areFeatureFlagsTurnedOnForUser = await controller.areFeatureFlagsTurnedOnForUser([featureFlagId], userId);
      expect(areFeatureFlagsTurnedOnForUser).toStrictEqual(true);
    });
  });

  describe('rolloutFeatureFlagToUsers', () => {
    it('should rollout FF status given userIds', async () => {
      const featureFlagId = 1;
      const userIds = [1, 2];
      await controller.rolloutFeatureFlagToUsers(featureFlagId, userIds);
      const areFeatureFlagsTurnedOnForUser = await controller.areFeatureFlagsTurnedOnForUser([featureFlagId], 2);
      expect(areFeatureFlagsTurnedOnForUser).toStrictEqual(true);
    });
  });
});
