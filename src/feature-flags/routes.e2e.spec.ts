/* eslint-disable jest/expect-expect */
import 'reflect-metadata';

import { Application } from 'express';
import request from 'supertest';
import { container } from 'tsyringe';

import { populateFeatureFlagsTable } from '../../seed/feature-flags';
import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { buildAppForTests, endConnections } from '../../tests/utils';
import { APPLICATION_JSON, CONTENT_TYPE } from '../common/constants';
import { HttpStatusCode } from '../common/types';
import { PersistedUser, TABLE_USERS } from '../users/entity';
import { FeatureFlagController } from './feature-flag-controller';
import { TABLE_FEATURE_FLAGS, TABLE_FEATURE_FLAGS_USERS } from './types';

describe('Feature Flags routes', () => {
  let app: Application;

  beforeAll(async () => {
    const context = __dirname;
    app = await buildAppForTests(context);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_FEATURE_FLAGS_USERS),
      resetTable(TABLE_USERS),
      resetTable(TABLE_FEATURE_FLAGS),
    ]);

    await endConnections();
  });

  describe('POST /rollout-feature-flag', () => {
    describe('when content type is incorrect', () => {
      it('should return bad request', async () => {
        await request(app).post('/rollout-feature-flag')
          .set(CONTENT_TYPE, 'foo')
          .send()
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when content type is correct', () => {
      describe('when body request is malformed', () => {
        it('should return bad request', async () => {
          await request(app).post('/rollout-feature-flag')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send({ name: 'create.companies', rolloutRatio: 0.2 })
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });

      describe('when body request is valid', () => {
        describe('when provided token is invalid', () => {
          it('should return bad request', async () => {
            await request(app).post('/rollout-feature-flag')
              .set(CONTENT_TYPE, APPLICATION_JSON)
              .send({ name: 'create.companies', rolloutRatio: 0.2, token: 'bar' })
              .expect(HttpStatusCode.BAD_REQUEST);
          });
        });

        describe('when provided token is valid', () => {
          let token: string;
          beforeAll(() => {
            token = process.env.INTERNAL_TOKEN_FF_ROLLOUT!;
          });

          describe('when feature flag does not exist', () => {
            it('should return not found', async () => {
              await request(app).post('/rollout-feature-flag')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .send({ name: 'foo', rolloutRatio: 0.2, token })
                .expect(HttpStatusCode.NOT_FOUND);
            });
          });

          describe('when feature flag exists', () => {
            beforeAll(async () => {
              const persistedUser = { email: 'general@grievous.com' } as PersistedUser;
              await Promise.all([
                populateUsersTable(),
                populateUsersTable(persistedUser),
                populateFeatureFlagsTable(),
              ]);
            });

            it('should rollout flag', async () => {
              await request(app).post('/rollout-feature-flag')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .send({ name: 'create.companies', rolloutRatio: 0.2, token: process.env.INTERNAL_TOKEN_FF_ROLLOUT })
                .expect(HttpStatusCode.OK);
            });
          });

          describe('when database is offline', () => {
            let controller: FeatureFlagController;

            beforeAll(() => {
              controller = container.resolve(FeatureFlagController);
              jest.spyOn(controller, 'getFeatureFlagByIndexedAttributes')
                .mockRejectedValueOnce(() => {
                  throw new Error('Mocked');
                });
            });

            it('should return internal server error', async () => {
              await request(app).post('/rollout-feature-flag')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .send({ name: 'create.companies', rolloutRatio: 0.2, token: process.env.INTERNAL_TOKEN_FF_ROLLOUT })
                .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
            });
          });
        });
      });
    });
  });
});
