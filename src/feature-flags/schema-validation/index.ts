import * as Joi from 'joi';

import { token } from '../../common/schema-validation/internal-token';

export const rolloutFeatureFlagSchema = Joi.object({
  name: Joi.string().required(),
  token,
  rolloutRatio: Joi.number()
    .min(0)
    .max(1)
    .required(),
});
