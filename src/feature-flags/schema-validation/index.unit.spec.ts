import { rolloutFeatureFlagSchema } from '.';

describe('SchemaValidation', () => {
  describe('rolloutFeatureFlagSchema', () => {
    it('should validate schema', () => {
      const body = { name: 'create.company', rolloutRatio: 0.101 };
      const validatedPayload = rolloutFeatureFlagSchema.validate(body);
      expect(validatedPayload.error).toStrictEqual(undefined);
    });
  });
});
