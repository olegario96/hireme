import { injectable, singleton } from 'tsyringe';

import { PostgresDao } from '../common/postgres-dao';
import { PersistedUser, TABLE_USERS, UserId } from '../users/entity';
import { FeatureFlag, FeatureFlagId, IndexedFeatureFlagAttributes, PersistedFeatureFlag, TABLE_FEATURE_FLAGS, TABLE_FEATURE_FLAGS_USERS } from './types';

@injectable()
@singleton()
export class FeatureFlagDao extends PostgresDao {
  private readonly OFFSET_FEATURE_FLAG_ID = 1;
  private readonly OFFSET_USER_ID = 2;
  private readonly OFFSET_RECORD = 2;

  async createFeatureFlag(featureFlag: FeatureFlag): Promise<PersistedFeatureFlag> {
    const query = `
      INSERT INTO
        ${TABLE_FEATURE_FLAGS} (
          name,
          description,
          created_at,
          updated_at
        )
      VALUES (
        $1,
        $2,
        $3,
        $4
      )
      RETURNING
        id
    `;

    const { name, description } = featureFlag;
    const { unixEpoch, timestamp } = this.getDateTimestamp();
    const values = [name, description, timestamp, timestamp];
    const [{ id }] = await this.db.executeQuery({ query, values });
    const persistedFeatureFlag = {
      ...featureFlag,
      id,
      createdAt: unixEpoch,
      updatedAt: unixEpoch,
      deletedAt: null,
    };

    await this.linkFeatureFlagToUsers(persistedFeatureFlag.id);
    return persistedFeatureFlag;
  }

  async getFeatureFlagsByNames(featureFlagNames: string[]): Promise<PersistedFeatureFlag[]> {
    if (!featureFlagNames.length) {
      return [];
    }

    const query = `
      SELECT
        *
      FROM
        ${TABLE_FEATURE_FLAGS}
      WHERE
        name = ANY ($1::text[])
        AND deleted_at IS NULL
    `;

    const values = [featureFlagNames];
    return this.db.executeQuery({ query, values });
  }

  async getFeatureFlagByIndexedAttributes(
    attributes: IndexedFeatureFlagAttributes,
  ): Promise<PersistedFeatureFlag | null> {
    if (!attributes.id && !attributes.name) {
      throw new Error('No attribute provided!');
    }

    const indexedAttributeName = Object.keys(attributes).pop()!;
    const indexedAttributeValue = Object.values(attributes).pop()!;
    const query = `
      SELECT
        *
      FROM
        ${TABLE_FEATURE_FLAGS}
      WHERE
        ${indexedAttributeName} = $1
        AND deleted_at IS NULL
    `;

    const values = [indexedAttributeValue];
    const featureFlag = await this.db.executeQuery({ query, values });
    if (!featureFlag.length) {
      return null;
    }

    return featureFlag.pop()!;
  }

  async rolloutFeatureFlagToUsers(featureFlagId: FeatureFlagId, userIds: UserId[]): Promise<void> {
    if (!userIds.length) {
      return;
    }

    const query = `
      UPDATE
        ${TABLE_FEATURE_FLAGS_USERS}
      SET
        is_turned_on = TRUE,
        updated_at = $1
      WHERE
        feature_flag_id = $2
        AND user_id = ANY ($3::int[])
    `;

    const { timestamp } = this.getDateTimestamp();
    const values = [timestamp, featureFlagId, userIds];
    await this.db.executeQuery({ query, values });
  }

  async changeFeatureFlagStatus(
    targetStatus: boolean,
    featureFlagId: FeatureFlagId,
    userId: UserId,
  ): Promise<void> {
    const query = `
      UPDATE
        ${TABLE_FEATURE_FLAGS_USERS}
      SET
        is_turned_on = $1,
        updated_at = $2
      WHERE
        feature_flag_id = $3
        AND user_id = $4
    `;

    const { timestamp } = this.getDateTimestamp();
    const values = [targetStatus, timestamp, featureFlagId, userId];
    await this.db.executeQuery({ query, values });
  }

  async areFeatureFlagsTurnedOnForUser(featureFlagIds: FeatureFlagId[], userId: UserId): Promise<boolean> {
    const query = `
      SELECT
        *
      FROM
        ${TABLE_FEATURE_FLAGS_USERS}
      WHERE
        user_id = $1
        AND feature_flag_id = ANY ($2::int[])
        AND deleted_at IS NULL
    `;

    const values = [userId, featureFlagIds];
    const flagsStatuses = await this.db.executeQuery({ query, values });
    if (!flagsStatuses.length) {
      throw new Error('Provided FF or user does not exist');
    }

    for (const flagStatus of flagsStatuses) {
      if (!flagStatus.isTurnedOn) {
        return false;
      }
    }

    return true;
  }

  private async linkFeatureFlagToUsers(featureFlagId: FeatureFlagId): Promise<void> {
    const userIds = await this.getUserIds();
    /* istanbul ignore next */
    if (!userIds.length) {
      return;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const values: any[] = [];
    const queryPlaceholder: string[] = [];
    let query = `
      INSERT INTO
        ${TABLE_FEATURE_FLAGS_USERS} (
          feature_flag_id,
          user_id
        )
      VALUES
    `;

    userIds.forEach((userId, idx) => {
      queryPlaceholder.push(`
        (
          $${idx * this.OFFSET_RECORD + this.OFFSET_FEATURE_FLAG_ID},
          $${idx * this.OFFSET_RECORD + this.OFFSET_USER_ID}
        )
      `);

      values.push(featureFlagId, userId);
    });

    query += queryPlaceholder.join(',');
    await this.db.executeQuery({ query, values });
  }

  private async getUserIds(): Promise<UserId[]> {
    const query = `
      SELECT
        id
      FROM
        ${TABLE_USERS}
      WHERE
        deleted_at IS NULL
    `;

    const users = await this.db.executeQuery({ query });
    return users.map((user: PersistedUser) => user.id);
  }
}
