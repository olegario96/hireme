import { Router } from 'express';
import { container } from 'tsyringe';

import { HttpStatusCode } from '../common/types';
import { Logger } from '../logger';
import {
  middlewareContentType,
  middlewareJoi,
  middlewarePreAuthorizedToken,
} from '../middlewares';
import { UserController } from '../users/user-controller';
import { FeatureFlagController } from './feature-flag-controller';
import { rolloutFeatureFlagSchema } from './schema-validation';

const INTERNAL_TOKEN_FF_ROLLOUT = 'INTERNAL_TOKEN_FF_ROLLOUT';

const loadRoutes = (router: Router): void => {
  const controller = container.resolve(FeatureFlagController);
  const logger = container.resolve(Logger);

  /**
   * @api [post] /rollout-feature-flag
   * bodyContentType: "application/json"
   * description: "Rollout specific feature flag to users"
   * parameters:
   *   - name: body
   *     in: body
   *     description: "HTTP Body request"
   *     schema:
   *       type: "object"
   *       required:
   *         - name
   *         - token
   *         - rolloutRatio
   *       properties:
   *         name:
   *           type: "string"
   *           example: "create.companies"
   *         token:
   *           type: "string"
   *           example: "your pre authorized token here"
   *         rolloutRatio:
   *           type: "float"
   *           example: "0.2"
   * responses:
   *   "200":
   *     description: "Message indicating success"
   *     schema:
   *       type: "object"
   *       properties:
   *         message:
   *           type: "string"
   *           example: "Rollout has been executed for 10 users"
   *   "400":
   *     description: "Invalid pre authorized token provided"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Invalid token provided!"
   *   "404":
   *     description: "Message indicating inexistent FF"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Feature Flag does not exist"
   *   "500":
   *     description: "Message indicating failure"
   *     schema:
   *       type: "object"
   *       properties:
   *         error:
   *           type: "string"
   *           example: "Could not rollout FF"
   */
  router.post(
    '/rollout-feature-flag',
    middlewareContentType(),
    middlewareJoi(rolloutFeatureFlagSchema),
    middlewarePreAuthorizedToken(INTERNAL_TOKEN_FF_ROLLOUT),
    async (req, res) => {
      try {
        const { rolloutRatio, name } = req.body;
        const featureFlag = await controller.getFeatureFlagByIndexedAttributes({ name });
        if (!featureFlag) {
          res.status(HttpStatusCode.NOT_FOUND).json({ error: 'Feature Flag does not exist' });
          return;
        }

        const users = await container.resolve(UserController).getUsers(rolloutRatio);
        const userIds = users.map(user => user.id);
        await controller.rolloutFeatureFlagToUsers(featureFlag.id, userIds);
        res.status(HttpStatusCode.OK).json({ message: `Rollout has been executed for ${userIds.length} users` });
      } catch (e) {
        const exception = e as Error;
        logger.error(`POST /rollout-feature-flag: ${exception.message}`);
        res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not rollout FF' });
      }
    });
};

module.exports = loadRoutes;
