import { injectable, singleton } from 'tsyringe';

import { UserId } from '../users/entity';
import { FeatureFlagDao } from './feature-flag-dao';
import { FeatureFlag, FeatureFlagId, IndexedFeatureFlagAttributes, PersistedFeatureFlag } from './types';

@injectable()
@singleton()
export class FeatureFlagController {
  constructor(private readonly featureFlagDao: FeatureFlagDao) {}

  async createFeatureFlag(featureFlag: FeatureFlag): Promise<PersistedFeatureFlag | null> {
    if (!featureFlag.name.length) {
      return null;
    }

    return this.featureFlagDao.createFeatureFlag(featureFlag);
  }

  getFeatureFlagsByNames(featureFlagNames: string[]): Promise<PersistedFeatureFlag[]> {
    return this.featureFlagDao.getFeatureFlagsByNames(featureFlagNames);
  }

  getFeatureFlagByIndexedAttributes(attributes: IndexedFeatureFlagAttributes): Promise<PersistedFeatureFlag | null> {
    return this.featureFlagDao.getFeatureFlagByIndexedAttributes(attributes);
  }

  rolloutFeatureFlagToUsers(featureFlagId: FeatureFlagId, userIds: UserId[]): Promise<void> {
    return this.featureFlagDao.rolloutFeatureFlagToUsers(featureFlagId, userIds);
  }

  changeFeatureFlagStatus(targetStatus: boolean, featureFlagId: FeatureFlagId, userId: UserId): Promise<void> {
    return this.featureFlagDao.changeFeatureFlagStatus(targetStatus, featureFlagId, userId);
  }

  areFeatureFlagsTurnedOnForUser(featureFlagIds: FeatureFlagId[], userId: UserId): Promise<boolean> {
    return this.featureFlagDao.areFeatureFlagsTurnedOnForUser(featureFlagIds, userId);
  }
}
