import { MetaFields } from '../common/types';

export type FeatureFlagId = number;
export interface PersistedFeatureFlag extends MetaFields {
  id: FeatureFlagId;
  name: string;
  description: string|null;
}

export interface IndexedFeatureFlagAttributes {
  id?: FeatureFlagId;
  name?: string;
}

export type FeatureFlag = Omit<PersistedFeatureFlag, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt'>;
export const TABLE_FEATURE_FLAGS = 'feature_flags';
export const TABLE_FEATURE_FLAGS_USERS = 'feature_flags_users';
