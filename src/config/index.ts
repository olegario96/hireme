import { config as loadEnvironmentVariables } from 'dotenv';
import { injectable, singleton } from 'tsyringe';

@injectable()
@singleton()
export class Config {
  constructor() {
    loadEnvironmentVariables({ path: '.env' });
  }

  ensureEnvironmentVariableIsDefined(environmentVariableName: string | string[]): void {
    const environmentVariableNames = Array.isArray(environmentVariableName)
      ? environmentVariableName
      : [environmentVariableName];

    for (const targetEnvironmentVariable of environmentVariableNames) {
      const environmentVariable = process.env[targetEnvironmentVariable];
      if (!environmentVariable) {
        throw new Error(`${targetEnvironmentVariable} was not provided!`);
      }
    }
  }
}
