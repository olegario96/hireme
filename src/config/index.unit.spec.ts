import 'reflect-metadata';

import { container } from 'tsyringe';

jest.mock('fs');

// Mock implementation should happen before the actual import
// eslint-disable-next-line simple-import-sort/imports
import { existsSync, readFileSync } from 'fs';
import { Config } from '.';

describe('Config', () => {
  let config: Config;
  let mockExistsSync: jest.Mock;
  let mockReadFileSync: jest.Mock;
  beforeAll(() => {
    mockExistsSync = existsSync as jest.Mock;
    mockReadFileSync = readFileSync as jest.Mock;
  });

  describe('ensureEnvironmentVariableIsDefined', () => {
    beforeAll(() => {
      mockExistsSync.mockReturnValue(true);
      mockReadFileSync.mockReturnValueOnce('IS_LOG_ENABLED=1\nFOO=bar');
      config = container.resolve(Config);
    });

    describe('when environment variable does not exist', () => {
      it('should throw an error', () => {
        const variable = 'var';
        expect(() => config.ensureEnvironmentVariableIsDefined(variable)).toThrow();
      });
    });

    describe('when environment variable exists', () => {
      let variable: string;

      beforeAll(() => {
        variable = 'hireme';
        process.env[variable] = variable;
      });

      it('should not throw an error', () => {
        expect(config.ensureEnvironmentVariableIsDefined(variable)).toStrictEqual(undefined);
      });
    });

    describe('when array is provided', () => {
      it('should iterate through all variables', () => {
        const environmentVariables = ['hireme'];
        expect(config.ensureEnvironmentVariableIsDefined(environmentVariables)).toStrictEqual(undefined);
      });
    });
  });
});
