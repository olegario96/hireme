export enum Environment {
  DEV = 'dev',
  CI = 'ci',
  STG = 'stg',
  PROD = 'prod',
}
