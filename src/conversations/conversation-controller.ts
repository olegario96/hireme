import { injectable, singleton } from 'tsyringe';

import { UserId } from '../users/entity';
import { ConversationDao } from './conversation-dao';
import { ConversationDomainLogic } from './conversation-domain-logic';
import { Conversation, ConversationId, PersistedConversation } from './types';

@injectable()
@singleton()
export class ConversationController {
  constructor(
    private readonly conversationDao: ConversationDao,
    private readonly conversationDomainLogic: ConversationDomainLogic
  ) {}

  createConversation(conversation: Conversation): Promise<PersistedConversation> {
    return this.conversationDao.createConversation(conversation);
  }

  async deactivateConversation(userId: UserId, conversationId: ConversationId): Promise<PersistedConversation | null> {
    const conversation = await this.conversationDao.getConversation(conversationId);
    if (!conversation) {
      return null;
    }

    return !this.conversationDomainLogic.shouldDeactivateConversation(userId, conversation)
      ? null
      : this.conversationDao.deactivateConversation(conversation);
  }
}
