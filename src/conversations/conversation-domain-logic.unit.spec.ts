import 'reflect-metadata';

import { container } from 'tsyringe';

import { ConversationDomainLogic } from './conversation-domain-logic';
import { PersistedConversation } from './types';

describe('ConversationDomainLogic', () => {
  let domainLogic: ConversationDomainLogic;
  beforeAll(() => {
    domainLogic = container.resolve(ConversationDomainLogic);
  });

  describe('shouldDeactivateConversation', () => {
    describe('when conversation has already been deactivated', () => {
      it('should return false', () => {
        const userId = 1;
        const conversation = { isActive: false } as PersistedConversation;
        const shouldDeactivateConversation = domainLogic.shouldDeactivateConversation(userId, conversation);
        expect(shouldDeactivateConversation).toStrictEqual(false);
      });
    });

    describe('when conversation is active', () => {
      describe('when user does not belong to the conversation', () => {
        it('should return false', () => {
          const userId = 1;
          const conversation = { isActive: true, userIdSender: 3, userIdReceiver: 4 } as PersistedConversation;
          const shouldDeactivateConversation = domainLogic.shouldDeactivateConversation(userId, conversation);
          expect(shouldDeactivateConversation).toStrictEqual(false);
        });
      });

      describe('when user belongs to the conversation', () => {
        it('should return true', () => {
          const userId = 1;
          const conversation = { isActive: true, userIdSender: 3, userIdReceiver: userId } as PersistedConversation;
          const shouldDeactivateConversation = domainLogic.shouldDeactivateConversation(userId, conversation);
          expect(shouldDeactivateConversation).toStrictEqual(true);
        });
      });
    });
  });
});
