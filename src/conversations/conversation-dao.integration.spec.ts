import 'reflect-metadata';

import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { TABLE_USERS, UserId } from '../users/entity';
import { ConversationDao } from './conversation-dao';
import { PersistedConversation, TABLE_CONVERSATIONS } from './types';

describe('ConversationDao', () => {
  let dao: ConversationDao;
  let userId: UserId;

  beforeAll(async () => {
    await connection.initialize();
    userId = 1;
    dao = container.resolve(ConversationDao);
    await populateUsersTable();
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_USERS),
      resetTable(TABLE_CONVERSATIONS),
    ]);

    await endConnections();
  });

  describe('createConversation', () => {
    it('should create conversation', async () => {
      const conversation = { userIdSender: userId, userIdReceiver: userId };
      await dao.createConversation(conversation);
      const conversations = await dao.getConversationsForUser(userId);
      expect(conversations.length).toStrictEqual(1);
    });
  });

  describe('deactivateConversation', () => {
    it('should deactivate conversation', async () => {
      const conversations = { id: 1 } as PersistedConversation;
      await dao.deactivateConversation(conversations);
      const shouldIncludeInactiveConversation = true;
      const targetConversations = await dao.getConversationsForUser(userId, shouldIncludeInactiveConversation);
      expect(targetConversations.length).toStrictEqual(1);
    });
  });
});
