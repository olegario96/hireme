import 'reflect-metadata';

import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { endConnections } from '../../tests/utils';
import { connection } from '../database/connection';
import { PersistedUser, TABLE_USERS } from '../users/entity';
import { ConversationController } from './conversation-controller';
import { TABLE_CONVERSATIONS } from './types';

describe('ConversationController', () => {
  let controller: ConversationController;

  beforeAll(async () => {
    await connection.initialize();
    controller = container.resolve(ConversationController);
    const override = { email: 'general@grievous.com' } as PersistedUser;
    await Promise.all([
      populateUsersTable(),
      populateUsersTable(override),
    ]);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_USERS),
      resetTable(TABLE_CONVERSATIONS),
    ]);

    await endConnections();
  });

  describe('createConversation', () => {
    it('should create conversation', async () => {
      const conversation = await controller.createConversation({ userIdSender: 1, userIdReceiver: 2 });
      expect(conversation).not.toStrictEqual(null);
    });
  });

  describe('deactivateConversation', () => {
    describe('when invalid conversation is provided', () => {
      it('should return a nullptr', async () => {
        const conversationId = -1;
        const userId = 2;
        const deactivatedConversation = await controller.deactivateConversation(userId, conversationId);
        expect(deactivatedConversation).toStrictEqual(null);
      });
    });

    describe('when a valid conversation is provided', () => {
      describe('when user can not deactivate conversation', () => {
        it('should return a nullptr', async () => {
          const userId = 3;
          const conversationId = 1;
          const deactivatedConversation = await controller.deactivateConversation(userId, conversationId);
          expect(deactivatedConversation).toStrictEqual(null);
        });
      });

      describe('when user can deactivate conversation', () => {
        it('should return deactivated conversation', async () => {
          const userId = 1;
          const conversationId = 1;
          const deactivatedConversation = (await controller.deactivateConversation(userId, conversationId))!;
          expect(deactivatedConversation.isActive).toStrictEqual(false);
        });
      });
    });
  });
});
