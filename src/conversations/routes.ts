import { Router } from 'express';
import { container } from 'tsyringe';

import { HttpStatusCode } from '../common/types';
import { Logger } from '../logger';
import {
  middlewareContentType,
  middlewareJoi,
  middlewareJwt,
} from '../middlewares';
import { CustomRequest } from '../middlewares/types';
import { ConversationController } from './conversation-controller';
import { createConversationSchema } from './schema-validation';

const loadRoutes = (router: Router): void => {
  const controller = container.resolve(ConversationController);
  const logger = container.resolve(Logger);

  router.post(
    '/conversation',
    middlewareContentType(),
    middlewareJoi(createConversationSchema),
    middlewareJwt(),
    async (req: CustomRequest, res) => {
      try {
        const userId = req.userId!;
        const { userIdSender, userIdReceiver } = req.body;
        if (userId !== userIdSender) {
          res.status(HttpStatusCode.FORBIDDEN).json({ error: 'You must be the sender when creating a conversation' });
          return;
        }

        const conversation = await controller.createConversation({ userIdSender, userIdReceiver });
        res.status(HttpStatusCode.OK).json(conversation);
      } catch (e) {
        const exception = e as Error;
        logger.error(`POST /conversation exception: ${exception.message}`);
        res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({ error: 'Could not authenticate user' });
      }
    });
};

module.exports = loadRoutes;
