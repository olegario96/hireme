import { MetaFields } from '../common/types';
import { UserId } from '../users/entity';

export type ConversationId = number;
export interface PersistedConversation extends MetaFields {
  id: ConversationId;
  userIdSender: UserId;
  userIdReceiver: UserId;
  isActive: boolean;
}

export type Conversation = Omit<PersistedConversation, 'id' | 'isActive' | 'createdAt' | 'updatedAt' | 'deletedAt'>;
export const TABLE_CONVERSATIONS = 'conversations';
