import * as Joi from 'joi';

export const createConversationSchema = Joi.object({
  userIdSender: Joi.number()
    .positive()
    .required(),
  userIdReceiver: Joi.number()
    .positive()
    .required(),
});
