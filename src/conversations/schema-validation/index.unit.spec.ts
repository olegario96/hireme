import { createConversationSchema } from '.';

describe('SchemaValidation', () => {
  describe('when provided object is valid', () => {
    it('should not return any error', () => {
      const conversation = { userIdSender: 1, userIdReceiver: 2 };
      const validatedConversation = createConversationSchema.validate(conversation);
      expect(validatedConversation.error).toStrictEqual(undefined);
    });
  });
});
