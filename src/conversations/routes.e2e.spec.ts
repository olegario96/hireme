/* eslint-disable jest/expect-expect */
import 'reflect-metadata';

import { Application } from 'express';
import request from 'supertest';
import { container } from 'tsyringe';

import { resetTable } from '../../seed/helper';
import { populateUsersTable } from '../../seed/users';
import { buildAppForTests, endConnections } from '../../tests/utils';
import { APPLICATION_JSON, AUTHORIZATION, CONTENT_TYPE } from '../common/constants';
import { HttpStatusCode } from '../common/types';
import { Jwt } from '../security/jwt';
import { TABLE_USERS, UserId } from '../users/entity';
import { ConversationController } from './conversation-controller';
import { TABLE_CONVERSATIONS } from './types';

describe('Conversations routes', () => {
  let app: Application;

  beforeAll(async () => {
    const context = __dirname;
    app = await buildAppForTests(context);
  });

  afterAll(async () => {
    await Promise.all([
      resetTable(TABLE_CONVERSATIONS),
      resetTable(TABLE_USERS),
    ]);

    await endConnections();
  });

  describe('POST /conversation', () => {
    describe('when invalid content type is provided', () => {
      it('should return bad request', async () => {
        await request(app).post('/conversation')
          .set(CONTENT_TYPE, 'application/javascript')
          .send()
          .expect(HttpStatusCode.BAD_REQUEST);
      });
    });

    describe('when valid content type is provided', () => {
      describe('when body request is invalid', () => {
        it('should return bad request', async () => {
          const body = { foo: 'bar' };
          await request(app).post('/conversation')
            .set(CONTENT_TYPE, APPLICATION_JSON)
            .send(body)
            .expect(HttpStatusCode.BAD_REQUEST);
        });
      });

      describe('when body request is valid', () => {
        describe('when invalid JWT is provided', () => {
          it('should return unauthorized', async () => {
            const body = { userIdSender: 1, userIdReceiver: 1 };
            await request(app).post('/conversation')
              .set(CONTENT_TYPE, APPLICATION_JSON)
              .set(AUTHORIZATION, 'Bearer token')
              .send(body)
              .expect(HttpStatusCode.UNAUTHORIZED);
          });
        });

        describe('when valid JWT is provided', () => {
          let jwt: Jwt;
          let userId: UserId;

          beforeAll(async () => {
            jwt = container.resolve(Jwt);
            const { id } = (await populateUsersTable())!;
            userId = id;
          });

          describe('when sender is different from the authenticated user', () => {
            it('should return forbidden status', async () => {
              const body = { userIdSender: 4, userIdReceiver: 1 };
              const token = jwt.signToken({ userId });
              await request(app).post('/conversation')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .set(AUTHORIZATION, `Bearer ${token}`)
                .send(body)
                .expect(HttpStatusCode.FORBIDDEN);
            });
          });

          describe('when sender is the same one who is authenticated', () => {
            it('should return ok status code', async () => {
              const body = { userIdSender: 1, userIdReceiver: 1 };
              const token = jwt.signToken({ userId });
              await request(app).post('/conversation')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .set(AUTHORIZATION, `Bearer ${token}`)
                .send(body)
                .expect(HttpStatusCode.OK);
            });
          });

          describe('when there is an exception', () => {
            let controller: ConversationController;
            beforeAll(() => {
              controller = container.resolve(ConversationController);
              jest.spyOn(controller, 'createConversation')
                .mockRejectedValueOnce(() => {
                  throw new Error('Mocked');
                });
            });

            it('should return internal server error', async () => {
              const body = { userIdSender: 1, userIdReceiver: 1 };
              const token = jwt.signToken({ userId });
              await request(app).post('/conversation')
                .set(CONTENT_TYPE, APPLICATION_JSON)
                .set(AUTHORIZATION, `Bearer ${token}`)
                .send(body)
                .expect(HttpStatusCode.INTERNAL_SERVER_ERROR);
            });
          });
        });
      });
    });
  });
});
