import { injectable, singleton } from 'tsyringe';

import { PostgresDao } from '../common/postgres-dao';
import { QueryParams } from '../database/types';
import { UserId } from '../users/entity';
import { Conversation, ConversationId, PersistedConversation, TABLE_CONVERSATIONS } from './types';

@injectable()
@singleton()
export class ConversationDao extends PostgresDao {
  async createConversation(conversation: Conversation): Promise<PersistedConversation> {
    const query = `
      INSERT INTO
        ${TABLE_CONVERSATIONS} (
          user_id_sender,
          user_id_receiver,
          is_active,
          created_at,
          updated_at
        )
      VALUES (
        $1,
        $2,
        $3,
        $4,
        $5
      )
      RETURNING
        id
    `;

    const { timestamp, unixEpoch } = this.getDateTimestamp();
    const isActive = true;
    const { userIdReceiver, userIdSender } = conversation;
    const values = [userIdSender, userIdReceiver, isActive, timestamp, timestamp];
    const [{ id }] = await this.db.executeQuery({ query, values });
    return {
      ...conversation,
      id,
      isActive,
      createdAt: unixEpoch,
      updatedAt: unixEpoch,
      deletedAt: null,
    };
  }

  async getConversation(conversationId: ConversationId): Promise<PersistedConversation | null> {
    const query = `
      SELECT
        *
      FROM
        ${TABLE_CONVERSATIONS}
      WHERE
        id = $1
        AND deleted_at IS NULL
    `;

    const values = [conversationId];
    const persistedConversation = await this.db.executeQuery({ query, values });
    return !persistedConversation.length ? null : persistedConversation.pop()!;
  }

  getConversationsForUser(
    userId: UserId,
    shouldIncludeInactiveConversation = false
  ): Promise<PersistedConversation[]> {
    let query = `
      SELECT
        *
      FROM
        ${TABLE_CONVERSATIONS}
      WHERE (
        user_id_sender = $1
        OR user_id_receiver = $2
      )
        AND deleted_at IS NULL
    `;

    const values: QueryParams = [userId, userId];
    if (!shouldIncludeInactiveConversation) {
      query += ' AND is_active = $3 ';
      values.push(true);
    }

    return this.db.executeQuery({ query, values });
  }

  async deactivateConversation(persistedConversation: PersistedConversation): Promise<PersistedConversation> {
    const query = `
      UPDATE
        ${TABLE_CONVERSATIONS}
      SET
        is_active = false
      WHERE
        id = $1
    `;

    const values = [persistedConversation.id];
    await this.db.executeQuery({ query, values });
    return {
      ...persistedConversation,
      isActive: false,
    };
  }
}
