import { injectable, singleton } from 'tsyringe';

import { UserId } from '../users/entity';
import { PersistedConversation } from './types';

@injectable()
@singleton()
export class ConversationDomainLogic {
  shouldDeactivateConversation(userId: UserId, conversation: PersistedConversation): boolean {
    const { userIdReceiver, userIdSender, isActive } = conversation;
    return !isActive
      ? false
      : userId === userIdReceiver || userId === userIdSender;
  }
}
