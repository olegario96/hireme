import 'reflect-metadata';

import { container } from 'tsyringe';

import { assertFeatureFlagsOnForUser } from './feature-flag';

describe('FeatureFlagMiddleware', () => {
  let req;
  let res;
  let resolveSpy: jest.SpyInstance;
  let next;

  beforeAll(() => {
    req = {};
    res = { boom: { internal: jest.fn(), unauthorized: jest.fn() } };
    next = jest.fn();
    resolveSpy = jest.spyOn(container, 'resolve');
  });

  describe('when request does not have userId', () => {
    it('should not call next function', () => {
      assertFeatureFlagsOnForUser([])(req, res, next);
      expect(res.boom.internal).toHaveBeenCalled();
      expect(next).not.toHaveBeenCalled();
    });
  });

  describe('when request has userId', () => {
    beforeAll(() => {
      req = { userId: 117 };
    });

    describe('when it can use database', () => {
      describe('when FF are not turned on for user', () => {
        beforeAll(() => {
          resolveSpy.mockImplementationOnce(() => ({
            getFeatureFlagsByNames: jest.fn().mockResolvedValueOnce([{ id: 1 }]),
            areFeatureFlagsTurnedOnForUser: jest.fn().mockResolvedValueOnce(false),
          }));
        });

        it('should not call next function', async () => {
          // await here is required due to I/O operation
          /* eslint-disable @typescript-eslint/await-thenable */
          await assertFeatureFlagsOnForUser(['create.companies'])(req, res, next);
          expect(res.boom.unauthorized).toHaveBeenCalled();
          expect(next).not.toHaveBeenCalled();
        });
      });

      describe('when FF are turned on for user', () => {
        beforeAll(() => {
          resolveSpy.mockImplementationOnce(() => ({
            getFeatureFlagsByNames: jest.fn().mockResolvedValueOnce([{ id: 1 }]),
            areFeatureFlagsTurnedOnForUser: jest.fn().mockResolvedValueOnce(true),
          }));
        });

        it('should call next function', async () => {
          await assertFeatureFlagsOnForUser(['create.companies'])(req, res, next);
          expect(next).toHaveBeenCalled();
        });
      });
    });

    describe('when it can not use database', () => {
      beforeAll(() => {
        resolveSpy.mockImplementationOnce(() => new Error('Mocked!'));
        resolveSpy.mockImplementationOnce(() => ({
          error: jest.fn(),
        }));
      });

      it('should not call next function', async () => {
        await assertFeatureFlagsOnForUser(['create.companies'])(req, res, next);
        expect(next).not.toHaveBeenCalled();
      });
    });
  });
});
