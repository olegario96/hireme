import { NextFunction, Response } from 'express';
import { container } from 'tsyringe';

import { BEARER } from '../common/constants';
import { Logger } from '../logger';
import { Jwt } from '../security/jwt';
import { UserController } from '../users/user-controller';
import { CustomRequest, MiddlewareRequestHandler } from './types';

const ERROR_MSG = 'Invalid token provided!';

export const jwtValidation = (): MiddlewareRequestHandler =>
  async (req: CustomRequest, res: Response, next: NextFunction) => {
    const token = req.headers.authorization;
    const parsedToken = getParsedToken(token);
    if (!parsedToken) {
      res.boom.unauthorized(ERROR_MSG);
      return;
    }

    try {
      const isValid = await isTokenValid(parsedToken);
      if (!isValid) {
        res.boom.unauthorized(ERROR_MSG);
        return;
      }

      const { userId } = container.resolve(Jwt).verifyToken(parsedToken);
      req.userId = userId;
      next();
    } catch (e) {
      const logger = container.resolve(Logger);
      logger.error(`Jwt Middleware exception: ${e.message}`);
      const isMalformedException = e.message.includes('malformed');
      if (isMalformedException) {
        res.boom.unauthorized(ERROR_MSG);
        return;
      }

      res.boom.internal('It is not possible to authenticate user for now');
    }
  };

const getParsedToken = (token?: string): string | null => {
  if (!token || !token.length) {
    return null;
  }

  if (!token.startsWith(BEARER)) {
    return null;
  }

  const pureToken = token.replace(BEARER, '').split(' ')
    .join('');

  if (!pureToken.length) {
    return null;
  }

  return pureToken;
};

const isTokenValid = async (parsedToken: string): Promise<boolean> => {
  const tokenPayload = container.resolve(Jwt).verifyToken(parsedToken);
  const controller = container.resolve(UserController);
  const persistedUser = await controller.getUser(tokenPayload.userId);
  return persistedUser !== null;
};
