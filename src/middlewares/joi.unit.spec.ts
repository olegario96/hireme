import { authenticateSchema } from '../users/schema-validation';
import { schemaValidation } from './joi';

describe('JoiMiddleware', () => {
  let req;
  let res;
  let next;

  beforeAll(() => {
    req = {};
    res = { boom: { badRequest: jest.fn() } };
    next = jest.fn();
  });

  describe('when request body does not match schema', () => {
    it('should not call next function', () => {
      req = { body: { firstName: '' } };
      schemaValidation(authenticateSchema)(req, res, next);
      expect(next).not.toHaveBeenCalled();
    });
  });

  describe('when request body matches schema', () => {
    it('should call next function', () => {
      req = { body: { email: 'darth@sidious.com', password: 'ExecuteOrder66' } };
      schemaValidation(authenticateSchema)(req, res, next);
      expect(next).toHaveBeenCalled();
    });
  });
});
