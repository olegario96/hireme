import { NextFunction, Request, Response } from 'express';
import { ObjectSchema } from 'joi';

import { MiddlewareRequestHandler } from './types';

export const schemaValidation = (schema: ObjectSchema): MiddlewareRequestHandler =>
  (req: Request, res: Response, next: NextFunction) => {
    const { error } = schema.validate(req.body);
    if (error) {
      res.boom.badRequest(`Invalid body provided: ${JSON.stringify(error)}`);
      return;
    }

    next();
  };
