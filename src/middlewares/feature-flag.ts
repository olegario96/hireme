import { NextFunction, Response } from 'express';
import { container } from 'tsyringe';

import { FeatureFlagController } from '../feature-flags/feature-flag-controller';
import { Logger } from '../logger';
import { CustomRequest, MiddlewareRequestHandler } from './types';

export const assertFeatureFlagsOnForUser = (featureFlagNames: string[]): MiddlewareRequestHandler =>
  async (req: CustomRequest, res: Response, next: NextFunction) => {
    const { userId } = req;
    if (!userId) {
      res.boom.internal('Middleware needs to be called after JWT one!');
      return;
    }

    try {
      const featureFlagController = container.resolve(FeatureFlagController);
      const persistedFeatureFlags = await featureFlagController.getFeatureFlagsByNames(featureFlagNames);
      const featureFlagsIds = persistedFeatureFlags.map(ff => ff.id);
      const areFeatureFlagsTurnedOnForUser =
        await featureFlagController.areFeatureFlagsTurnedOnForUser(featureFlagsIds, userId);

      if (!areFeatureFlagsTurnedOnForUser) {
        res.boom.unauthorized('FF is not turned on for user');
        return;
      }

      next();
    } catch (e) {
      const logger = container.resolve(Logger);
      logger.error(`Feature Flag Middleware exception: ${e.message}`);
      res.boom.unauthorized('Could not read FF status now');
    }
  };
