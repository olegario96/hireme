import { NextFunction, Request, Response } from 'express';
import { container } from 'tsyringe';

import { Config } from '../config';
import { Logger } from '../logger';
import { MiddlewareRequestHandler } from './types';

export const preAuthorizedToken = (tokenName: string): MiddlewareRequestHandler =>
  (req: Request, res: Response, next: NextFunction) => {
    const config = container.resolve(Config);
    config.ensureEnvironmentVariableIsDefined(tokenName);
    const token = process.env[tokenName]!;
    const { token: providedToken } = req.body;
    if (!providedToken || providedToken !== token) {
      const logPrefix = 'PreAuthorizedToken -';
      const logger = container.resolve(Logger);
      logger.warn(
        `${logPrefix} Someone has tried to use one of our internal routes with invalid token: ${providedToken}`
      );

      res.boom.badRequest('Invalid token provided!');
      return;
    }

    next();
  };
