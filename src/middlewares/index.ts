import { contentTypeJsonHeader } from './content-type';
import { assertFeatureFlagsOnForUser } from './feature-flag';
import { schemaValidation } from './joi';
import { jwtValidation } from './jwt';
import { loginAttempt } from './login-attempt';
import { preAuthorizedToken } from './pre-authorized-token';

export const middlewareContentType = contentTypeJsonHeader;
export const middlewareFeatureFlag = assertFeatureFlagsOnForUser;
export const middlewareJoi = schemaValidation;
export const middlewareJwt = jwtValidation;
export const middlewareLoginAttempt = loginAttempt;
export const middlewarePreAuthorizedToken = preAuthorizedToken;
