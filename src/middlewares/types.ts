/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, RequestHandler } from 'express';
import { ParamsDictionary, Query } from 'express-serve-static-core';

import { UserId } from '../users/entity';

type P = ParamsDictionary;
type ResBody = any;
type ReqBody = any;
type ReqQuery = Query;
type Locals = Record<string, any>;
export type MiddlewareRequestHandler = RequestHandler<P, ResBody, ReqBody, ReqQuery, Locals>;
export type CustomRequest = Request & {
  userId?: UserId;
};
