import 'reflect-metadata';

import { container } from 'tsyringe';

import { loginAttempt } from './login-attempt';

describe('LoginAttemptMiddleware', () => {
  let req;
  let res;
  let resolveSpy: jest.SpyInstance;
  let next;

  beforeAll(() => {
    req = { headers: {} };
    res = { boom: { badRequest: jest.fn(), tooManyRequests: jest.fn() } };
    next = jest.fn();
    resolveSpy = jest.spyOn(container, 'resolve');
  });

  describe('when provided ip is not valid', () => {
    it('should not call next function', () => {
      req.ip = '127..0.1';
      loginAttempt()(req, res, next);
      expect(next).not.toHaveBeenCalled();
    });
  });

  describe('when valid ip address is provided', () => {
    describe('when number of ip attempts is lower than the hard limit', () => {
      beforeAll(() => {
        resolveSpy.mockImplementationOnce(() => ({
          isIpValid: jest.fn().mockReturnValue(true),
        }));

        resolveSpy.mockImplementationOnce(() => ({
          createLoginAttemptForIp: jest.fn().mockResolvedValue([]),
          getLoginAttemptsForIp: jest.fn().mockResolvedValue([]),
        }));
      });

      it('should call next function', async () => {
        req.ip = '127.0.0.1';
        // await here is required due to I/O operation
        /* eslint-disable @typescript-eslint/await-thenable */
        await loginAttempt()(req, res, next);
        expect(next).toHaveBeenCalled();
      });
    });

    describe('when number of ip attempts is greater than the hard limit', () => {
      beforeAll(() => {
        resolveSpy.mockImplementationOnce(() => ({
          isIpValid: jest.fn().mockReturnValue(true),
        }));

        resolveSpy.mockImplementationOnce(() => ({
          createLoginAttemptForIp: jest.fn().mockResolvedValue([]),
          // eslint-disable-next-line @typescript-eslint/no-magic-numbers
          getLoginAttemptsForIp: jest.fn().mockResolvedValue([1, 2, 3, 4, 5, 6]),
        }));
      });

      afterAll(() => {
        jest.resetAllMocks();
      });

      it('should not call next function', async () => {
        req.ip = '127.0.0.1';
        await loginAttempt()(req, res, next);
        expect(next).not.toHaveBeenCalled();
      });
    });

    describe('when it can not execute operations against the database', () => {
      beforeAll(() => {
        resolveSpy.mockImplementationOnce(() => ({
          isIpValid: jest.fn().mockReturnValue(true),
        }));

        resolveSpy.mockImplementationOnce(() => ({
          createLoginAttemptForIp: jest.fn().mockRejectedValue(new Error('Mock')),
        }));

        resolveSpy.mockImplementationOnce(() => ({
          error: jest.fn(),
        }));
      });

      it('should not call next function', async () => {
        req.ip = '127.0.0.1';
        await loginAttempt()(req, res, next);
        expect(next).not.toHaveBeenCalled();
      });
    });
  });
});
