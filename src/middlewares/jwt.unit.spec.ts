import 'reflect-metadata';

import { container } from 'tsyringe';

import { Jwt } from '../security/jwt';
import { jwtValidation } from './jwt';

describe('JwtMiddleware', () => {
  let jwt: Jwt;
  let req;
  let res;
  let resolveSpy: jest.SpyInstance;
  let next;

  beforeAll(() => {
    jwt = container.resolve(Jwt);
    req = { headers: {} };
    res = { boom: { unauthorized: jest.fn(), internal: jest.fn() } };
    next = jest.fn();
    resolveSpy = jest.spyOn(container, 'resolve');
  });

  describe('when provided token is undefined', () => {
    it('should not call next function', () => {
      jwtValidation()(req, res, next);
      expect(next).not.toHaveBeenCalled();
    });
  });

  describe('when provided token is empty', () => {
    beforeAll(() => {
      req.headers.authorization = '';
    });

    it('should not call next function', () => {
      jwtValidation()(req, res, next);
      expect(next).not.toHaveBeenCalled();
    });
  });

  describe('when provided toke is not empty', () => {
    describe('when token does not start with Bearer', () => {
      beforeAll(() => {
        req.headers.authorization = 'asdf';
      });

      it('should not call next function', () => {
        jwtValidation()(req, res, next);
        expect(next).not.toHaveBeenCalled();
      });
    });

    describe('when token starts with Bearer', () => {
      describe('when Bearer field  is empty', () => {
        beforeAll(() => {
          req.headers.authorization = 'Bearer ';
        });

        it('should not call next function', () => {
          jwtValidation()(req, res, next);
          expect(next).not.toHaveBeenCalled();
        });
      });

      describe('when Bearer field is not empty', () => {
        describe('when token is malformed', () => {
          beforeAll(() => {
            req.headers.authorization = 'Bearer asdf';
            resolveSpy.mockImplementationOnce(() => {
              throw new Error('Jwt Middleware exception: jwt malformed');
            });

            resolveSpy.mockImplementationOnce(() => ({
              error: jest.fn(),
            }));
          });

          it('should not call next function', () => {
            jwtValidation()(req, res, next);
            expect(next).not.toHaveBeenCalled();
          });
        });

        describe('when token is correctly formatted', () => {
          beforeAll(() => {
            const token = jwt.signToken({ userId: 66 });
            req.headers.authorization = `Bearer ${token}`;
          });

          describe('when can not connect to the database', () => {
            beforeAll(() => {
              resolveSpy.mockImplementationOnce(() => {
                throw new Error('Can not connect to the database');
              });

              resolveSpy.mockImplementationOnce(() => ({
                error: jest.fn(),
              }));
            });

            it('should not call next function', () => {
              jwtValidation()(req, res, next);
              expect(next).not.toHaveBeenCalled();
            });
          });

          describe('when it can connect to the database', () => {
            // await here is required due to I/O operation
            /* eslint-disable @typescript-eslint/await-thenable */
            describe('when user does not exist', () => {
              beforeAll(() => {
                resolveSpy.mockImplementationOnce(() =>
                  ({ verifyToken: jest.fn().mockImplementationOnce(() => ({ userId: 66 })) }),
                );

                resolveSpy.mockImplementationOnce(() => ({ getUser: jest.fn().mockImplementationOnce(() => null) }));
              });

              it('should not call next function', async () => {
                await jwtValidation()(req, res, next);
                expect(next).not.toHaveBeenCalled();
              });
            });

            describe('when user exists', () => {
              beforeAll(() => {
                resolveSpy.mockImplementationOnce(() =>
                  ({ verifyToken: jest.fn().mockImplementationOnce(() => ({ userId: 1 })) }),
                );

                resolveSpy.mockImplementationOnce(() =>
                  ({ getUser: jest.fn().mockImplementationOnce(() => ({ id: 1 })) })
                );
              });

              it('should call next function', async () => {
                await jwtValidation()(req, res, next);
                expect(next).toHaveBeenCalled();
              });
            });
          });
        });
      });
    });
  });
});
