import { NextFunction, Response } from 'express';
import { container } from 'tsyringe';

import { Logger } from '../logger';
import { IpAddressDomainLogic } from '../server/ip-address-domain-logic';
import { LoginAttemptsDao } from '../server/login-attempts-dao';
import { CustomRequest, MiddlewareRequestHandler } from './types';

const DEFAULT_ERROR_MSG = 'Too many login attempts!';
const MAX_LOGIN_ATTEMPTS = 5;

export const loginAttempt = (): MiddlewareRequestHandler =>
  async (req: CustomRequest, res: Response, next: NextFunction) => {
    const { ip } = req;
    const ipAddressDomainLogic = container.resolve(IpAddressDomainLogic);
    if (!ipAddressDomainLogic.isIpValid(ip)) {
      res.boom.badRequest('Invalid IP address provided!');
      return;
    }

    const loginAttemptDao = container.resolve(LoginAttemptsDao);
    try {
      /** ip validation ensures non undefined value */
      await loginAttemptDao.createLoginAttemptForIp(ip!);
      const loginAttempts = await loginAttemptDao.getLoginAttemptsForIp(ip!);
      if (loginAttempts.length > MAX_LOGIN_ATTEMPTS) {
        res.boom.tooManyRequests(DEFAULT_ERROR_MSG);
        return;
      }

      next();
    } catch (e) {
      const logger = container.resolve(Logger);
      logger.error(`Login Attempts Middleware exception: ${e.message}`);
      res.boom.tooManyRequests(DEFAULT_ERROR_MSG);
    }
  };
