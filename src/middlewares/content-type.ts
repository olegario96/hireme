import { NextFunction, Request, Response } from 'express';

import { APPLICATION_JSON, CONTENT_TYPE } from '../common/constants';
import { MiddlewareRequestHandler } from './types';

export const contentTypeJsonHeader = (): MiddlewareRequestHandler =>
  (req: Request, res: Response, next: NextFunction) => {
    if (req.headers[CONTENT_TYPE] !== APPLICATION_JSON) {
      res.boom.badRequest('Invalid content-type header value');
      return;
    }

    next();
  };
