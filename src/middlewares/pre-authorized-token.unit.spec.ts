import 'reflect-metadata';

import { container } from 'tsyringe';

import { Config } from '../config';
import { preAuthorizedToken } from './pre-authorized-token';

const INTERNAL_TOKEN_FF_ROLLOUT = 'INTERNAL_TOKEN_FF_ROLLOUT';

describe('PreAuthorizedTokenMiddleware', () => {
  let req;
  let res;
  let next;

  beforeEach(() => {
    req = { body: { } };
    res = { boom: { badRequest: jest.fn() } };
    next = jest.fn();
    const config = container.resolve(Config);
    jest
      .spyOn(container, 'resolve')
      .mockImplementationOnce(() => config)
      .mockImplementationOnce(() => ({
        warn: jest.fn(),
      }));
  });

  describe('when body request does not contain a token', () => {
    it('should return bad request', () => {
      preAuthorizedToken(INTERNAL_TOKEN_FF_ROLLOUT)(req, res, next);
      expect(res.boom.badRequest).toHaveBeenCalled();
    });
  });

  describe('when body request contains a token', () => {
    describe('when tokens do not match', () => {
      beforeEach(() => {
        req.body.token = 'foo';
      });

      it('should return bad request', () => {
        preAuthorizedToken(INTERNAL_TOKEN_FF_ROLLOUT)(req, res, next);
        expect(res.boom.badRequest).toHaveBeenCalled();
      });
    });

    describe('when tokens match', () => {
      beforeEach(() => {
        req.body.token = process.env.INTERNAL_TOKEN_FF_ROLLOUT!;
      });

      it('should call next function', () => {
        preAuthorizedToken(INTERNAL_TOKEN_FF_ROLLOUT)(req, res, next);
        expect(next).toHaveBeenCalled();
      });
    });
  });
});
