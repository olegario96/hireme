import { CONTENT_TYPE } from '../common/constants';
import { contentTypeJsonHeader } from './content-type';

describe('ContentTypeMiddleware', () => {
  let req;
  let res;
  let next;

  beforeAll(() => {
    req = {};
    res = { boom: { badRequest: jest.fn() } };
    next = jest.fn();
  });

  describe('when content-type header value is invalid', () => {
    it('should return bad request', () => {
      req = { headers: { [CONTENT_TYPE]: 'text/plain' } };
      contentTypeJsonHeader()(req, res, next);
      expect(next).not.toHaveBeenCalled();
    });
  });

  describe('when content-type header value is valid', () => {
    it('should call next function', () => {
      req = { headers: { [CONTENT_TYPE]: 'application/json' } };
      contentTypeJsonHeader()(req, res, next);
      expect(next).toHaveBeenCalled();
    });
  });
});
