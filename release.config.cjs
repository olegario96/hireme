/** @type {import('semantic-release').GlobalConfig} */
module.exports = {
  branches: ['master'],
  repositoryUrl: 'https://gitlab.com/olegario96/hireme',
  tagFormat: 'v${version}',
  plugins: [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    '@semantic-release/changelog',
    [
      '@semantic-release/npm',
      {
        npmPublish: false,
      }
    ],
    [
      '@semantic-release/git',
      {
        assets: [
          'CHANGELOG.md',
          'package.json',
          'package-lock.json'
        ]
      }
    ],
    [
      '@semantic-release/gitlab',
      {
        gitlabUrl: 'https://gitlab.com/olegario96/hireme',
        successComment: false,
        failComment: false,
        failTitle: false,
        assignee: 'olegario96'
      }
    ]
  ]
};
