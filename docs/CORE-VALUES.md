# Core values
We strongly believe soft and hard skills are equally important to
maintain our software. Nevertheless, there are some abilities we
judge every contributor must have in order to collaborate to this
project. Here you will find each one of them and you can also better
understand why we think we need to have these competencies.

## Transparent to the core
It is impossible to build trustful relationships without transparency. Our
project uses a wide range of open source tools. Said that, we believe
we can show to both customers and contributors our honesty by making
this repository 100% open source and still be profitable. Besides,
by having an open source project, we empower everyone to create
healthy discussions and improve it whenever it is necessary.

## Security as first priority
There's no way we can build a reliable software without the
appropriate security in place. No matter if we are creating
new features or improving our project, we need to ensure
we are taking the right measures to protect us and our customers.

## Automation is the key
Time is our most precious resource and we should not waste it. There's
nothing more frustrating than do the same thing over and over
again. If it's possible to automate a task bring the discussion to
the team and let's evaluate the effort to do so. If we believe
it will facilitate everyone's life, there's no reason to not do it.

## A better code means a better software
Most of the time we are in a rush to deliver features and
complete tasks as fast as we can. We'll always do our best
to deliver things on time, but there's always room for improvement.
Remind yourself: you can be the one that will be maintaining
the code tomorrow, so we need to consistently review it and
be certain we are creating code with the best practices. Do you
think we can improve our code? Let's do it. It is really
demotivating deal with chaotic code.
