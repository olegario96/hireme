# hireme
FIXME

## Running all containers
```sh
cp .env.dev .env
docker-compose up -d
```

## Running migrations locally (outside of container)
```sh
npm run db:migrate run
```

### Running SQL commands inside docker instance
```
npm run db:local:connect
```

## Docs
We document our API using `swagger-ui`. You can serve
documentation locally by running:
```sh
npm run docs
```

`swagger` will be available at `localhost:8080`

## Running lint
```sh
npm run lint
```

To properly format files, make sure the [`editorconfig`](https://editorconfig.org/)
plugin is enabled on your IDE.

## Running tests
[Click here](./tests/README.md) to see how to run tests and
understand more about their organization.
